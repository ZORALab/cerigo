# Version 0.0.2
## Mon, 21 Sep 2020 23:46:47 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 21b096d .godocgen/template/md: fixed minor example rendering bug
2. 31553b5 os/term: refactored to facilitate new infrastructure and windows
3. ed9249f root: renamed master branch to main branch
4. 975fe46 .configs/gopher/releases/targets: fixed bad build target
5. 6779c44 root: upgraded Bissetii to version v1.12.5
6. ed1a6ed root: added docs.go and docs_test.go
7. fa06db9 root: upgraded infrastructure to self-contained git repository
8. 7d2fe70 .sites/static/img: added missing img/sponsors/zoralab.png
9. 7bbe3e3 .fennec: updated fennec to the latest and greatest
10. 48ef3ae .gitlab-ci.yml: enable documentation CI
11. 2f4d25b root: updated documentation engines and documentations
12. 4694c03 VERSION: starting v0.0.2 development

# Version 0.0.1
## Thu, 16 Jan 2020 12:31:09 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. dcd6432 .configs/manta/manta.cfg: set reference branch to staging
2. f20a49d .scripts/goSetup.bash: added web crawling to fetch latest version
3. 795e347 .gitignore: added exception to .d/ directory
4. e1d4e54 .scripts/goCI.bash: fixed missing goConfig.bash sourcing problem
5. 6bd3dfd .scripts/goCI.bash: added defensive mechanism for _install_go
6. 2847af5 .scripts/goSetup.bash: fixed incorrect command code.
7. 88a7240 .scripts/goSetup.bash: set bad command exit value to 1
8. 5fe68c4 .scripts: rename everything to match standardized pattern
9. d1c5381 .scripts/gosetup.bash: corrected invalid command responses
10. 145c4eb .gitlab-ci.yml: added setup Go compiler
11. 10d8c9b .root: updated cerigo operational needs
12. 98356b6 encoding/float: optimized to use encoding/float/floatconv
13. 410f93b encoding/float: added floatconv package
14. bda282a VERISON: reset version back to 0.0.1 for reinitalization
15. 917222a root: removed version directory concept and revert to branching
16. b5845cc v3/crypto: removed unstable crypto packages
17. 4d016fc .scripts/goCI.bash: updated regular expression for ruby
18. 2667a14 v1: added version 1.1.0 into v1 directory
19. cd491bd go.mod: clean up go.mod at root directory
20. c9fa150 v2: added version 2 cerigo into v2 directory.
21. d6f3f8f .gitlab-ci.yml: shift repo CI back to script modes
22. e07cc71 v3: comply to go module vX directory pattern
23. c9ad944 .scripts/goconfig.bash: styled to match all automation scripts
24. e81bc74 .scripts/goCI.bash: renamed from .scripts/gotestCI.bash
25. 1b94acc .scripts/goconfig.bash: added go program check before source
26. 73c533d .scripts/gosetup.bash: optimized gosetup.bash for CI automation
27. 117b7f4 .scripts/gotestCI.bash: optimized gotestCI.bash
28. 29debb4 os/term: applied gosec G204 audit
29. bc249f9 os/term: added wsl corrections
30. 047ea91 .golangci.yml: increase gocognit threshold from 15 to 20
31. a20d948 os/file/filehelper: applied wsl corrections
32. 23cdb0c os/args: applied gocognit false positive detection
33. ccee3ac os/args: applied gocyclo false positive detections
34. a02cd3d os/args: added unparam exclusion
35. d31f9b7 os/args: applied misspell correction
36. a2c7693 os/args: added lll corrections
37. 2e550e1 os/args: added wsl corrections
38. 3ba16de strings/strhelper: added wsl corrections
39. 17eeb38 algo/cmdchain: applied gocognit corrections
40. 0c82662 algo/cmdchain: added lll corrections.
41. 2c473d2 .golangci.yml: increase dupl threshold from 100 to 150
42. 409d832 algo/cmdchain: applied wsl corrections
43. 76b111f encoding/float/example_test.go: fixed wsl issue
44. 85451b5 encoding/float: fixed gocyclo and gocognit reported complexity
45. da34e93 .scripts/goconfig.bash: added goDocument command
46. d45b626 .scripts/goconfig.bash: change logging directory to .log
47. 547b80e .scripts/goconfig.bash: refactored goCheck for golanci-lint
48. 2441b47 docs/.../projects/license.md: updated URL filepath
49. 3181acf LICENSE: ported hard-copy into repository
50. ff3d517 testing/thelper: added gocyclo and gocognit corrections
51. 91046b1 .golangci.yml: added golangci-lint configuration file
52. bd71e65 testing/thelper: added corrections to issues reported by dupl linter
53. 820d7db testing/thelper: applied wsl linter correction
54. 2e9bae2 encoding: added float ENDEC package
55. 048f02c go.mod: updated to go1.13
56. 288b70c testing/thelper: added float32, float64, math/big.Float assertions
57. 04c4993 email: removed existing network related packages
58. f7ba671 docs/.../projects/status.md: renamed Release Health to Release Info.
59. dad77c7 docs/.../getting_started: added contribute_back.md guide
60. 28d2cd8 docs/.../getting_started: added using_as_consumer.md
61. 2580d5d .sites/config/_default/languages.toml: re-ordered language by weight
62. 5d03418 .scripts: added goconfig.bash command functions source codes
63. 3a89c62 .sites, docs: added bissetti hugo-based documentation system
64. 6dd2862 root: added .gitlab templates into repository
65. 15a8c34 strings/strhelper: refactored strhelper to use thelper.Scenario
66. db7b417 os/term: refactored to use thelper.Scenario
67. 69fd46a os/files/filehelper: refactored to use thelper.Scenario
68. e55feb0 os/args: updated testManagerScenario to use thelper.Scenario
69. 2fc6819 testing/thelper: re-organize the documentation for thelper package
70. 91f4c78 testing/thelper: added ExpectStringHasKeywords(...) assertion function
71. 1012aed algo: added cmdchain package
72. 3eb2155 testing/thelper: added ExpectSameBool(...) assertion function
73. 04a4f9e testing/thelper: added Scenario test case structure
74. 12b5593 .scripts/gotest.bash: renamed gotest.bash to gotestCI.bash
75. e16900e .scripts: added gosetup.bash for easy go setup on UNIX system
76. 5b555a0 os/flags: removed flags package
77. 91ad9ca os: added args package for parsing command line arguments
78. def2eab strings: added strhelper package
79. 3192267 os/term: added Size() to get terminal row and column sizes
80. 5240578 testing/thelper: added ExpectSameStrings(...) function to THelper
81. e5da53d testing/thelper: refactored thelper test suites
82. d5ab4cb testing/thelper: added ExpectSameStringSlices(...) assertion function
83. 14b4c99 go.mod: add replace clause for relative local repository reference
84. 8635d01 root: removed doc.go and doc_test.go for unused package directory
85. abfffa8 root: added doc_test.go
86. f8ea976 os/file/filehelper: added FileHasKeywords(...) function
87. e433f8a os/file/filehelper: added FileExists function and small refactoring
88. d7985e5 README.md: added staging column to branch tables
89. 0a7b828 os/term: set PrepareCommand to use 5 seconds timeout
90. 3c7d5eb os/term: set default terminal root value and useShortTimeout
91. 6c7d770 testing/thelper: removed FailKeyword from each assertions
92. a39036f os: added terminal package (term) into repository
93. 8f34b32 .scripts/gotest.bash: added ruby-regex parser tag
94. de8a17e os/file/filehelper: changed deepCompare test cases to use badFilepath
95. 9825a41 testing/thelper: styled QuietMode output
96. f6c130c testing/thelper: styled reporting format to a sensible output
97. 9cc4a3f os: renamed cmds package to term package
98. 2896709 os: deleted osfiles package
99. 3b0a601 os/file: added filehelper package
100. 2e0d3e0 testing/thelper: added expectUIDCorrectness assertion
101. 0a9314d .scripts/gotest.bash: fixed bad return value for any fail cases
102. 23df554 testing: added thelper into the repository
103. 7d8b251 secure: remove unmaintained secure security directory
104. f4d0ddb README.md: fixed a typo <br/> problem with sponsorship
105. aa2cd1b crypto/manager: adds cryptography manager
106. 0e19084 crypto/manager/internal/ciphers: added data returns for Verify
107. 21687c5 crypto/manager/internal/ciphers: added keyProperties function
108. 6bec116 crypto/manager/_internals/errors.go: reworded cipher specific message
109. 31e6722 crypot: created manager package placeholder
110. d12bbb9 crypto/internal/ciphers: added Argon2
111. 3df8fa2 crypto/internal/ciphers: refactored to a unified testsuite codes
112. 6f8b419 crypto/internal: added ciphers internal package for crypto
113. 8821eb0 os/cmds/command_test.go: refactored test-driven table for TestWait
114. fa10796 os/cmds/terminal.go: added IsExist, FileHasKeywords LFS functions

# Version 2.1.0
## Fri, 29 Mar 2019 11:44:27 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 646a877 os/cmds/terminal.go: added Exit function

# Version 2.0.0
## Thu, 28 Mar 2019 20:35:26 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 397aca5 .gitignore: added test artifacts into the .gitignore
2. 3c211fa os: added cmds package into cerigo
3. 1261d2f os/flags/flag*.go: added Overrides subroutine feature
4. 96c76bd .gitlab-ci.yml: updated GO_VERSION to 1.12.1.
5. 7a1d490 .scripts/gotest.bash: increase timeout for large file testing
6. 4673068 email/providers/mailgun: removed mailgun 3rd party provider

# Version 1.1.0
## Fri, 18 Jan 2019 16:31:15 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 7131576 os/flags: refactored to use go interface feature

# Version 1.0.1
## Fri, 18 Jan 2019 12:09:50 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 76b6a9a README.md: updated README.md to using go module
2. 501a9a2 .scripts/gotest.bash: added goreportcard.com calls
3. 2876a69 root: added gotest.bash script for executing local test run

# Version 1.0.0
## Fri, 18 Jan 2019 09:43:05 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. e4c4c76 .configs: rename _configs directory to .configs
2. 7719f46 .scripts: renamed _scripts to use .scripts
3. 5221374 go.sum: updated go.sum
4. b040b4b .fennec: added build-essentials into linux core
5. 59bba38 root: updated import paths to match go modules
6. 6cad04b email/providers/mailgun/v2: updated to match vendor requirement
7. 5f6caa9 email: updated for vendor requirement
8. 77f0a22 .fennec: added git into go installation
9. 91a9db7 .fennec: shifted fennec into repo configuration directory
10. ec99d30 root: implementing go module as instructed
11. df3d039 _scripts: renamed repo tool folder scripts to _scripts
12. f6c6204 secure/cookies: refactored to meet vendor requirement
13. 377af78 secure/crypto/password: updated to meet new securekey interfaces
14. 5a7ac92 secure/crypto/aes-gcm: updated based on new securekey format
15. ef84f6b secure/crypto/key: refactored to use Go interface
16. 8f32d7d secure/crypto/aes-gcm: refactored for using Go interface
17. 7d12e21 secure/crypto/password: updated for vendor requirement
18. 7c9a387 secure/crypto/hashing: updated for vendor requirement
19. 40e1311 secure/crypto/key: updated for vendor deployment
20. ae2031f secure/crypto/aes-gcm: refactored to be vendor directory friendly
21. e000e71 _configs: move all repository configurations under _configs folder
22. aa5c567 fennec: added fennec GitLab CI automation library
23. 6a3f627 manta: added manta configurations files
24. 71937e2 scripts/manta: added manta packaging automation scripts
25. cb84286 README.md: fix release badge typo
26. 1b9385c secure: ported shion-go libraries into cerigo
27. 42046c2 os: ported shion-go libraries into cerigo
28. 6cc0926 email: ported shion library into cerigo
