package thelper

import (
	"testing"
)

func TestCalculateTimeLimits(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperCalculateTimeLimits {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)
		start, duration, ranges := s.prepareTimeLimits()

		// test
		min, max := th.CalculateTimeLimits(start, duration, ranges)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertTimeLimits(t, min, max, start, duration, ranges)
		s.logf(t, th, min, max)
	}
}
