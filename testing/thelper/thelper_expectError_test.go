package thelper

import (
	"testing"
)

func TestExpectError(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectError {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)

		// test
		err, ok := s.inSubject.(error)
		if !ok {
			err = nil
		}

		ret := th.ExpectError(err, s.inExpect)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertExistence(t, th, ret)
		s.logf(t, th, ret, err)
	}
}
