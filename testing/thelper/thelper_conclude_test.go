package thelper

import (
	"testing"
)

func TestConclude(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperConclude {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		_, _, m := s.prepareMessages()
		s.prepareContainers(th, m)
		s.prepareMissingContainers(th)

		// test
		th.Conclude()

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertConclusionVerdicts(t, th)
		s.logf(t, th, 0, "N/A")
	}
}
