package thelper

import (
	"testing"
)

func TestExpectUIDCorrectness(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectUIDCorrectness {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)
		index, uid, startFromZero := s.prepareUIDSample()

		// test
		err, ok := s.inSubject.(error)
		if !ok {
			err = nil
		}

		ret := th.ExpectUIDCorrectness(index, uid, startFromZero)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertExpectUIDCorrectness(t, th, ret)
		s.assertExistence(t, th, ret)
		s.logf(t, th, ret, err)
	}
}
