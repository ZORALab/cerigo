package thelper

import (
	"testing"
)

func TestLogScenario(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperLogScenario {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareMissingContainers(th)
		scenario, data := s.prepareScenario()

		// test
		th.LogScenario(scenario, data)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertLogScenario(t, th, data)
		s.logf(t, th, scenario, data)
	}
}
