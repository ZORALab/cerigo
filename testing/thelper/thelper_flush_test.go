package thelper

import (
	"testing"
)

func TestFlush(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperFlush {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		_, _, m := s.prepareMessages()
		s.prepareContainers(th, m)
		s.prepareMissingContainers(th)

		// test
		th.Flush(s.inFlushType)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertEmptyContainers(t, th)
		s.assertFilledContainers(t, th)
		s.logf(t, th, 0, "N/A")
	}
}
