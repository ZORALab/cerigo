package thelper

import (
	"testing"
)

func TestExpectSameFloat32(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectSameFloat32 {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		labelA, sA, labelB, sB := s.prepareFloat32()

		// test
		ret := th.ExpectSameFloat32(labelA, sA, labelB, sB)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertReturnValue(t, th, ret)
		s.logf(t, th, ret, "N/A")
	}
}
