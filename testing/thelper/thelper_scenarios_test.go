package thelper

import (
	"fmt"
)

func testTHelperScenarios() []testTHelperScenario {
	unknownType := uint(0)
	goodLabel := "mySubjectName"
	emptyLabel := ""
	goodByte := []byte{0xDE, 0xAD, 0xBE, 0xEF}
	emptyByte := []byte{}
	passedReply := 0
	badExistReply := 1
	errorObject := fmt.Errorf("just a test error")

	return []testTHelperScenario{
		{
			uid:      1,
			testType: testTHelperErrorf,
			description: `errorf should be able to process error
message with arguments and save it into its failed messages container`,
			switches: map[string]bool{},
		}, {
			uid:      2,
			testType: testTHelperErrorf,
			description: `errorf should be able to handle missing
argument messages and save it into its failed messages container`,
			switches: map[string]bool{
				missingArguments: true,
			},
		}, {
			uid:      3,
			testType: testTHelperErrorf,
			description: `errorf should be able to handle missing
messages with format and save it into its failed messages container`,
			switches: map[string]bool{
				missingMessage: true,
			},
		}, {
			uid:      4,
			testType: testTHelperErrorf,
			description: `errorf should be able to handle missing
container automatically`,
			switches: map[string]bool{
				missingContainer: true,
			},
		}, {
			uid:      5,
			testType: testTHelperLogf,
			description: `logf should be able to process log
message with arguments and save it into its log messges container`,
			switches: map[string]bool{},
		}, {
			uid:      6,
			testType: testTHelperLogf,
			description: `logf should be able to handle missing
argument messages and save it into its log messages container`,
			switches: map[string]bool{
				missingArguments: true,
			},
		}, {
			uid:      7,
			testType: testTHelperLogf,
			description: `logf should be able to handle missing
messages with format and save it into its log messages container`,
			switches: map[string]bool{
				missingMessage: true,
			},
		}, {
			uid:      8,
			testType: testTHelperLogf,
			description: `logf should be able to handle missing
container automatically`,
			switches: map[string]bool{
				missingContainer: true,
			},
		}, {
			uid:      9,
			testType: testTHelperFlush,
			description: `Flush should be able to empty all its
containers`,
			switches:    map[string]bool{},
			inFlushType: AllType,
		}, {
			uid:      10,
			testType: testTHelperFlush,
			description: `Flush AllType should be able to handle
missing containers`,
			switches: map[string]bool{
				missingContainer: true,
			},
			inFlushType: AllType,
		}, {
			uid:      11,
			testType: testTHelperFlush,
			description: `Flush should be able to empty all its
containers`,
			switches:    map[string]bool{},
			inFlushType: FailedType,
		}, {
			uid:      12,
			testType: testTHelperFlush,
			description: `Flush AllType should be able to handle
missing containers`,
			switches: map[string]bool{
				missingContainer: true,
			},
			inFlushType: FailedType,
		}, {
			uid:      13,
			testType: testTHelperFlush,
			description: `Flush should be able to empty all its
containers`,
			switches:    map[string]bool{},
			inFlushType: LogType,
		}, {
			uid:      14,
			testType: testTHelperFlush,
			description: `Flush AllType should be able to handle
missing containers`,
			switches: map[string]bool{
				missingContainer: true,
			},
			inFlushType: LogType,
		}, {
			uid:      15,
			testType: testTHelperFlush,
			description: `Flush should be able to retain all
containers when given an unknown type`,
			switches:    map[string]bool{},
			inFlushType: unknownType,
		}, {
			uid:      16,
			testType: testTHelperFlush,
			description: `Flush should be able to retain all
containers when they are missing and given an unknown type`,
			switches: map[string]bool{
				missingContainer: true,
			},
			inFlushType: unknownType,
		}, {
			uid:      17,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
both log and error messages`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
		}, {
			uid:      18,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for both log and error messages`,
			switches: map[string]bool{
				missingController: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
		}, {
			uid:      19,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for log only messages`,
			switches: map[string]bool{
				missingFailed: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
		}, {
			uid:      20,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for failed only messages`,
			switches: map[string]bool{
				missingLog: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
		}, {
			uid:      21,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for no message`,
			switches: map[string]bool{
				missingLog:    true,
				missingFailed: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
		}, {
			uid:      22,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
both log and error messages`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: true,
		}, {
			uid:      23,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for both log and error messages`,
			switches: map[string]bool{
				missingController: true,
			},
			inFlushType: AllType,
			inQuietMode: true,
		}, {
			uid:      24,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for log only messages`,
			switches: map[string]bool{
				missingFailed: true,
			},
			inFlushType: AllType,
			inQuietMode: true,
		}, {
			uid:      25,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for failed only messages`,
			switches: map[string]bool{
				missingLog: true,
			},
			inFlushType: AllType,
			inQuietMode: true,
		}, {
			uid:      26,
			testType: testTHelperConclude,
			description: `Conclude should be operate properly with
missing controller for no message`,
			switches: map[string]bool{
				missingLog:    true,
				missingFailed: true,
			},
			inFlushType: AllType,
			inQuietMode: true,
		}, {
			uid:      27,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given object with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   goodByte,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      28,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given object without bad label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     emptyLabel,
			inSubject:   goodByte,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      29,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given empty object with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   emptyByte,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      30,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given object with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   goodByte,
			inExpect:    false,
			outValue:    badExistReply,
		}, {
			uid:      31,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given object without bad label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     emptyLabel,
			inSubject:   goodByte,
			inExpect:    false,
			outValue:    badExistReply,
		}, {
			uid:      32,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given empty object with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   emptyByte,
			inExpect:    false,
			outValue:    badExistReply,
		}, {
			uid:      33,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given nil object with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    false,
			outValue:    passedReply,
		}, {
			uid:      34,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given nil object with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      35,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given true boolean with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   true,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      36,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given true boolean  with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   false,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      37,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given false boolean with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   false,
			inExpect:    false,
			outValue:    passedReply,
		}, {
			uid:      38,
			testType: testTHelperExpectExists,
			description: `Expect Exists is able to operate properly
with given false boolean with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   false,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      39,
			testType: testTHelperExpectError,
			description: `Expect Error is able to operate properly
with a given error with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   errorObject,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      40,
			testType: testTHelperExpectError,
			description: `Expect Error is able to operate properly
with given error with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   errorObject,
			inExpect:    false,
			outValue:    badExistReply,
		}, {
			uid:      41,
			testType: testTHelperExpectError,
			description: `Expect Error is able to operate properly
with a given nil with good label and good expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    false,
			outValue:    passedReply,
		}, {
			uid:      42,
			testType: testTHelperExpectError,
			description: `Expect Error is able to operate properly
with given nil with good label and bad expectation`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:         43,
			testType:    testTHelperSnapTime,
			description: `SnapTime is able to operate properly`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      44,
			testType: testTHelperCalculateDuration,
			description: `TimeDuration is able to operate properly
when given proper start time and proper stop time`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      45,
			testType: testTHelperCalculateDuration,
			description: `TimeDuration is able to operate properly
when given missing start time and proper stop time`,
			switches: map[string]bool{
				missingStartTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      46,
			testType: testTHelperCalculateDuration,
			description: `TimeDuration is able to operate properly
when given proper start time and missing stop time`,
			switches: map[string]bool{
				missingStopTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      47,
			testType: testTHelperCalculateDuration,
			description: `TimeDuration is able to operate properly
when given missing start time and missing stop time`,
			switches: map[string]bool{
				missingStartTime: true,
				missingStopTime:  true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      48,
			testType: testTHelperCalculateDuration,
			description: `TimeDuration is able to operate properly
when given improper start time and improper stop time`,
			switches: map[string]bool{
				badTimePlacement: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      49,
			testType: testTHelperCalculateTimeLimits,
			description: `CalculateTimeLimits is able to operate
properly when given a proper start time, proper duration, and proper ranges`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      50,
			testType: testTHelperCalculateTimeLimits,
			description: `CalculateTimeLimits is able to operate
properly when given a proper start time, proper duration, and bad ranges`,
			switches: map[string]bool{
				badRange: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      51,
			testType: testTHelperCalculateTimeLimits,
			description: `CalculateTimeLimits is able to operate
properly when given a proper start time, bad duration, and proper ranges`,
			switches: map[string]bool{
				badDuration: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      52,
			testType: testTHelperCalculateTimeLimits,
			description: `CalculateTimeLimits is able to operate
properly when given a proper start time, bad duration, and bad ranges`,
			switches: map[string]bool{
				badDuration: true,
				badRange:    true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      53,
			testType: testTHelperCalculateTimeLimits,
			description: `CalculateTimeLimits is able to operate
properly when given a missing start time, proper duration, and proper ranges`,
			switches: map[string]bool{
				missingStartTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      54,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a min timestamp, a max timestamp`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      55,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a min missing timestamp, a max timestamp`,
			switches: map[string]bool{
				missingMinTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      56,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a min timestamp, a missing max timestamp`,
			switches: map[string]bool{
				missingMaxTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      57,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a missing min timestamp, a missing max
timestamp`,
			switches: map[string]bool{
				missingMinTime: true,
				missingMaxTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      58,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a missing stop time, a min timestamp, a max timestamp`,
			switches: map[string]bool{
				missingStopTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      59,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a missing stop time, a missing min timestamp, a max
timestamp`,
			switches: map[string]bool{
				missingStopTime: true,
				missingMinTime:  true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      60,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a missing stop time, a min timestamp, a missing max
timestamp`,
			switches: map[string]bool{
				missingStopTime: true,
				missingMaxTime:  true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      61,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a missing stop time, a missing min timestamp, a missing max
timestamp`,
			switches: map[string]bool{
				missingStopTime: true,
				missingMinTime:  true,
				missingMaxTime:  true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      62,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a bad min timestamp, a max timestamp`,
			switches: map[string]bool{
				badMinTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      63,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a min timestamp, a bad max timestamp`,
			switches: map[string]bool{
				badMaxTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      64,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a missing min timestamp, a bad max timestamp`,
			switches: map[string]bool{
				missingMinTime: true,
				badMaxTime:     true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      65,
			testType: testTHelperExpectInTime,
			description: `ExpectInTime is able to operate
properly when given a stop time, a bad min timestamp, a missing max timestamp`,
			switches: map[string]bool{
				badMinTime:     true,
				missingMaxTime: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      66,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a proper byteA, a proper labelB, and a
proper byteB`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      67,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a bad labelA, a proper byteA, a proper labelB, and a
proper byteB`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      68,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a proper byteA, a bad labelB, and a
proper byteB`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      69,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a bad labelA, a proper byteA, a bad labelB, and a
proper byteB`,
			switches: map[string]bool{
				badLabelA: true,
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      70,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a bad byteA, a proper labelB, and a
proper byteB`,
			switches: map[string]bool{
				badByteA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      71,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a proper byteA, a proper labelB, and a
bad byteB`,
			switches: map[string]bool{
				badByteA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      72,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a bad byteA, a proper labelB, and a
bad byteB`,
			switches: map[string]bool{
				badByteA: true,
				badByteB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      73,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a empty byteA, a proper labelB, and a
proper byteB`,
			switches: map[string]bool{
				missingByteA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      74,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a proper byteA, a proper labelB, and a
missing byteB`,
			switches: map[string]bool{
				missingByteB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      75,
			testType: testTHelperSameBytesSlices,
			description: `ExpectSameBytesSlices is able to operate
properly when given a proper labelA, a missing byteA, a proper labelB, and a
missing byteB`,
			switches: map[string]bool{
				missingByteA: true,
				missingByteB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      76,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a proper index, a proper uid`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      77,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a mistmatched index, a proper uid`,
			switches: map[string]bool{
				mismatchedIndex: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      78,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a proper index, a mismatched uid`,
			switches: map[string]bool{
				mismatchedUID: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      79,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a mistmatched index, a mismatched uid`,
			switches: map[string]bool{
				mismatchedUID:   true,
				mismatchedIndex: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      80,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a proper index, a proper uid, a startZero flag`,
			switches: map[string]bool{
				beginFromZero: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      81,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a mistmatched index, a proper uid, a startZero flag`,
			switches: map[string]bool{
				beginFromZero:   true,
				mismatchedIndex: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      82,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a proper index, a mismatched uid, a startZero flag`,
			switches: map[string]bool{
				beginFromZero: true,
				mismatchedUID: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      83,
			testType: testTHelperExpectUIDCorrectness,
			description: `ExpectUIDCorrectness is able to operate
properly when given a mistmatched index, a mismatched uid, a startZero flag`,
			switches: map[string]bool{
				beginFromZero:   true,
				mismatchedIndex: true,
				mismatchedUID:   true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      84,
			testType: testTHelperErrorf,
			description: `Errof is able to operate properly when
having a empty FailKeyword`,
			switches: map[string]bool{
				emptyFailKeyword: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      85,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. proper string slice A
3. proper label B
4. proper string slice B
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      86,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. bad label A
2. proper string slice A
3. proper label B
4. proper string slice B
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      87,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. proper string slice A
3. bad label B
4. proper string slice B
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      88,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. bad string slice A
3. proper label B
4. proper string slice B
`,
			switches: map[string]bool{
				badStringA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      89,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. proper string slice A
3. proper label B
4. bad string slice B
`,
			switches: map[string]bool{
				badStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      90,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. missing string slice A
3. proper label B
4. proper string slice B
`,
			switches: map[string]bool{
				missingStringA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      91,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. proper string slice A
3. proper label B
4. missing string slice B
`,
			switches: map[string]bool{
				missingStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      92,
			testType: testTHelperSameStringSlices,
			description: `
ExpectSameStringSlices is able to operate properly when having:
1. proper label A
2. missing string slice A
3. proper label B
4. missing string slice B
`,
			switches: map[string]bool{
				missingStringA: true,
				missingStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      93,
			testType: testTHelperSameStrings,
			description: `
ExpectSameStrings is able to operate properly when having:
1. proper label A
2. proper string A
3. proper label B
4. proper string B
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      94,
			testType: testTHelperSameStrings,
			description: `
ExpectSameStrings is able to operate properly when having:
1. bad label A
2. proper string A
3. proper label B
4. proper string B
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      95,
			testType: testTHelperSameStrings,
			description: `
ExpectSameStrings is able to operate properly when having:
1. proper label A
2. proper string A
3. bad label B
4. proper string B
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      96,
			testType: testTHelperSameStrings,
			description: `
ExpectSameStrings is able to operate properly when having:
1. proper label A
2. bad string A
3. proper label B
4. proper string B
`,
			switches: map[string]bool{
				badStringA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      97,
			testType: testTHelperSameStrings,
			description: `
ExpectSameStrings is able to operate properly when having:
1. proper label A
2. proper string A
3. proper label B
4. bad string B
`,
			switches: map[string]bool{
				badStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      98,
			testType: testTHelperSameStrings,
			description: `
ExpectSameStrings is able to operate properly when having:
1. proper label A
2. bad string A
3. proper label B
4. bad string B
`,
			switches: map[string]bool{
				badStringA: true,
				badStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      99,
			testType: testTHelperLogScenario,
			description: `
LogScenario is able to operate properly when having:
1. proper switches
2. proper data input
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      100,
			testType: testTHelperLogScenario,
			description: `
LogScenario is able to operate properly when having:
1. empty switches
2. proper data input
`,
			switches: map[string]bool{
				emptyScenarioSwitches: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      101,
			testType: testTHelperLogScenario,
			description: `
LogScenario is able to operate properly when having:
1. missing switches
2. proper data input
`,
			switches: map[string]bool{
				missingScenarioSwitches: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      102,
			testType: testTHelperLogScenario,
			description: `
LogScenario is able to operate properly when having:
1. proper switches
2. empty data input
`,
			switches: map[string]bool{
				emptyScenarioData: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      103,
			testType: testTHelperLogScenario,
			description: `
LogScenario is able to operate properly when having:
1. proper switches
2. missing data input
`,
			switches: map[string]bool{
				missingScenarioData: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      104,
			testType: testTHelperLogScenario,
			description: `
LogScenario is able to operate properly when having:
1. empty switches
2. empty data input
`,
			switches: map[string]bool{
				emptyScenarioSwitches: true,
				emptyScenarioData:     true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      105,
			testType: testTHelperExpectSameBool,
			description: `
ExpectBool is able to operate properly when having:
1. proper label A
2. bool A is true
3. proper label B
4. bool B is true
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      106,
			testType: testTHelperExpectSameBool,
			description: `
ExpectBool is able to operate properly when having:
1. bad label A
2. bool A is true
3. proper label B
4. bool B is true
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      107,
			testType: testTHelperExpectSameBool,
			description: `
ExpectBool is able to operate properly when having:
1. proper label A
2. bool A is true
3. bad label B
4. bool B is true
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      108,
			testType: testTHelperExpectSameBool,
			description: `
ExpectBool is able to operate properly when having:
1. bad label A
2. bool A is false
3. proper label B
4. bool B is true
`,
			switches: map[string]bool{
				badBoolA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      109,
			testType: testTHelperExpectSameBool,
			description: `
ExpectBool is able to operate properly when having:
1. bad label A
2. bool A is true
3. proper label B
4. bool B is false
`,
			switches: map[string]bool{
				badBoolB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      110,
			testType: testTHelperExpectSameBool,
			description: `
ExpectBool is able to operate properly when having:
1. bad label A
2. bool A is false
3. proper label B
4. bool B is false
`,
			switches: map[string]bool{
				badBoolA: true,
				badBoolB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      111,
			testType: testTHelperExpectStringHasKeywords,
			description: `
ExpectStringHasKeywords is able to operate properly when having:
1. proper label A
2. proper string A
3. proper label B
4. proper string B
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      112,
			testType: testTHelperExpectStringHasKeywords,
			description: `
ExpectStringHasKeywords is able to operate properly when having:
1. bad label A
2. proper string A
3. proper label B
4. proper string B
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      113,
			testType: testTHelperExpectStringHasKeywords,
			description: `
ExpectStringHasKeywords is able to operate properly when having:
1. proper label A
2. proper string A
3. bad label B
4. proper string B
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      114,
			testType: testTHelperExpectStringHasKeywords,
			description: `
ExpectStringHasKeywords is able to operate properly when having:
1. proper label A
2. bad string A
3. proper label B
4. proper string B
`,
			switches: map[string]bool{
				badStringA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      115,
			testType: testTHelperExpectStringHasKeywords,
			description: `
ExpectStringHasKeywords is able to operate properly when having:
1. proper label A
2. proper string A
3. proper label B
4. bad string B
`,
			switches: map[string]bool{
				badStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      116,
			testType: testTHelperSameStrings,
			description: `
ExpectStringHasKeywords is able to operate properly when having:
1. proper label A
2. bad string A
3. proper label B
4. bad string B
`,
			switches: map[string]bool{
				badStringA: true,
				badStringB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      117,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. proper label A
2. proper float32 A
3. proper label B
4. proper float32 B
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      118,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. proper label A
2. bad float32 A
3. proper label B
4. proper float32 B
`,
			switches: map[string]bool{
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      119,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. proper label A
2. proper float32 A
3. proper label B
4. bad float32 B
`,
			switches: map[string]bool{
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      120,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. bad label A
2. proper float32 A
3. proper label B
4. proper float32 B
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      121,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. proper label A
2. proper float32 A
3. proper label B
4. bad float32 B
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      122,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. bad label A
2. bad float32 A
3. proper label B
4. proper float32 B
`,
			switches: map[string]bool{
				badLabelA:   true,
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      123,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. bad label A
2. proper float32 A
3. proper label B
4. bad float32 B
`,
			switches: map[string]bool{
				badLabelA:   true,
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      124,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. proper label A
2. bad float32 A
3. bad label B
4. proper float32 B
`,
			switches: map[string]bool{
				badLabelB:   true,
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      125,
			testType: testTHelperExpectSameFloat32,
			description: `
ExpectSameFloat32 is able to operate properly when having:
1. proper label A
2. proper float32 A
3. bad label B
4. bad float32 B
`,
			switches: map[string]bool{
				badLabelB:   true,
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      126,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. proper label A
2. proper float64 A
3. proper label B
4. proper float64 B
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      127,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. proper label A
2. bad float64 A
3. proper label B
4. proper float64 B
`,
			switches: map[string]bool{
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      128,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. proper label A
2. proper float64 A
3. proper label B
4. bad float64 B
`,
			switches: map[string]bool{
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      129,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. bad label A
2. proper float64 A
3. proper label B
4. proper float64 B
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      130,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. proper label A
2. proper float64 A
3. proper label B
4. bad float64 B
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      131,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. bad label A
2. bad float64 A
3. proper label B
4. proper float64 B
`,
			switches: map[string]bool{
				badLabelA:   true,
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      132,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. bad label A
2. proper float64 A
3. proper label B
4. bad float64 B
`,
			switches: map[string]bool{
				badLabelA:   true,
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      133,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. proper label A
2. bad float64 A
3. bad label B
4. proper float64 B
`,
			switches: map[string]bool{
				badLabelB:   true,
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      134,
			testType: testTHelperExpectSameFloat64,
			description: `
ExpectSameFloat64 is able to operate properly when having:
1. proper label A
2. proper float64 A
3. bad label B
4. bad float64 B
`,
			switches: map[string]bool{
				badLabelB:   true,
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      135,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. proper big float A
3. proper label B
4. proper big float B
`,
			switches:    map[string]bool{},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      136,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. bad big float A
3. proper label B
4. proper big float B
`,
			switches: map[string]bool{
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      137,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. proper big float A
3. proper label B
4. bad big float B
`,
			switches: map[string]bool{
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      138,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. bad label A
2. proper big float A
3. proper label B
4. proper big float B
`,
			switches: map[string]bool{
				badLabelA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      139,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. proper big float A
3. proper label B
4. bad big float B
`,
			switches: map[string]bool{
				badLabelB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		}, {
			uid:      140,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. bad label A
2. bad big float A
3. proper label B
4. proper big float B
`,
			switches: map[string]bool{
				badLabelA:   true,
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      141,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. bad label A
2. proper big float A
3. proper label B
4. bad big float B
`,
			switches: map[string]bool{
				badLabelA:   true,
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      142,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. bad big float A
3. bad label B
4. proper big float B
`,
			switches: map[string]bool{
				badLabelB:   true,
				badSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      143,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. proper big float A
3. bad label B
4. bad big float B
`,
			switches: map[string]bool{
				badLabelB:   true,
				badSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      144,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. proper big float A
3. proper label B
4. missing big float B
`,
			switches: map[string]bool{
				missingSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      145,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. missing big float A
3. proper label B
4. proper big float B
`,
			switches: map[string]bool{
				missingSubjectA: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    badExistReply,
		}, {
			uid:      146,
			testType: testTHelperExpectSameBigFloat,
			description: `
ExpectSameBigFloat is able to operate properly when having:
1. proper label A
2. missing big float A
3. proper label B
4. missing big float B
`,
			switches: map[string]bool{
				missingSubjectA: true,
				missingSubjectB: true,
			},
			inFlushType: AllType,
			inQuietMode: false,
			inLabel:     goodLabel,
			inSubject:   nil,
			inExpect:    true,
			outValue:    passedReply,
		},
	}
}
