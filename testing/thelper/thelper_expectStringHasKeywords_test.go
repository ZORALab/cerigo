package thelper

import (
	"testing"
)

func TestExpectStringHasKeywords(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectStringHasKeywords {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)
		labelA, sA, labelB, sB := s.prepareStrings()

		// test
		ret := th.ExpectStringHasKeywords(labelA, sA, labelB, sB)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertReturnValue(t, th, ret)
		s.logf(t, th, ret, "N/A")
	}
}
