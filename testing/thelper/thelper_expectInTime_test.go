package thelper

import (
	"testing"
)

func TestExpectInTime(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectInTime {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)
		subject, min, max := s.prepareTimeSet()

		// test
		ret := th.ExpectInTime(subject, min, max)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertReturnValue(t, th, ret)
		s.logf(t, th, ret, subject)
	}
}
