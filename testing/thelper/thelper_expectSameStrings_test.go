package thelper

import (
	"testing"
)

func TestExpectSameStrings(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperSameStrings {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)
		labelA, sA, labelB, sB := s.prepareStrings()

		// test
		ret := th.ExpectSameStrings(labelA, sA, labelB, sB)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertReturnValue(t, th, ret)
		s.logf(t, th, ret, "N/A")
	}
}
