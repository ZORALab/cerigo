package thelper

import (
	"testing"
)

func TestLogf(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperLogf {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareMissingContainers(th)
		message, arguments, final := s.prepareMessages()

		// test
		th.Logf(message, arguments...)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertLogfMessage(t, th, final)
		s.logf(t, th, message, arguments)
	}
}
