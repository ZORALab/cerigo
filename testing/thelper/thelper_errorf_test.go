package thelper

import (
	"testing"
)

func TestErrorf(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperErrorf {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareMissingContainers(th)
		message, arguments, final := s.prepareMessages()

		// test
		th.Errorf(message, arguments...)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertErrorfMessage(t, th, final)
		s.logf(t, th, message, arguments)
	}
}
