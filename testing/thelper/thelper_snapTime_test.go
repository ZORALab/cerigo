package thelper

import (
	"testing"
)

func TestExpectSnapTime(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperSnapTime {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)

		// test
		data := th.SnapTime()

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertTimestamp(t, data)
		s.logf(t, th, data, "N/A")
	}
}
