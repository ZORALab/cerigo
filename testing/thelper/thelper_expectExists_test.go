package thelper

import (
	"testing"
)

func TestExpectExists(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectExists {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)

		// test
		ret := th.ExpectExists(s.inLabel, s.inSubject, s.inExpect)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertExistence(t, th, ret)
		s.logf(t, th, ret, "N/A")
	}
}
