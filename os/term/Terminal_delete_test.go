package term

import (
	"testing"
)

func TestDelete(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testDelete {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		label, _, _ := s.prepareCommand(x)

		// test
		x.Delete(label)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertDeletedCommand(th, x, label)
		s.log(th, map[string]interface{}{
			"terminal": x,
			"inLabel":  label,
		})
		th.Conclude()
	}
}
