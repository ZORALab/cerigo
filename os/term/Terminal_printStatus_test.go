package term

import (
	"testing"
)

func TestPrintStatus(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testPrintStatus {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		statusID, format, arguments, final := s.preparePrintStatus()

		// test
		x.PrintStatus(statusID, format, arguments...)

		// assert
		message := s.getSTDERRString(x)
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertPrintStatus(th, x, message, final)
		s.log(th, map[string]interface{}{
			"terminal":     x,
			"inStatusID":   statusID,
			"inFormat":     format,
			"inArgument":   arguments,
			"final string": final,
			"got string":   message,
		})
		th.Conclude()
	}
}
