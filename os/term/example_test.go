package term

import (
	"fmt"
)

func Example() {
	var t *Terminal
	var c *Command
	var err error
	var stdout, stderr []byte

	// 1. Create the terminal with the TerminalType
	t = NewTerminal(BASHTerminal)

	// 2. To check the terminal has root permission
	if t.IsRoot() {
		fmt.Printf("I have root permission")
	}

	// 3. To quickly execute a command, with timeout of 5 seconds
	//         cmd  , s   ms     ns
	stdout, stderr, err = t.Execute("ls /", 5*1000*1000)
	if err != nil {
		fmt.Printf("handle error: %v\n", err)
	}

	fmt.Printf("stdout: %v\nstderr: %v\n", stdout, stderr)

	// 4. To add a command into the terminal
	err = t.Add("myCommandLabel", "ls /", 5*1000*1000)
	if err != nil {
		fmt.Printf("handle error: %v\n", err)
	}

	// 5. To run the command synchonously
	c, err = t.Run("myCommandLabel", true)
	if err != nil {
		fmt.Printf("handle error: %v\n", err)
	}

	// 6. To read output from the command
	stdout, stderr = c.ReadOutput()

	fmt.Printf("stdout: %v\nstderr: %v\n", stdout, stderr)

	// 7. To run the command asynchonously
	c, err = t.Run("myCommandLabel", false)
	if err != nil {
		fmt.Printf("handle error: %v\n", err)
	}

	// 8. To wait for asynchonous command execution to complete
	c.Wait()
}
