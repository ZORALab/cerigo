package term

import (
	"testing"
)

func TestRun(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testRun {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		label, mustWait, hasError := s.prepareRunCommand(x)

		// test
		c, err := x.Run(label, mustWait)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectError(err, hasError)
		s.log(th, map[string]interface{}{
			"terminal":    x,
			"inLabel":     label,
			"inMustWait":  mustWait,
			"expectError": hasError,
			"outCommand":  c != nil,
			"outError":    err,
		})
		th.Conclude()
	}
}
