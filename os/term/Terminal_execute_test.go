package term

import (
	"testing"
)

func TestExecute(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testExecute {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		_, command, hasError := s.prepareCommand(x)
		timeout, hasError := s.prepareTimeout(hasError)

		// test
		stdout, stderr, err := x.Execute(command, timeout)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectError(err, hasError)
		s.assertOutput(th, stdout, stderr, hasError)
		s.log(th, map[string]interface{}{
			"terminal":    x,
			"expectError": hasError,
			"outError":    err,
			"stdout":      len(stdout),
			"stderr":      len(stderr),
		})
		th.Conclude()
	}
}
