//+build windows !linux !darwin

package term

const (
	testBadCommand        = "noideaAtall -a -r"
	testCommandLabel      = "ping"
	testBadCommandLabel   = "blablabla"
	testEmptyCommand      = ""
	testEmptyCommandLabel = ""
	testLongCommand       = "ping -n 6 127.0.0.1 > nul"
	testShortCommand      = "ping -n 1 127.0.0.1 > nul"
)
