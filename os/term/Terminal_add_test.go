package term

import (
	"testing"
)

func TestAdd(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testAdd {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		label, command, hasError := s.prepareCommand(x)
		timeout, hasError := s.prepareTimeout(hasError)

		// test
		err := x.Add(label, command, timeout)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectError(err, hasError)
		s.assertCommandExists(th, x, label)
		s.log(th, map[string]interface{}{
			"terminal":     x,
			"expect error": hasError,
			"got error":    err,
			"inLabel":      label,
			"inCommand":    command,
			"inTimeout":    timeout,
		})
		th.Conclude()
	}
}
