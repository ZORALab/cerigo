package term

import (
	"testing"
)

func TestGet(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testGet {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		label, _, _ := s.prepareCommand(x)

		// test
		c := x.Get(label)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertGotCommand(th, c)
		s.log(th, map[string]interface{}{
			"terminal": x,
			"inLabel":  label,
			"command":  c,
		})
		th.Conclude()
	}
}
