package term

import (
	"testing"
)

func TestPrintf(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testPrintf {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		format, arguments, final := s.preparePrintf()

		// test
		x.Printf(format, arguments...)

		// assert
		message := s.getSTDOUTString(x)
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertPrintf(th, x, message, final)
		s.log(th, map[string]interface{}{
			"terminal":     x,
			"inFormat":     format,
			"inArgument":   arguments,
			"final string": final,
			"got string":   message,
		})
		th.Conclude()
	}
}
