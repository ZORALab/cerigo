package term

import (
	"testing"
)

func TestSize(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testSize {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)

		// test
		row, column := x.Size()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertSize(th, x, row, column)
		s.log(th, map[string]interface{}{
			"terminal": x,
			"row":      row,
			"column":   column,
		})
		th.Conclude()
	}
}
