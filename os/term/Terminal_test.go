package term

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	defaultRow    = uint(12345)
	defaultColumn = uint(67890)
)

const (
	testAdd         = "testAdd"
	testNewTerminal = "testNewTerminal"
	testPrintStatus = "testPrintStatus"
	testPrintf      = "testPrintf"
	testRegister    = "testRegister"
	testGet         = "testGet"
	testUpdate      = "testUpdate"
	testDelete      = "testDelete"
	testRun         = "testRun"
	testExecute     = "testExecute"
	testIsRoot      = "testIsRoot"
	testSize        = "testSize"
)

const (
	doNotWait           = "doNotWait"
	missingArguments    = "missingArguments"
	missingFormat       = "missingFormat"
	missingSTDERR       = "missingSTDERR"
	missingSTDOUT       = "missingSTDOUT"
	preOccupyCommand    = "preOccupyCommand"
	setRoot             = "setRoot"
	unknownTerminalType = "unknownTerminalType"
	useBadCommand       = "useBadCommand"
	useBadLabel         = "useBadLabel"
	useBASHTerminal     = "useBASHTerminal"
	useDebugStatus      = "useDebugStatus"
	useDOSTerminal      = "useDOSTerminal"
	useEmptyCommand     = "useEmptyCommand"
	useEmptyLabel       = "useEmptyLabel"
	useErrorStatus      = "useErrorStatus"
	useInfoStatus       = "useInfoStatus"
	useLongCommand      = "useLongCommand"
	useShortTimeout     = "useShortTimeout"
	useSHTerminal       = "useSHTerminal"
	useSystemTermSizes  = "useSystemTermSizes"
	useUnknownStatus    = "useUnknownStatus"
	useWarningStatus    = "useWarningStatus"
)

type testTerminalScenario thelper.Scenario

func (s *testTerminalScenario) log(th *thelper.THelper,
	data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testTerminalScenario) assertPrintStatus(th *thelper.THelper,
	x *Terminal,
	message string,
	final string) {
	if x.stderr == nil {
		th.Errorf("stderr writer is not set")
	}

	if s.Switches[missingSTDERR] {
		return
	}

	if !strings.Contains(message, final) {
		th.Errorf("generated message does not have the content")
	}
}

func (s *testTerminalScenario) assertPrintf(th *thelper.THelper,
	x *Terminal,
	message string,
	final string) {
	if x.stdout == nil {
		th.Errorf("stdout writer is not set")
	}

	if s.Switches[missingSTDOUT] {
		return
	}

	if !strings.Contains(message, final) {
		th.Errorf("generated message does not have the content")
	}
}

func (s *testTerminalScenario) assertCommandExists(th *thelper.THelper,
	x *Terminal,
	label string) {
	switch {
	case s.Switches[useEmptyLabel]:
		return
	default:
	}

	_, ok := x.commands[label]
	if !ok {
		th.Errorf("missing command")
	}
}

func (s *testTerminalScenario) assertGotCommand(th *thelper.THelper,
	c *Command) {
	if c == nil {
		switch {
		case s.Switches[useEmptyLabel]:
		case s.Switches[preOccupyCommand]:
			th.Errorf("missing command")
		}

		return
	}

	if !s.Switches[preOccupyCommand] {
		th.Errorf("bogus command object")
	}
}

func (s *testTerminalScenario) assertUpdatedCommand(th *thelper.THelper,
	x *Terminal,
	label string,
	newCommand string) {
	if x == nil || s.Switches[useEmptyLabel] {
		return
	}

	c := x.commands[label]
	if c == nil {
		return
	}

	if c.command != newCommand {
		th.Errorf("new command was not updated")
	}
}

func (s *testTerminalScenario) assertDeletedCommand(th *thelper.THelper,
	x *Terminal,
	label string) {
	if x == nil || s.Switches[useEmptyLabel] {
		return
	}

	c := x.commands[label]
	if c != nil {
		th.Errorf("failed to delete the command")
		return
	}
}

func (s *testTerminalScenario) assertIsRoot(th *thelper.THelper,
	x *Terminal,
	verdict bool) {
	switch {
	case s.Switches[setRoot] && !verdict:
		th.Errorf("root is not set when expected")
	case !s.Switches[setRoot] && verdict:
		th.Errorf("root is set when unexpected")
	}

	switch {
	case s.Switches[setRoot] && !x.root:
		th.Errorf("root is not set inside test terminal when expected")
	case !s.Switches[setRoot] && x.root:
		th.Errorf("root is set inside test terminal when unexpected")
	}
}

func (s *testTerminalScenario) assertOutput(th *thelper.THelper,
	stdout []byte,
	stderr []byte,
	hasError bool) {
	if hasError {
		if len(stdout) != 0 {
			th.Errorf("got stdout while having error")
		}

		if len(stderr) != 0 {
			th.Errorf("got  stderr while having error")
		}
	}
}

func (s *testTerminalScenario) assertSize(th *thelper.THelper,
	x *Terminal,
	row uint,
	column uint) {
	if x == nil {
		th.Errorf("assertSize got a nil terminal")
		return
	}

	if !s.Switches[useSystemTermSizes] {
		if x.row != row {
			th.Errorf("row is not the same as Terminal")
		}

		if x.column != column {
			th.Errorf("column is not the same as Terminal")
		}

		if x.row != defaultRow {
			th.Errorf("malformed row value")
		}

		if x.column != defaultColumn {
			th.Errorf("malformed column value")
		}

		return
	}
}

func (s *testTerminalScenario) getSTDOUTString(x *Terminal) string {
	if s.Switches[missingSTDOUT] || x == nil {
		return ""
	}

	if x.stdout == nil {
		return ""
	}

	b := x.stdout.(*bytes.Buffer)

	return b.String()
}

func (s *testTerminalScenario) getSTDERRString(x *Terminal) string {
	if s.Switches[missingSTDERR] || x == nil {
		return ""
	}

	if x.stderr == nil {
		return ""
	}

	b := x.stderr.(*bytes.Buffer)

	return b.String()
}

func (s *testTerminalScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testTerminalScenario) prepareTerminalType() uint {
	v := uint(0)

	switch {
	case s.Switches[unknownTerminalType]:
		v = ^v
	case s.Switches[useDOSTerminal]:
		v = DOSTerminal
	case s.Switches[useBASHTerminal]:
		v = BASHTerminal
	case s.Switches[useSHTerminal]:
		v = SHTerminal
	}

	return v
}

func (s *testTerminalScenario) prepareTerminal(x *Terminal) {
	x.stdout = &bytes.Buffer{}
	x.stderr = &bytes.Buffer{}

	if s.Switches[missingSTDOUT] {
		x.stdout = nil
	}

	if s.Switches[missingSTDERR] {
		x.stderr = nil
	}

	x.root = false

	if s.Switches[setRoot] {
		x.root = true
	}

	if !s.Switches[useSystemTermSizes] {
		x.row = defaultRow
		x.column = defaultColumn
	}
}

func (s *testTerminalScenario) preparePrintStatus() (statusID uint,
	format string,
	arguments []interface{},
	final string) {
	tag := ""
	statusID = 0

	switch {
	case s.Switches[useInfoStatus]:
		statusID = InfoStatus
		tag = infoTag
	case s.Switches[useWarningStatus]:
		statusID = WarningStatus
		tag = warningTag
	case s.Switches[useErrorStatus]:
		statusID = ErrorStatus
		tag = errorTag
	case s.Switches[useDebugStatus]:
		statusID = DebugStatus
		tag = debugTag
	case s.Switches[useUnknownStatus]:
		statusID = ^statusID
	}

	format, arguments, _ = s.preparePrintf()
	final = fmt.Sprintf(tag+format, arguments...)

	return statusID, format, arguments, final
}

func (s *testTerminalScenario) preparePrintf() (format string,
	arguments []interface{},
	final string) {
	if !s.Switches[missingFormat] {
		format = "%v and %v when up the hill"
	}

	if !s.Switches[missingArguments] {
		arguments = []interface{}{"Jack", "Jill"}
	}

	final = fmt.Sprintf(format, arguments...)

	return format, arguments, final
}

func (s *testTerminalScenario) prepareCommand(x *Terminal) (label string,
	command string,
	hasError bool) {
	// NOTE: command and label data are in the linux / windows source
	//       codes. They are OS specific commands so are written in a
	//       set of separate source codes.
	label = testCommandLabel
	command = testShortCommand
	hasError = false

	switch {
	case s.Switches[useLongCommand] && s.TestType == testExecute:
		fallthrough
	case s.Switches[useLongCommand] && s.TestType == testRun:
		hasError = true
		fallthrough
	case s.Switches[useLongCommand]:
		command = testLongCommand
	case s.Switches[useEmptyCommand]:
		command = testEmptyCommand
	case s.Switches[useBadCommand]:
		command = testBadCommand
		hasError = true
	}

	x.commands[label] = &Command{
		command:      command,
		terminalType: x.terminalType,
		Timeout:      5 * 1000 * 1000, // 5 seconds to speed up test
	}

	switch {
	case s.TestType == testAdd:
		fallthrough
	case s.TestType == testRegister:
		hasError = true

		if !s.Switches[preOccupyCommand] {
			hasError = false

			delete(x.commands, label)
		}
	case s.Switches[preOccupyCommand]:
		hasError = true
	default:
	}

	switch {
	case s.Switches[useEmptyLabel]:
		label = testEmptyCommandLabel
		hasError = true
	case s.Switches[useBadLabel]:
		label = testBadCommandLabel
		hasError = true
	}

	return label, command, hasError
}

func (s *testTerminalScenario) prepareNewCommand() string {
	if s.Switches[useLongCommand] {
		return testShortCommand
	}

	return testLongCommand
}

func (s *testTerminalScenario) prepareRunCommand(x *Terminal) (label string,
	mustWait bool,
	hasError bool) {
	label, _, hasError = s.prepareCommand(x)
	mustWait = true

	if s.Switches[doNotWait] {
		mustWait = false
	}

	return label, mustWait, hasError
}

func (s *testTerminalScenario) prepareTimeout(hasError bool) (timeout uint64,
	err bool) {
	if hasError {
		err = true
	}

	switch {
	case s.Switches[useShortTimeout]:
		timeout = uint64(5) // nanoseconds
		err = true
	default:
	}

	return timeout, err
}
