//+build linux darwin !windows

package term

const (
	testBadCommand        = "noideaAtall -a -r"
	testCommandLabel      = "generate random"
	testBadCommandLabel   = "blablabla"
	testEmptyCommand      = ""
	testEmptyCommandLabel = ""
	testLongCommand       = "sleep 1000; echo 'DONE'"
	testShortCommand      = "ls ."
)
