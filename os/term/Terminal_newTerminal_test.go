package term

import (
	"testing"
)

func TestNewTerminal(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testNewTerminal {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		prefix := s.prepareTerminalType()

		// test
		x := NewTerminal(prefix)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectExists("terminal", x, true)
		s.log(th, map[string]interface{}{
			"terminal": x,
		})
		th.Conclude()
	}
}
