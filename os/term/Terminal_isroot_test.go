package term

import (
	"testing"
)

func TestIsRoot(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testIsRoot {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)

		// test
		ret := x.IsRoot()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertIsRoot(th, x, ret)
		s.log(th, map[string]interface{}{
			"terminal":     x,
			"expect value": s.Switches[setRoot],
			"got value":    ret,
		})
		th.Conclude()
	}
}
