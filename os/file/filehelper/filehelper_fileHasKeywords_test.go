package filehelper

import (
	"testing"
)

func TestFileHasKeywords(t *testing.T) {
	scenarios := testFileHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testFileHasKeywords {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		f := &FileHelper{}
		s.prepareFileHelper(f)
		f1, keywords, expects := s.prepareSeekerFiles()

		// test
		ret := f.FileHasKeywords(f1, keywords...)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertFileHasKeywords(th, ret, expects)
		s.log(th, map[string]interface{}{
			"filepath1":      f1,
			"inKeywords":     keywords,
			"expectedReturn": expects,
			"gotReturn":      ret,
		})
		th.Conclude()
	}
}
