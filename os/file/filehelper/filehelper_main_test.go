package filehelper

import (
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	var err error

	fmt.Println("[ SUITE ] Setting up local sample files...")
	fmt.Println("          It may takes 10 mins depending on system.")

	err = testCreateRandomSampleFiles()
	if err != nil {
		fmt.Println("[ FAILED ] to create sample files. Abort testing.")
		os.Exit(1)
	}

	err = testCreateSeekerFiles()
	if err != nil {
		fmt.Println("[ FAILED ] to create seeker files. Abort testing.")
	}

	restoreSampleFilesPermission()
	fmt.Println("[ DONE ]")

	// main test
	retCode := m.Run()

	// clean up
	e := testCleanupSampleFiles()
	if e != "" {
		fmt.Println("failed to clean up local sample files. Got error:")
		fmt.Printf("%s\n", e)
	}

	os.Exit(retCode)
}

func testCreateSeekerFiles() (err error) {
	f1, err := os.Create(seeker1)
	if err != nil {
		return err
	}
	defer f1.Close()

	f2, err := os.Create(seeker2)
	if err != nil {
		return err
	}
	defer f2.Close()

	_, err = f1.Write([]byte(seeker1Content))
	if err != nil {
		return err
	}

	_, err = f2.Write([]byte(seeker2Content))
	if err != nil {
		return err
	}

	return nil
}

func testCreateRandomSampleFiles() (err error) {
	f1, err := os.Create(filepath1)
	if err != nil {
		return err
	}
	defer f1.Close()

	f2, err := os.Create(filepath2)
	if err != nil {
		return err
	}
	defer f2.Close()

	f3, err := os.Create(filepath3)
	if err != nil {
		return err
	}
	defer f3.Close()

	f4, err := os.Create(filepath4)
	if err != nil {
		return err
	}
	defer f4.Close()

	for i := 0; i < 2001; i++ {
		b := testPrepareRandomBytes()

		_, err = f1.Write(b)
		if err != nil {
			return err
		}

		_, err = f3.Write(b)
		if err != nil {
			return err
		}

		_, err = f4.Write(testPrepareRandomBytes())
		if err != nil {
			return err
		}

		if i > 1999 {
			continue
		}

		_, err = f2.Write(testPrepareRandomBytes())
		if err != nil {
			return err
		}
	}

	return nil
}

func testPrepareRandomBytes() []byte {
	// testPrepareRandomBytes is using ctza non-crypto random string
	// generator algorithm available at:
	// https://stackoverflow.com/questions/22892120
	letters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	letterIdxBits := uint(6)
	letterIdxMask := 1<<letterIdxBits - 1
	letterIdxMax := 63 / letterIdxMask
	columnWidth := 80
	lastColumn := columnWidth - 1

	src := rand.NewSource(time.Now().UnixNano())
	b := make([]byte, columnWidth)

	for i, cache, remain := lastColumn, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}

		idx := int(cache & int64(letterIdxMask))
		if idx < len(letters) {
			b[i] = letters[idx]
			i--
		}

		cache >>= letterIdxBits
		remain--
	}

	b[lastColumn] = 0x0A // newline character

	return b
}

func testCleanupSampleFiles() (e string) {
	e = ""
	files := []string{filepath1,
		filepath2,
		filepath3,
		filepath4,
		seeker1,
		seeker2,
	}

	for _, f := range files {
		err := os.Remove(f)
		if err != nil {
			e = e + err.Error() + "\n"
			err = nil
		}
	}

	return e
}

func restoreSampleFilesPermission() {
	l := []string{filepath1, filepath2, filepath3, filepath4}
	for _, f := range l {
		_ = os.Chmod(f, 0666)
	}
}
