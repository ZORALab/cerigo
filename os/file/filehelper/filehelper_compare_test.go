package filehelper

import (
	"testing"
)

func TestCompare(t *testing.T) {
	var v bool

	scenarios := testFileHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testCompare {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		f := &FileHelper{}
		s.prepareFileHelper(f)
		f1, f2, verdict := s.prepareFiles()

		// test
		v = s.runCompare(f.deepCompare, f.Compare, f1, f2)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertVerdict(th, verdict, v)
		s.log(th, map[string]interface{}{
			"filepath1":      f1,
			"filepath2":      f2,
			"verdict":        v,
			"expect verdict": verdict,
		})
		th.Conclude()
	}
}
