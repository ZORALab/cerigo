package filehelper

func testFileHelperScenarios() []fileHelperScenario {
	return []fileHelperScenario{
		{
			UID:      1,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, a proper
filepath2, with default ChunkSize`,
			Switches: map[string]bool{},
		}, {
			UID:      2,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, a different
length filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				differentFileLength: true,
			},
		}, {
			UID:      3,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, same
filepath1 for filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				sameFile: true,
			},
		}, {
			UID:      4,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a bad filepath1, a proper for
filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				badFilepath1: true,
			},
		}, {
			UID:      5,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, bad for
filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				badFilepath2: true,
			},
		}, {
			UID:      6,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a bad permission filepath1,
proper for filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				noPermissionFilepath1: true,
			},
		}, {
			UID:      7,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, bad
permission filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				noPermissionFilepath2: true,
			},
		}, {
			UID:      8,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a bad permission filepath1, bad
permission filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				noPermissionFilepath2: true,
			},
		}, {
			UID:      9,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, different
content filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				differentFileContent: true,
			},
		}, {
			UID:      10,
			TestType: testCompare,
			Description: `
deepCompare is able to function properly when given a proper filepath1,
different content filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				useDeepCompare:       true,
				differentFileContent: true,
			},
		}, {
			UID:      11,
			TestType: testCompare,
			Description: `
deepCompare is able to function properly when given a proper filepath1, bad
filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				useDeepCompare: true,
				badFilepath2:   true,
			},
		}, {
			UID:      12,
			TestType: testCompare,
			Description: `
deepCompare is able to function properly when given a bad filepath1, proper
filepath2, with default ChunkSize`,
			Switches: map[string]bool{
				useDeepCompare: true,
				badFilepath1:   true,
			},
		}, {
			UID:      13,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, different
content filepath2, with low custom ChunkSize`,
			Switches: map[string]bool{
				lowChunkSize: true,
			},
		}, {
			UID:      14,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, different
content filepath2, with high custom ChunkSize`,
			Switches: map[string]bool{
				highChunkSize: true,
			},
		}, {
			UID:      15,
			TestType: testCompare,
			Description: `
Compare is able to function properly when given a proper filepath1, different
content filepath2, with high standard ChunkSize`,
			Switches: map[string]bool{
				highStandardChunkSize: true,
			},
		}, {
			UID:      16,
			TestType: testFileExists,
			Description: `
FileExist is able functioning properly when given a proper filepath`,
			Switches: map[string]bool{},
		}, {
			UID:      17,
			TestType: testFileExists,
			Description: `
FileExist is able functioning properly when given a bad filepath`,
			Switches: map[string]bool{
				badFilepath1: true,
			},
		}, {
			UID:      18,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given a proper filepath, a
set of proper keywords`,
			Switches: map[string]bool{},
		}, {
			UID:      19,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given a bad filepath, a set
of proper keywords`,
			Switches: map[string]bool{
				badFilepath1: true,
			},
		}, {
			UID:      20,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given an empty filepath, a
set of proper keywords`,
			Switches: map[string]bool{
				emptyFilepath1: true,
			},
		}, {
			UID:      21,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given a different filepath, a
set of proper keywords`,
			Switches: map[string]bool{
				differentFileContent: true,
			},
		}, {
			UID:      22,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given a proper filepath, a
set of bad keywords`,
			Switches: map[string]bool{
				badSeekerKeywords: true,
			},
		}, {
			UID:      23,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given a proper filepath, a
set of empty keywords`,
			Switches: map[string]bool{
				emptySeekerKeywords: true,
			},
		}, {
			UID:      24,
			TestType: testFileHasKeywords,
			Description: `
FileHasKeywords is able functioning properly when given a proper filepath, a
single keyword`,
			Switches: map[string]bool{
				singleSeekerKeyword: true,
			},
		},
	}
}
