package args

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	goodIntN1            = 123
	goodIntN2            = 254
	negativeIntN1        = -123
	goodUIntN1           = uint(456)
	goodUIntN2           = uint(324)
	goodFloatN1          = 5234.237231347342352352352123123
	goodFloatN2          = 237568532524657457245235.23523
	negativeFloatN1      = -3448523436345232347343346.34634
	goodStringN1         = "aeprjgeahrearheareanbpeahearheahearh"
	goodStringN2         = "asvtedvwadvagear235lmad[aergn23ptr2g"
	emptyBytes           = ""
	emptyString          = ""
	emptyProgramExamples = ""
	goodByteDataN1       = "A quick brown fox jumps over the lazy dog"
	goodByteDataN2       = "A quick grey moose jumps over the lazy cat"
	goodFlagNameN1       = "TEST FLAG NAME"
	goodFlagHelpN1       = "run this test help for proper run-up"
	argKeywordN1         = "test-args"
	argKeywordN2         = "testArgs"
	argKeywordN3         = "ta"
	argProgram           = "./testProgram"
)

// help contents
const (
	flagNameH1       = "help"
	flagLabelH1M1    = "-h"
	flagLabelH1M2    = "--help"
	flagLabelH1M3    = "help"
	flagValueLabelH1 = ""
	flagHelpH1       = "display help to understand how this program works"
	flagExampleH1M1  = "Commonly   : $ ./program -h"
	flagExampleH1M2  = "Exmplicitly: $ ./program --help"
	flagExampleH1M3  = ""

	flagNameH2       = "run"
	flagLabelH2M1    = "-r"
	flagLabelH2M2    = "--run"
	flagLabelH2M3    = "run"
	flagValueLabelH2 = "VALUE"
	flagHelpH2       = `
execute the program with the given value. This program takes a full integer
input and does nothing. Basically, I just want to test this long help
description.
`
	flagExampleH2M1 = "Commonly   : $ ./program -r 123"
	flagExampleH2M2 = "Exmplicitly: $ ./program --run \"123\""
	flagExampleH2M3 = "Default    : $ ./program 123"

	flagNameH3       = "test"
	flagLabelH3M1    = "test"
	flagLabelH3M2    = ""
	flagLabelH3M3    = ""
	flagValueLabelH3 = ""
	flagHelpH3       = "run the program's self-diagnostic system"
	flagExampleH3M1  = "Commonly   : $ ./program test"
	flagExampleH3M2  = ""
	flagExampleH3M3  = ""

	flagNameH4       = "version"
	flagLabelH4M1    = "version"
	flagLabelH4M2    = ""
	flagLabelH4M3    = ""
	flagValueLabelH4 = ""
	flagHelpH4       = "display the program version"
	flagExampleH4M1  = "Commonly   : $ ./program version"
	flagExampleH4M2  = ""
	flagExampleH4M3  = ""

	programNameASCII              = "Cerigo Args Test Program"
	programVersionString          = "2.5.0~rc1"
	programVersionInteger         = 2
	programVersionUnsignedInteger = uint(programVersionInteger)
	programDescriptionASCII       = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque neque
urna, imperdiet quis volutpat ut, posuere non diam. Aliquam id sapien dolor.
Vestibulum imperdiet, sem et convallis tincidunt, orci massa efficitur dolor,
non ornare turpis ipsum non ligula. Nullam nec pharetra ex. Curabitur congue
sapien et nulla ultrices pellentesque. Praesent sit amet dignissim justo. In
semper hendrerit nulla at faucibus. Maecenas fermentum dapibus sapien, et
tempor neque interdum a. Cras sit amet efficitur nibh. Suspendisse placerat,
dolor nec interdum elementum, quam elit lobortis turpis, in vulputate sapien
risus vitae nisl. Duis a pretium leo. Nulla facilisis dui ac nibh euismod
tempus. Nullam tincidunt accumsan mauris sed aliquam.
`
	programDescriptionASCIISeeker = "ipsum dolor sit"
	programDescriptionUnicode     = `
一們而獨我場毒關歡是排現較人統可國的意集當情方主定性只簡國直道點行問聽。已我今
資們案詩注童廣難上上了大開辦海中清影北，報出一，說取許來的，景音洲者禮書團做他
人地布我角專道水？多非費病大金久畫那一神，平足童個時功費當狀，布目在；古愛縣主
了息現見麼該朋些樣不理：低比管止以了臺！回花古意種王。河問在的出教算條表用車效
定物一加是戲己實百紀庭星其素也以速皮理人，人劇是數？一候學軍世河，小手好只：子
現主說現比？

行成代基定時。時來像到個表像，驗格王。

國器色個光，會規使總國去女此公？
`
	programDescriptorUnicodeSeeker = "時來像到個"
	programDescriptionEmpty        = ""
	programExampleP1M1             = "Normal Run  : $ ./program -r 123"
	programExampleP1M2             = "Ask for Help: $ ./program --args"
	programExampleP1M3             = ""
	programMaxColumns              = uint(80)
)

// switches
const (
	alternateContent               = "alternateContent"
	disableFlagHelp                = "disableFlagHelp"
	emptyFlagName                  = "emptyFlagName"
	emptyFlagHelp                  = "emptyFlagHelp"
	emptyLabelsList                = "emptyLabelsList"
	emptyProgramDescription        = "emptyProgramDescription"
	emptyProgramVersion            = "emptyProgramVersion"
	hideFlagExamples               = "hideFlagExamples"
	intProgramVersion              = "intProgramVersion"
	missingFlag                    = "missingFlag"
	negativeFloat                  = "negativeFloat"
	negativeInt                    = "negativeInt"
	preExistedFlag                 = "preExistedFlag"
	prepareArgValueWithDoubleQuote = "prepareArgValueWithDoubleQuote"
	prepareArgValueWithSingleQuote = "prepareArgValueWithSingleQuote"
	prepareDoubleDashArg           = "prepareDoubleDashArg"
	prepareMissingArg              = "prepareMissingArg"
	prepareNoArgs                  = "prepareNoArgs"
	prepareShortArgKeyword         = "prepareShortArgKeyword"
	prepareSingleDashArg           = "prepareSingleDashArg"
	prepareStandaloneArg           = "prepareStandaloneArg"
	prepareStandaloneArgKeyword    = "prepareStandaloneArgKeyword"
	prepareValueOnlyArg            = "prepareValueOnlyArg"
	prepareArgWithEqual            = "prepareArgWithEqual"
	seekInt                        = "seekInt"
	seekInt8                       = "seekInt8"
	seekInt16                      = "seekInt16"
	seekInt32                      = "seekInt32"
	seekInt64                      = "seekInt64"
	seekUInt                       = "seekUInt"
	seekUInt8                      = "seekUInt8"
	seekUInt16                     = "seekUInt16"
	seekUInt32                     = "seekUInt32"
	seekUInt64                     = "seekUInt64"
	seekFloat32                    = "seekFloat32"
	seekFloat64                    = "seekFloat64"
	seekString                     = "seekString"
	seekBytes                      = "seekBytes"
	seekBytesPointer               = "seekBytesPointer"
	seekBoolean                    = "seekBoolean"
	uintProgramVersion             = "uintProgramVersion"
	unicodeProgramDescription      = "unicodeProgramDescription"
)

const (
	testManagerAdd       = "testManagerAdd"
	testManagerParse     = "testManagerParse"
	testManagerPrintHelp = "testManagerPrintHelp"
)

type testData struct {
	i   int
	u   uint
	i64 int64
	u64 uint64
	f64 float64
	i32 int32
	u32 uint32
	f32 float32
	i16 int16
	u16 uint16
	s   string
	b   []byte
	bp  *[]byte
	i8  int8
	u8  uint8
	bl  bool
}

type testManagerScenario thelper.Scenario

func (s *testManagerScenario) log(th *thelper.THelper,
	data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testManagerScenario) assertError(th *thelper.THelper, err error) {
	switch {
	case s.Switches[missingFlag]:
		if err != nil {
			return
		}

		th.Errorf("missing error when having missing flag")
	case s.Switches[preExistedFlag]:
		if err != nil {
			return
		}

		th.Errorf("panic error is incorrect")
	default:
	}
}

func (s *testManagerScenario) assertFlagExists(th *thelper.THelper,
	m *Manager,
	f *Flag) {
	if m == nil {
		return
	}

	if f == nil {
		if !s.Switches[missingFlag] {
			th.Errorf("given input flag was nil")
		}

		return
	}

	if m.flags == nil {
		th.Errorf("manager's flags list is not initialized properly")
		return
	}

	for _, x := range m.flags {
		if x == f {
			return
		}
	}

	th.Errorf("given flag is not found")
}

func (s *testManagerScenario) assertParsedData(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[prepareNoArgs] || s.Switches[prepareStandaloneArg] {
		return
	}

	s.assertParsedInt(th, sample, actual)
	s.assertParsedUInt(th, sample, actual)
	s.assertParsedFloat(th, sample, actual)
	s.assertParsedBytes(th, sample, actual)
	s.assertParsedBoolean(th, sample, actual)
	s.assertParsedString(th, sample, actual)
}

func (s *testManagerScenario) assertParsedString(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[seekString] {
		if actual.s != sample.s {
			th.Errorf("parsed string does not match")
		}
	}
}

func (s *testManagerScenario) assertParsedBoolean(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[seekBoolean] {
		if actual.bl != sample.bl {
			th.Errorf("parsed boolean does not match")
		}
	}
}

func (s *testManagerScenario) assertParsedBytes(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[seekBytes] {
		th.ExpectSameBytesSlices("given bytes",
			&sample.b,
			"actual bytes",
			&actual.b)
	}

	if s.Switches[seekBytesPointer] {
		th.ExpectSameBytesSlices("given bytes",
			&sample.b,
			"actual bytes",
			actual.bp)
	}
}

func (s *testManagerScenario) assertParsedFloat(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[seekFloat32] {
		if actual.f32 != sample.f32 {
			th.Errorf("parsed float32 does not match")
		}
	}

	if s.Switches[seekFloat64] {
		if actual.f64 != sample.f64 {
			th.Errorf("parsed float64 does not match")
		}
	}
}

func (s *testManagerScenario) assertParsedUInt(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[seekUInt] {
		if actual.u != sample.u {
			th.Errorf("parsed uint does not match")
		}
	}

	if s.Switches[seekUInt8] {
		if actual.u8 != sample.u8 {
			th.Errorf("parsed uint8 does not match")
		}
	}

	if s.Switches[seekUInt16] {
		if actual.u16 != sample.u16 {
			th.Errorf("parsed uint16 does not match")
		}
	}

	if s.Switches[seekUInt32] {
		if actual.u32 != sample.u32 {
			th.Errorf("parsed uint32 does not match")
		}
	}

	if s.Switches[seekUInt64] {
		if actual.u64 != sample.u64 {
			th.Errorf("parsed uint64 does not match")
		}
	}
}

func (s *testManagerScenario) assertParsedInt(th *thelper.THelper,
	sample *testData,
	actual *testData) {
	if s.Switches[seekInt] {
		if actual.i != sample.i {
			th.Errorf("parsed int does not match")
		}
	}

	if s.Switches[seekInt8] {
		if actual.i8 != sample.i8 {
			th.Errorf("parsed int8 does not match")
		}
	}

	if s.Switches[seekInt16] {
		if actual.i16 != sample.i16 {
			th.Errorf("parsed int16 does not match")
		}
	}

	if s.Switches[seekInt32] {
		if actual.i32 != sample.i32 {
			th.Errorf("parsed int32 does not match")
		}
	}

	if s.Switches[seekInt64] {
		if actual.i64 != sample.i64 {
			th.Errorf("parsed int64 does not match")
		}
	}
}

func (s *testManagerScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testManagerScenario) prepareDataSample() (d *testData) {
	n := goodIntN1
	u := goodUIntN1
	f := goodFloatN1
	str := goodStringN1
	b := goodByteDataN1
	bl := true

	switch {
	case s.Switches[negativeInt]:
		n = negativeIntN1
	case s.Switches[negativeFloat]:
		f = negativeFloatN1
	case s.Switches[emptyString]:
		str = emptyString
	case s.Switches[emptyBytes]:
		b = ""
	case s.Switches[alternateContent]:
		n = goodIntN2
		u = goodUIntN2
		f = goodFloatN2
		str = goodStringN2
		b = goodByteDataN2
	}

	d = &testData{
		i:   n,
		i8:  int8(n),
		i16: int16(n),
		i32: int32(n),
		i64: int64(n),
		u:   u,
		u8:  uint8(u),
		u16: uint16(u),
		u32: uint32(u),
		u64: uint64(u),
		f32: float32(f),
		f64: f,
		s:   str,
		b:   []byte(b),
		bl:  bl,
	}

	return d
}

func (s *testManagerScenario) prepareArgKeyword(keyword string) (arg string) {
	switch {
	case s.Switches[prepareValueOnlyArg]:
		arg = ""
	case s.Switches[prepareStandaloneArg]:
		arg = fmt.Sprintf("%v", keyword)
	case s.Switches[prepareSingleDashArg]:
		arg = fmt.Sprintf("-%v", keyword)
	case s.Switches[prepareDoubleDashArg]:
		fallthrough
	default:
		arg = fmt.Sprintf("--%v", keyword)
	}

	return arg
}

func (s *testManagerScenario) prepareArgValue(value string) (out string) {
	if s.Switches[prepareStandaloneArg] {
		return ""
	}

	switch {
	case s.Switches[prepareArgValueWithSingleQuote]:
		return fmt.Sprintf("'%v'", value)
	case s.Switches[prepareArgValueWithDoubleQuote]:
		return fmt.Sprintf("\"%v\"", value)
	}

	return value
}

func (s *testManagerScenario) prepareArgs(keyword string,
	value string) (args []string) {
	v := s.prepareArgValue(value)

	args = []string{argProgram}

	if !s.Switches[prepareMissingArg] {
		switch {
		case s.Switches[prepareArgWithEqual]:
			args = append(args, fmt.Sprintf("%v=%v", keyword, v))
		default:
			args = append(args, keyword)
			if v != "" {
				args = append(args, v)
			}
		}
	}

	if s.Switches[prepareNoArgs] {
		args = nil
	}

	return args
}

func (s *testManagerScenario) prepareDataFlags(m *Manager) (f *Flag,
	sample *testData,
	d *testData) {
	sample = s.prepareDataSample()
	d = &testData{}
	value := ""
	f = &Flag{}
	keyword := ""

	switch {
	case s.Switches[prepareShortArgKeyword]:
		keyword = argKeywordN3
	case s.Switches[prepareStandaloneArgKeyword]:
		keyword = argKeywordN2
	default:
		keyword = argKeywordN1
	}

	argKeyword := s.prepareArgKeyword(keyword)
	f.Label = []string{argKeyword}

	value = s.setFlagValue(sample, d, f)

	if !s.Switches[emptyFlagName] {
		f.Name = goodFlagNameN1
	}

	if !s.Switches[emptyFlagHelp] {
		f.Help = goodFlagHelpN1
	}

	switch {
	case s.Switches[preExistedFlag]:
		_ = m.Add(f)
	case s.Switches[missingFlag]:
		f = nil
	}

	if m != nil {
		m.args = s.prepareArgs(argKeyword, value)
	}

	return f, sample, d
}

//nolint:gocyclo
func (s *testManagerScenario) setFlagValue(sample *testData,
	d *testData,
	f *Flag) (value string) {
	switch {
	case s.Switches[seekInt]:
		value = fmt.Sprintf("%v", sample.i)
		f.Value = &d.i
	case s.Switches[seekInt8]:
		value = fmt.Sprintf("%v", sample.i8)
		f.Value = &d.i8
	case s.Switches[seekInt16]:
		value = fmt.Sprintf("%v", sample.i16)
		f.Value = &d.i16
	case s.Switches[seekInt32]:
		value = fmt.Sprintf("%v", sample.i32)
		f.Value = &d.i32
	case s.Switches[seekInt64]:
		value = fmt.Sprintf("%v", sample.i64)
		f.Value = &d.i64
	case s.Switches[seekUInt]:
		value = fmt.Sprintf("%v", sample.u)
		f.Value = &d.u
	case s.Switches[seekUInt8]:
		value = fmt.Sprintf("%v", sample.u8)
		f.Value = &d.u8
	case s.Switches[seekUInt16]:
		value = fmt.Sprintf("%v", sample.u16)
		f.Value = &d.u16
	case s.Switches[seekUInt32]:
		value = fmt.Sprintf("%v", sample.u32)
		f.Value = &d.u32
	case s.Switches[seekUInt64]:
		value = fmt.Sprintf("%v", sample.u64)
		f.Value = &d.u64
	case s.Switches[seekFloat32]:
		value = fmt.Sprintf("%v", sample.f32)
		f.Value = &d.f32
	case s.Switches[seekFloat64]:
		value = fmt.Sprintf("%v", sample.f64)
		f.Value = &d.f64
	case s.Switches[seekString]:
		value = fmt.Sprintf("%v", sample.s)
		f.Value = &d.s
	case s.Switches[seekBytes]:
		value = fmt.Sprintf("%s", sample.b)
		f.Value = &d.b
	case s.Switches[seekBytesPointer]:
		value = fmt.Sprintf("%s", sample.b)
		f.Value = &d.bp
	case s.Switches[seekBoolean]:
		value = fmt.Sprintf("%v", sample.bl)
		f.Value = &d.bl
	}

	return value
}

func (s *testManagerScenario) assertPrintHelp(th *thelper.THelper,
	m *Manager,
	output string) {
	str := ""

	// program title
	str = m.Name
	s.assertHelpKeywordExistence(th, str, output, "program name")

	// program version
	str = fmt.Sprintf("%v", m.Version)
	s.assertHelpKeywordExistence(th, str, output, "program version")

	// program descriptions
	switch {
	case s.Switches[unicodeProgramDescription]:
		str = programDescriptorUnicodeSeeker
	case s.Switches[emptyProgramDescription]:
		str = ""
	default:
		str = programDescriptionASCIISeeker
	}

	s.assertHelpKeywordExistence(th, str, output, "program description")

	// program examples
	switch {
	case s.Switches[emptyProgramExamples]:
		str = ""
	default:
		str = strings.Split(m.Examples[0], "$")[1]
	}

	s.assertHelpKeywordExistence(th, str, output, "program examples")

	// flags
	s.assertHelpFlags(th, output)
}

//nolint:gocyclo,gocognit
func (s *testManagerScenario) assertHelpFlags(th *thelper.THelper,
	output string) {
	str := flagLabelH1M1
	verdict := s.seekHelpOutputFor(str, output)

	// flag label
	switch {
	case s.Switches[disableFlagHelp]:
		fallthrough
	case s.Switches[emptyLabelsList]:
		if verdict == true {
			th.Errorf("got label / argument for empty labels list")
		}
	default:
		if verdict == false {
			th.Errorf("missing label")
		}
	}

	verdict = s.seekHelpOutputFor(", ,", output)
	if verdict {
		th.Errorf("flag did process empty label ( 'arg', '', '') ")
	}

	// flag value label
	str = flagValueLabelH2
	verdict = s.seekHelpOutputFor(str, output)

	switch {
	case s.Switches[disableFlagHelp]:
		fallthrough
	case s.Switches[emptyLabelsList]:
		if verdict == true {
			th.Errorf("got flag value label for empty labels list")
		}
	default:
		if verdict == false {
			th.Errorf("missing value label")
		}
	}

	// flag help statement
	x := strings.Split(flagHelpH1, " ")
	str = x[0] + " " + x[1] + " " + x[2]
	verdict = s.seekHelpOutputFor(str, output)

	switch {
	case s.Switches[disableFlagHelp]:
		fallthrough
	case s.Switches[emptyLabelsList]:
		if verdict == true {
			th.Errorf("got help statement for empty labels list")
		}
	default:
		if verdict == false {
			th.Errorf("missing help statement")
		}
	}

	// flag examples list
	str = strings.Split(flagExampleH1M1, "$ ")[1]
	verdict = s.seekHelpOutputFor(str, output)

	switch {
	case s.Switches[hideFlagExamples]:
		fallthrough
	case s.Switches[disableFlagHelp]:
		fallthrough
	case s.Switches[emptyLabelsList]:
		if verdict == true {
			th.Errorf("got flag example for empty labels list")
		}
	default:
		if verdict == false {
			th.Errorf("missing flag example")
		}
	}
}

func (s *testManagerScenario) assertHelpKeywordExistence(th *thelper.THelper,
	keyword string,
	output string,
	label string) {
	verdict := s.seekHelpOutputFor(keyword, output)

	switch {
	case verdict && keyword == "":
		th.Errorf("bogus %s output", label)
	case !verdict && keyword != "":
		th.Errorf("%s is not included", label)
	}
}

func (s *testManagerScenario) seekHelpOutputFor(keyword string,
	help string) bool {
	if keyword == "" {
		return false
	}

	upcase := strings.ToUpper(keyword)
	lowcase := strings.ToLower(keyword)

	vu := strings.Contains(help, upcase)
	vl := strings.Contains(help, lowcase)

	return vu || vl
}

func (s *testManagerScenario) prepareManagerForPrint(m *Manager) {
	// set for testing
	m.maxColumn = programMaxColumns

	fl := []*Flag{
		{
			Name: flagNameH1,
			Label: []string{
				flagLabelH1M1,
				flagLabelH1M2,
				flagLabelH1M3,
			},
			ValueLabel: flagValueLabelH1,
			Help:       flagHelpH1,
			HelpExamples: []string{flagExampleH1M1,
				flagExampleH1M2,
				flagExampleH1M3,
			},
		}, {
			Name: flagNameH2,
			Label: []string{
				flagLabelH2M1,
				flagLabelH2M2,
				flagLabelH2M3,
			},
			ValueLabel: flagValueLabelH2,
			Help:       flagHelpH2,
			HelpExamples: []string{flagExampleH2M1,
				flagExampleH2M2,
				flagExampleH2M3,
			},
		}, {
			Name: flagNameH3,
			Label: []string{
				flagLabelH3M1,
				flagLabelH3M2,
				flagLabelH3M3,
			},
			ValueLabel: flagValueLabelH3,
			Help:       flagHelpH3,
			HelpExamples: []string{flagExampleH3M1,
				flagExampleH3M2,
				flagExampleH3M3,
			},
		}, {
			Name: flagNameH4,
			Label: []string{
				flagLabelH4M1,
				flagLabelH4M2,
				flagLabelH4M3,
			},
			ValueLabel: flagValueLabelH4,
			Help:       flagHelpH4,
			HelpExamples: []string{flagExampleH4M1,
				flagExampleH4M2,
				flagExampleH4M3,
			},
		},
	}

	m.Name = programNameASCII

	switch {
	case s.Switches[uintProgramVersion]:
		m.Version = programVersionUnsignedInteger
	case s.Switches[intProgramVersion]:
		m.Version = programVersionInteger
	case s.Switches[emptyProgramVersion]:
	default:
		m.Version = programVersionString
	}

	switch {
	case s.Switches[unicodeProgramDescription]:
		m.Description = programDescriptionUnicode
	case s.Switches[emptyProgramDescription]:
		m.Description = programDescriptionEmpty
	default:
		m.Description = programDescriptionASCII
	}

	m.Examples = []string{
		programExampleP1M1,
		programExampleP1M2,
		programExampleP1M3,
	}
	if s.Switches[emptyProgramExamples] {
		m.Examples = []string{}
	}

	if !s.Switches[hideFlagExamples] {
		m.ShowFlagExamples = true
	}

	for _, f := range fl {
		f.DisableHelp = s.Switches[disableFlagHelp]
		_ = m.Add(f)
	}

	if s.Switches[emptyLabelsList] {
		for _, f := range fl {
			f.Label = []string{}
		}
	}
}
