package args

import (
	"testing"
)

func TestManagerPrintHelp(t *testing.T) {
	scenarios := testManagerScenarios()

	for i, s := range scenarios {
		if s.TestType != testManagerPrintHelp {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		m := NewManager()
		s.prepareManagerForPrint(m)

		// test
		out := m.PrintHelp()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertPrintHelp(th, m, out)
		s.log(th, map[string]interface{}{
			"got output": out,
		})
		th.Conclude()
	}
}
