package args

import (
	"testing"
)

func TestManagerParse(t *testing.T) {
	scenarios := testManagerScenarios()

	for i, s := range scenarios {
		if s.TestType != testManagerParse {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		m := NewManager()
		f, sample, actual := s.prepareDataFlags(m)
		err := m.Add(f)

		// test
		m.Parse()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertError(th, err)
		s.assertFlagExists(th, m, f)
		s.assertParsedData(th, sample, actual)
		s.log(th, map[string]interface{}{
			"input args":  m.args,
			"input flag":  f,
			"got manager": m.flags,
			"got error":   err,
			"input data":  sample,
			"got data":    actual,
		})
		th.Conclude()
	}
}
