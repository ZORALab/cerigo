package args

import (
	"fmt"
)

type exampleMyConfig struct {
	action string
	a      int
	b      int
}

func (c *exampleMyConfig) Add() int {
	return c.a + c.b
}

func Example() {
	var err error
	// ...you have your own configuration variables...
	c := exampleMyConfig{}

	// 1. create a manager
	m := NewManager()
	m.Name = "My Program Title"
	m.Description = `
This is a long single paragraph description about the program. You can write
it this way using Go's raw string. Unicode is supported.
`
	m.Version = "0.2.0"
	m.Examples = []string{"",
		"call for help:   $ ./program --help",
		"set A        :   $ ./prgoram --set-a 123",
		"set B        :   $ ./program --set-b 123",
	}
	m.ShowFlagExamples = true

	// 2. add your flags accordingly
	err = m.Add(&Flag{
		Name:  "help",
		Label: []string{"-h", "--help", "help"},
		Value: &c.action,
		Help:  "to print help",
		HelpExamples: []string{"",
			"$ ./program -h",
			"$ ./program --help",
			"$ ./program help",
		},
	})
	if err != nil {
		// handle error
		return
	}

	err = m.Add(&Flag{
		Name:  "set A",
		Label: []string{"-sa", "--set-a"},
		Value: &c.a,
		Help:  "to add",
		HelpExamples: []string{"",
			"$ ./program -sa 123",
			"$ ./program --set-a 123",
		},
	})
	if err != nil {
		// handle error
		return
	}

	err = m.Add(&Flag{
		Name:  "set B",
		Label: []string{"-sb", "--set-b"},
		Value: &c.b,
		Help:  "to add",
		HelpExamples: []string{"",
			"$ ./program -sb 123",
			"$ ./program --set-b 123",
		},
	})
	if err != nil {
		// handle error
		return
	}

	// 3. parse the flags
	m.Parse()

	// 4. execute based on your configs
	switch c.action {
	case "-h":
		fallthrough
	case "--help":
		fallthrough
	case "help":
		// 5. generate help output from config
		help := m.PrintHelp()
		fmt.Printf("%s", help)
	default:
		fmt.Printf("total: %v\n", c.Add())
	}
}

// Output CLI:
//
// u0:gosandbox$ ./main help
// MY PROGRAM TITLE (0.2.0)
// ────────────────────────
// This is a long single paragraph description about the program. You can write
// it this way using Go's raw string. Unicode is supported.
//
//
// USAGES
// ──────
// 1)  call for help:   $ ./program --help
// 2)  set A        :   $ ./prgoram --set-a 123
// 3)  set B        :   $ ./program --set-b 123
//
//
// ARGUMENTS
// ─────────
// -h, --help, help     to print help
//                      EXAMPLES
//                        1) $ ./program -h
//                        2) $ ./program --help
//                        3) $ ./program help
//
// -sa, --set-a         to add
//                      EXAMPLES
//                       1) $ ./program -sa 123
//                       2) $ ./program --set-a 123
//
// -sb, --set-b         to add
//                     EXAMPLES
//                       1) $ ./program -sb 123
//                       2) $ ./program --set-b 123
// u0:gosandbox$
// u0:gosandbox$ ./main -sa 43 -sb 123
// total: 166
// u0:gosandbox$
