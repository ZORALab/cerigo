package args

const (
	// ErrorMissingFlag is the error message for missing flag object
	ErrorMissingFlag = "missing flag"

	// ErrorConflictedFlag is the error message for flag with conflicted
	// labels
	ErrorConflictedFlag = "detected conflicted label"
)
