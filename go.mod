module gitlab.com/zoralab/cerigo

go 1.13

replace gitlab.com/zoralab/cerigo => ./

require (
	gitlab.com/zoralab/godocgen v0.0.2 // indirect
	golang.org/x/perf v0.0.0-20200918155509-d949658356f9 // indirect
)
