// package cerigo is a Go library module extended from the standard library.
//
// Cerigo acts as a general-purpose toolbox for quick development. Heavily
// unit tested, Cerigo packages are reliable enough to consistently produce
// the same results. Hence, it is reliably enough to roll out a prototype and
// subsequently converting it into production model.
package cerigo
