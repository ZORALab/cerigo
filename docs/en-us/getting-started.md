+++
date = "2020-04-17T12:35:22+08:00"
title = "Getting Started"
description = """
Let's get started with deploying Cerigo in your compute module. Cerigo itself
is a simplified server program built using various tools and components. That
way, Cerigo can continuously improve itself in a scalable manner while making
the user experience easier.
"""
keywords = ["getting", "started", "cerigo"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "getting-started-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Getting Started with Cerigo"

[thumbnails.1]
url = "getting-started-1200x628.png"
width = "1200"
height = "628"
alternateText = "Getting Started with Cerigo"


[menu.main]
parent = ""
name = "Getting Started"
pre = "🛫"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Understand Cerigo
Before we do something, let's understand Cerigo in a nut shell.



### No Application
Cerigo is a **full-fletch library package** module. Hence, there are no
applications (e.g. `cmd/someApp`) to `go get` for.



### Supported Version Patterns
Currently, Cerigo releases its packages under a different definition of
[Semantic Versioning](https://semver.org/). It is defined as follows:

```txt
vX.Y.Z

v = requirement to attach "v"
X = flag to indicate package mode ("production" = 1 | "development = "0" )
Y = non-backward compatible version release
Z = backward compatible version release. Reset to 0 if Y is increased.
```

Example: for `v0.2.0`, `v0.2.1`, `v0.3.0`, and `v1.3.0`, the time sequences and
priority list (latest updates first) would be:

```txt
v1.3.0
v0.3.0
v0.2.1
v0.2.0
```



### Using Branching Over Tagging
We decided to use **branching** over **tagging** mainly because the current
`go get` enforces version folder implementation or magical `go.mod` definitions.

We tried both ways. They are a roller-coaster experience for Cerigo team so to
keep things simple, we stick to branching and keep the major number between `0`
and `1` as a flag instead.

Hence, when you update Cerigo in your `go.mod`, you can use any of the following
patterns:

```bash
$ go get gitlab.com/zoralab/cerigo@main             // latest stable release
$ go get gitlab.com/zoralab/cerigo@staging          // next stable (testing)
$ go get gitlab.com/zoralab/cerigo@next             // bleeding edge
$ go get gitlab.com/zoralab/cerigo@releases-v0.0.1  // v0.0.1 release
```



## Using Cerigo
Now that we understand how Cerigo works, we can proceed to use it in your
project.




### Import Statement
To `import` any of Cerigo's library package, you can call in the `import`
statement like the usual Go style. For example, to import `demo/XYZ` package,
you can do the following:

```go
import "gitlab.com/zoralab/cerigo/demo/XYZ"
```
If you're using `import` group, the best practice would be:

```go
import (
	"bytes"
	"io"
	...
						// leave a space here
	"gitlab.com/zoralab/cerigo/XYZ"		// include library
)
```




### Update Cerigo
To update your `go.mod` automatically, you can `go get` it at the `go.mod`
directory level:

```bash
$ go get gitlab.com/zoralab/cerigo@BRANCH-NAME
```

Example, to track `next` branch, it is:


```bash
$ go get gitlab.com/zoralab/cerigo@next
```


## Epilogue
That's it. Cerigo is that easy to use in Go project. The rest of the steps would
be checking out the Go Documentations section and apply the necessary modules
to it. If you have any questions, please feel free to raise a ticket at our
Issues section available here:

https://gitlab.com/zoralab/cerigo/-/issues
