+++
data = "2019-08-06T13:08:02+08:00"
title = "Cerigo"
description = """
Cerigo is a Go library module extended from the standard library. It acts as a
general-purpose toolbox for quick development and backward compatibles. That
way, one can focuses on the idea rather than meddling with Go codes.
"""
keywords = ["library", "Cerigo", "Go", "ZORALab"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Cerigo"

[thumbnails.1]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Cerigo"


[menu.main]
parent = ""
name = "Home"
pre = "🏠"
weight = 1


[schema]
selectType = "WebPage"
+++

![Cerigo]({{< link "/img/logo/default-2048x1024.svg" "this" "url-only" >}})

# {{% param "title" %}}
{{% param "description" %}}

Cerigo is designed specifically to use
[Go Module](https://blog.golang.org/using-go-modules). Therefore, its minimum
supported version is `1.11` onwards.




## Why Cerigo
Here are some of the great reasons for using Cerigo in your next project:

{{< card "cerigo.cards.features" "grid" "embed" >}}




## Core Features
From time to time, Cerigo identified and executed a wide varieties of features
implementations for better use in the future. Here are some of our recognizable
features implemented across Cerigo versions.

| Name                                        | Status      | Starting Version |
|:--------------------------------------------|:------------|:-----------------|
| `algo/cmdchain` support                     | ✅Completed | `v0.0.1`         |
| `encoding/float` support                    | ✅Completed | `v0.0.1`         |
| `strings/strhelper` support                 | ✅Completed | `v0.0.1`         |
| `testing/thelper` support                   | ✅Completed | `v0.0.1`         |
| `os/args` support                           | ✅Completed | `v0.0.1`         |
| `os/file/filehelper` support                | ✅Completed | `v0.0.1`         |
| `os/term` support                           | ✅Completed | `v0.0.1`         |
| `crypto/manager` support                    | 📅Working   | `N/A`            |
| `web/docs` support                          | 📅Working   | `N/A`            |

For latest information, please check out the Issues board at:
https://gitlab.com/ZORALab/cerigo/issues




## Powered by Simple Tools
Cerigo is powered by many simple tools integrated together in order to produce
great results. Here are some of the core technologies:

{{< svg/shieldTag "core language" "go" "#03a9f4" >}}
{{< svg/shieldTag "core tool" "benchstat" "#ab47bc" >}}
{{< svg/shieldTag "core tool" "golangci-lint" "#ab47bc" >}}
{{< svg/shieldTag "core tool" "godocgen" "#ab47bc" >}}
{{< svg/shieldTag "core tool" "gopher" "#ab47bc" >}}
{{< svg/shieldTag "core tool" "helix" "#ab47bc" >}}

## Quality Assured Via Continuous Integrations
To ensure that our customers are getting updates in an incremental and stable
way, Cerigo uses [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/)
technologies to manage its quality assurance from release to even developer
experimentation stage.

Feel free to check out their current status here:

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | {{< image "Master Pipeline Status"
	"https://gitlab.com/ZORALab/cerigo/badges/master/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Master Coverage Report"
	"https://gitlab.com/ZORALab/cerigo/badges/master/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |
| `staging` | {{< image "Staging Pipeline Status"
	"https://gitlab.com/ZORALab/cerigo/badges/staging/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Staging Coverage Report"
	"https://gitlab.com/ZORALab/cerigo/badges/staging/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |
| `next` | {{< image "Next Pipeline Status"
	"https://gitlab.com/ZORALab/cerigo/badges/next/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Next Coverage Report"
	"https://gitlab.com/ZORALab/cerigo/badges/next/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |

To find Cerigo source codes, it is available at the following repositories:

| Name          | Location                            |
|:--------------|:------------------------------------|
| GitLab.com    | https://gitlab.com/ZORALab/cerigo   |




## Contributors
Cerigo is not just a single person work but a culmulative of large amount of
knowledge and wisdom. Here are some of the critical contributors for making
Cerigo a success.



### Maintainers
Here are the maintainers who manage Cerigo's code releases and its health.

1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2019 to present



### Developers
Here are the developers who works tirelessly to enable and keeping Cerigo
up-to-date.

1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2019 to present



### Knowledge Experts
Here are the knowledge experts contributed insights and wisdom for Cerigo to
be successful.

1. [Golang.org](https://golang.org/) - 2019
2. [Schema.org](https://schema.org/) - 2020



### Sponsorship
Here are the sponsors that provide financial supports to enable Cerigo team
to continue keeping Cerigo alive:

{{< card "cerigo.cards.sponsors" "grid" "default" >}}


{{< note "note" "Sponsor Us" >}}
Interested to sponsor this project? Check out: [Sponsoring Section]({{< link
	"sponsoring"
	"this"
	"url-only" >}})
{{< /note >}}




## Where to Go Next?
Interested to use Cerigo for your next project? Feel free to:

{{< renderHTML "html" "amp" >}}
<div class="row">
	<div class="column" style="text-align: center">
		<a class="button pinpoint"
			href="{{< link "getting-started" "this" "url-only">}}"
			style="text-align: center">
			Getting Started with Cerigo
		</a>
	</div>
</div>
{{< /renderHTML >}}
