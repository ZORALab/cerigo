+++
# WARNING: This markdown is autogenerated by bot. Do not edit it manually!
date = "2020-09-21T11:52:16+08:00"
title = "strhelper"
description = """
strhelper package is an extension to the standard strings package offering
macro styling functions. Normally, these functions are not suitable to be
in the standard strings package as they became slightly specific to use
cases, which is outside the standard package's sole objective.

This package offers functions like Indent, WordWrapping, etc., further the
use of formatting or styling strings.

"""
keywords = [ "strhelper", "go", "cerigo", "docs" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "/img/thumbnails/go/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Cerigo Go Documents"

[thumbnails.1]
url = "/img/thumbnails/go/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Cerigo Go Documents"


[menu.main]
parent = "X) Go Doc"
name = "strings/strhelper"
pre = "📘"
weight = 1


[schema]
selectType = "WebPage"
+++

# Package strhelper
```go
import "gitlab.com/zoralab/cerigo/strings/strhelper"
```
strhelper package is an extension to the standard strings package offering
macro styling functions. Normally, these functions are not suitable to be
in the standard strings package as they became slightly specific to use
cases, which is outside the standard package's sole objective.

This package offers functions like Indent, WordWrapping, etc., further the
use of formatting or styling strings.





## Constants

```go
const (
	// CharTabIndent is the tab character for indent usage
	CharTabIndent = "\t"


	// CharSpaceIndent is the space character for indent usage
	CharSpaceIndent = " "


	// CharLegacyNewLine is the CR character ("\r") for newline
	// usage
	CharLegacyNewLine = "\r"


	// CharUNIXNewLine is the LF character ("\n") for newline
	// usage, commonly used in modern UNIX systems.
	CharUNIXNewLine = "\n"


	// CharWindowsNewLine is the CRLF character("\r\n") for
	// newline usage, commonly used in modern Windows systems.
	CharWindowsNewLine = "\r\n"
)
```





## Styler
```go
type Styler struct {
}
```
Styler is the data structure for styling a given string. It is safe to
create using the conventional structure{} method.



### `Func (s *Styler) ContentWrap(paragraphs string, limit uint, newLineCharacter string) (out []string)`
ContentWrap performs multiple paragraphs word wrapping against a
characters-width boundary limit, producing a slice with multi-line strings
with designed newline convention to separate each paragraphs.

By default, ContentWrap uses the LF ("\n") newLine characters. This is true
when newLineCharacter is empty ("") or defined to use LF
(strhelper.CharUNIXNewLine). If you need other variant like CRLF ("\r\n"),
you can supply strhelper.CharWindowsNewLine to newLineCharacter parameter.
If the newLineCharacter has unrecognizable string character(s), this
function returns nil.

When the limit is below a word length, this function returns a minimum of
1 word regardless of its word length.

If the limit is 0, this function returns nil.

It returns:
  1. data          - successful word-wrap in string slice
  2. nil           - incomprehensible input(s) for wrapping



### `Func (s *Styler) Indent(paragraph []string, indentChar string, indentSize uint) (out []string)`
Indent is shifting a paragraph list towards left by the defined size
(indentSize) using the given indent character (indentChar). This function
creates the indent by repeatedly generate the given indent character n
times, where n is the defined size before proceeding to next line.

By default, the character is space (strhelper.CharSpaceIndent). If tab
character (strhelper.CharTabIndent) is provided, IndentLeft will use tab
instead. If you provide anything other than those 2, this function returns
nil.

If the given paragraph is having 0 length or nil, it returns the given
paragraph as output.

If the given indentSize is 0, however, it returns nil instead.

It returns:
  1.  data     - successful indent
  2.  nil      - indent size is 0 or given paragraph is 0 or nil



### `Func (s *Styler) ToUnix(paragraphs string) (out string)`
ToUnix is to convert a set of given paragraphs (regardless input os) to use
Unix newline convention: LF ("\n").



### `Func (s *Styler) ToWindows(paragraphs string) (out string)`
ToWindows is to convert a set of given paragraphs (regardless input os) to
use Windows new line convention: CRLF ("\r\n").



### `Func (s *Styler) WordWrap(paragraph string, limit uint) (out []string)`
If the limit is 0, this function returns nil.

It returns:
  1. data          - successful word-wrap in string slice
  2. nil           - incomprehensible limit for wrapping
