+++
date = "2020-09-21T13:15:04+08:00"
title = "v0.0.1 Release Note"
description = """
This is the release note for v0.0.1 Cerigo packages. Here, you will what are
the major changes, backward-compatible changes, new features, and etc. for
Cerigo version v0.0.1.
"""
keywords = ["v0.0.1", "cerigo", "release note"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
	# Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "releases/release-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Cerigo"

[thumbnails.1]
url = "releases/release-1200x628.png"
width = "1200"
height = "628"
alternateText = "Cerigo"


[menu.main]
parent = "R) Release Notes"
name = "v0.0.1"
pre = "🧾"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## What's New
Here are some of the key changes:

1. changed all release tagging to branch releases.
2. support `algo/cmdchain`
3. support `encoding/float`
4. support `strings/strhelper`
5. support `testing/thelper`
6. support `os/args`
7. support `os/file/filehelper`
8. support `os/term`



### Backward Compatible
Here are some of the backward compatible changes:

1. changed all release tagging to branch releases.
2. support `algo/cmdchain` for implementing command chain algorithm.
3. support `encoding/float` for manipulating float values.
4. support `strings/strhelper` for advanced string styling.
5. support `testing/thelper` for scalable Go unit testing.
6. support `os/args` for systematic command line parameters parsing.
7. support `os/file/filehelper` for managing files.
8. support `os/term` for terminal display management.



### Non-Backward Compatible
Here are some of the breaking changes:

1. changed all release tag strategy to branch release strategy.



## Deprecation Notice
Here are some of the known deprecation notices:

**NONE**




## Epilogue
That's all for this release. Should you have any questions or found any issue
please do not hesitate to contact the development team at:

https://gitlab.com/zoralab/cerigo/-/issues
