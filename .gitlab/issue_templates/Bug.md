# Description
Describe WHAT is the bug here.



## Expected Behavior
Explain the expected behavior.



## Current Behavior
Explain the current behavior.



## How to Reproduce
1. Describe the steps to reproduce the issues.
2. You can do it step by step demonstrated here.
3. Just remember to leave a space after your numbering.
2. You can use the "Add files" to add screenshots too.
3. Just list the steps out crisp and clear.



## Severity
> Leave only ONE. Delete the rest including this label.

1. ***Critical*** - danger, hang, freeze, or kill computer
2. ***Severe*** - blocking and can't use. Can't workaround it.
3. ***Significant*** - quite a problem but still usable. Can workaround it.
4. ***Not significant*** - just some minor touch up. Everything is working fine.



## Urgency
> Leave only ONE. Delete the rest including this label.

1. ***Code Black*** - Somebody's life is at stake (e.g. medical equipment, national security)
2. ***Code Red*** - Immediate Attention. (e.g. My business is not running at all)
3. ***Code Blue*** - Please plan out. (e.g. I can survive for now)
4. ***Code Green*** - Not urgent. (e.g. take your time)



----
----

## Automated Settings
> Don't worry! Let Cory takes over here.

/label ~"Bug"
