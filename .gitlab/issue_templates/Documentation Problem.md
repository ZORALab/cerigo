# Description
Describe WHAT is the documentation problem.



## Where is the Problem
### Link
Copy and Paste Your Link here.

### Version
State the current version.



## Current State
Explain the current problem.



## Expected State
Explain the expected outcome.



## Severity
> Leave only ONE. Delete the rest including this label.
1. ***Critical*** - danger, hang, freeze, or kill computer
2. ***Severe*** - blocking and can't use. Can't workaround it.
3. ***Significant*** - quite a problem but still usable. Can workaround it.
4. ***Not significant*** - just some minor touch up. Everything is working fine.



## Urgency
> Leave only ONE. Delete the rest including this label.
1. ***Code Black*** - Somebody's life is at stake (e.g. medical equipment, national security)
2. ***Code Red*** - Immediate Attention. (e.g. My business is not running at all)
3. ***Code Blue*** - Please plan out. (e.g. I can survive for now)
4. ***Code Green*** - Not urgent. (e.g. take your time)


----
----

## Automated Settings
> Don't worry! Let Cory takes over here.

/label ~"Discussion" ~"Documentation"
