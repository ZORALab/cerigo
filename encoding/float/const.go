package float

// known base types
const (
	base2  = 2
	base8  = 8
	base10 = 10
	base36 = 36

	minBase = base2
	maxBase = base36
)

// float types
const (
	floatNone  = 0
	float32bit = 1
	float64bit = 2
	floatBig   = 3
)
