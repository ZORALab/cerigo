package float

// Format Type Enumerations
const (
	// Normal is the normal REAL number format.
	//
	// This is essentially the same as math.big.Float.Text('g', precision).
	// The format is:
	//   1. -d.dddde±dd --> for large exponents
	//   2. -ddddd.dddd --> for otherwise
	Normal = 0

	// Scientific is the scientific REAL number format.
	//
	// This is the presentation for scientific community's representation.
	// The format is:
	//   1. -ddddd.dddd*10^(±dd)
	Scientific = 1

	// ISO6093NR1 is the REAL number containing only round numbers format.
	//
	// This is essentially the same as math.big.Float.Text('f', 0). The
	// format is:
	//   1. -ddddd
	ISO6093NR1 = 2

	// ISO6093NR2 is the REAL number containing decimal number only format.
	//
	// This is the essentially the same as
	// math.big.Float.Text('f', precision). The format is:
	//   1. -ddddd.dddd
	ISO6093NR2 = 3

	// ISO6093NR3 is the decimal number and exponent format.
	//
	// This is essentially the same as math.big.Float.Text('e', precision).
	// The format is:
	//   1. -d.dddde±dd
	ISO6093NR3 = 4
)

// Normalization Enumerations
const (
	// NormalizeNormal is to normalize toward single digit round number.
	//
	// Example:
	//   1. XXXX.YYYYeZZZ --> X.XXXYYYYe(ZZZ+3)
	NormalizeNormal = 0

	// NormalizeFullRound is to normalize toward full round numbers.
	//
	// Example:
	//   1. XXXX.YYYYeZZZ --> XXXXYYYY.0e(ZZZ-4)
	NormalizeFullRound = 1

	// NormalizeNone is to not apply any normalization.
	//
	// Example:
	//   1. XXXX.YYYYeZZZ --> XXXX.YYYYeZZZ
	NormalizeNone = 2
)
