package float

import (
	"testing"
)

func TestToBase(t *testing.T) {
	scenarios := testENDECScenarios()

	for i, s := range scenarios {
		if s.TestType != testToBase {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		e := New()
		float := s.prepareFloat64()
		base := s.prepareBase()
		accuracy := s.prepareAccuracy()
		precision := s.preparePrecision()
		normalization := s.prepareNormalization()
		result, resultErr := s.prepareToBaseResult()

		if !s.Switches[useFloatNone] {
			e.ParseFloat64(float)
		}

		// test
		x, err := e.ToBase(base, normalization, accuracy, precision)
		str := ""

		if x != nil {
			str = x.String()
		}

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStrings("given", result, "got", str)
		th.ExpectError(err, resultErr)
		s.log(th, map[string]interface{}{
			"ENDEC        ": e,
			"expect       ": result,
			"output       ": x,
			"output string": str,
			"base         ": base,
			"float        ": float,
			"accuracy     ": accuracy,
			"precision    ": precision,
			"normalization": normalization,
		})
		th.Conclude()
	}
}
