// package float is an encoder/decoder for floating-points numerical values.
//
// The encoder/decoder (ENDEC) is meant to process the presentation of a given
// floating-points number, either in a form of float32, float64 or
// math/big.Float object.
//
// The ENDEC also supports base numbering conversion, allowing one to convert
// floating-points number from the conventional Base 10 representation to
// anywhere between 2 to 36.
//
// MATHEMATICAL BASE CONVERSION
//
// The mathematical model for base conversion relies on the following
// derrivatives:
//     B^y = 10^x
//     y = x*logB(10)  --- [1]
//
//     From [1],
//     y = ‖x*(log2(10) / log2(B))‖
//     y = ‖x*c, c = log2(10) / log2(B)‖    --- [2]
//
//     ∴,
//     base2,
//     y = x*c, c = log2(10) / log2(2)
//     y = x*c, c = log2(10)
//     y = ‖x*log2(10)‖
//
//     base5,
//     y = x*log5(10)
//     y = ‖x*c, c = log2(10) / log2(5)‖
//
//     base8,
//     y = x*log8(10)
//     y = x*c, c = log2(10) / log2(8)
//     y = ‖x*c, c = log2(10) / 3‖
//
//     base16,
//     y = x*log16(10)
//     y = x*c, c = log2(10) / log2(16)
//     y = ‖x*c, c = log2(10) / 4‖
//
// With base conversion made available for exponent values, the rest would be
// converting the mantissa, where round numbers and partial numbers are
// converted separately.
//
// For round number, it is a direct conversion using standard library strconv
// package. It has both ParseFloat(...) and FormatFloat(...) functions that
// can easily translate any round numbers.
//
// For partial numbers, it is converted using base number multiplication upto
// a given precision limit.
//
// APPLIED OPTIMIZATIONS
//
// 1. c Constant Lookup Table
//
// Currently, the exponent's base number mathematical model relies on division.
// To speed up the process, the constant c is pre-calculated and tabulated into
// a look-up table for known bases like Base-2, Base-8, and Base-16. Otherwise,
// it will falls back to manual division calculations.
//
// LIMITATIONS
//
// 1. Accuracy
//
// The ENDEC is under Go's float numbers' limitations and c Constant's
// accuracy. Otherwise, the ENDEC is usable for any base conversion. User must
// keep in mind that the more operations done onto a floating point numbers,
// the more accuracy it loses. Hence, it is best to use ENDEC once all
// calculations are done in conventional Go.
package float
