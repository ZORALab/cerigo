package float

func testENDECScenarios() []testENDECScenario {
	return []testENDECScenario{
		{
			UID:      1,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				usePositiveFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      2,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero float32
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				useZeroFloat32:      true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      3,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative good float32
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				useNegativeFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      4,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. none
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      5,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. zero accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:    true,
				usePositiveFloat32: true,
				useZeroAccuracy:    true,
			},
		}, {
			UID:      6,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. negative accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				usePositiveFloat32:  true,
				useNegativeAccuracy: true,
			},
		}, {
			UID:      7,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. positive accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat: true,
				usePositiveFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      8,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero float32
2. positive accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat: true,
				useZeroFloat32:      true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      9,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative good float32
2. positive accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat: true,
				useNegativeFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      10,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. none
2. positive accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat: true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      11,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. zero accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat: true,
				usePositiveFloat32:  true,
				useZeroAccuracy:     true,
			},
		}, {
			UID:      12,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. negative accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat: true,
				usePositiveFloat32:  true,
				useNegativeAccuracy: true,
			},
		}, {
			UID:      13,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero float32
2. positive accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format: true,
				useZeroFloat32:      true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      14,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative good float32
2. positive accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format: true,
				useNegativeFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      15,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. none
2. positive accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format: true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      16,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. zero accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format: true,
				usePositiveFloat32:  true,
				useZeroAccuracy:     true,
			},
		}, {
			UID:      17,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. negative accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format: true,
				usePositiveFloat32:  true,
				useNegativeAccuracy: true,
			},
		}, {
			UID:      18,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. positive accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format: true,
				usePositiveFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      19,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. positive accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format: true,
				usePositiveFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      20,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero float32
2. positive accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format: true,
				useZeroFloat32:      true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      21,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative good float32
2. positive accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format: true,
				useNegativeFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      22,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. none
2. positive accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format: true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      23,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. zero accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format: true,
				usePositiveFloat32:  true,
				useZeroAccuracy:     true,
			},
		}, {
			UID:      24,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. negative accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format: true,
				usePositiveFloat32:  true,
				useNegativeAccuracy: true,
			},
		}, {
			UID:      25,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero float32
2. positive accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format: true,
				useZeroFloat32:      true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      26,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative good float32
2. positive accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format: true,
				useNegativeFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      27,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. none
2. positive accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format: true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      28,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. zero accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format: true,
				usePositiveFloat32:  true,
				useZeroAccuracy:     true,
			},
		}, {
			UID:      29,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. negative accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format: true,
				usePositiveFloat32:  true,
				useNegativeAccuracy: true,
			},
		}, {
			UID:      30,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32
2. positive accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format: true,
				usePositiveFloat32:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      31,
			TestType: testParseFloat32,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive float32
`,
			Switches: map[string]bool{
				usePositiveFloat32: true,
			},
		}, {
			UID:      32,
			TestType: testParseFloat32,
			Description: `
ENDEC.Parse32 is able to parse:
1. negative float32
`,
			Switches: map[string]bool{
				useNegativeFloat32: true,
			},
		}, {
			UID:      33,
			TestType: testParseFloat32,
			Description: `
ENDEC.Parse32 is able to parse:
1. zero float32
`,
			Switches: map[string]bool{
				useZeroFloat32: true,
			},
		}, {
			UID:      34,
			TestType: testParseFloat32,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive float32
2. ISO6093NR2 Format
`,
			Switches: map[string]bool{
				useZeroFloat32:      true,
				useISO6093NR2Format: true,
			},
		}, {
			UID:      35,
			TestType: testParseFloat32,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive float32
2. ISO6093NR3 Format
`,
			Switches: map[string]bool{
				useZeroFloat32:      true,
				useISO6093NR3Format: true,
			},
		}, {
			UID:      36,
			TestType: testParseFloat64,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive float32
`,
			Switches: map[string]bool{
				usePositiveFloat64: true,
			},
		}, {
			UID:      37,
			TestType: testParseFloat64,
			Description: `
ENDEC.Parse32 is able to parse:
1. negative float64
`,
			Switches: map[string]bool{
				useNegativeFloat64: true,
			},
		}, {
			UID:      38,
			TestType: testParseFloat64,
			Description: `
ENDEC.Parse32 is able to parse:
1. zero float64
`,
			Switches: map[string]bool{
				useZeroFloat64: true,
			},
		}, {
			UID:      39,
			TestType: testParseFloat64,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive float64
2. ISO6093NR2 Format
`,
			Switches: map[string]bool{
				useZeroFloat64:      true,
				useISO6093NR2Format: true,
			},
		}, {
			UID:      40,
			TestType: testParseFloat64,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive float64
2. ISO6093NR3 Format
`,
			Switches: map[string]bool{
				useZeroFloat64:      true,
				useISO6093NR3Format: true,
			},
		}, {
			UID:      41,
			TestType: testParseBigFloat,
			Description: `
ENDEC.Parse32 is able to parse:
1. negative *bigFloat
`,
			Switches: map[string]bool{
				useNegativeFloat64: true,
				convertToBigFloat:  true,
			},
		}, {
			UID:      42,
			TestType: testParseBigFloat,
			Description: `
ENDEC.Parse32 is able to parse:
1. zero *bigFloat
`,
			Switches: map[string]bool{
				useZeroFloat64:    true,
				convertToBigFloat: true,
			},
		}, {
			UID:      43,
			TestType: testParseBigFloat,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive *bigFloat
2. ISO6093NR2 Format
`,
			Switches: map[string]bool{
				useZeroFloat64:      true,
				useISO6093NR2Format: true,
				convertToBigFloat:   true,
			},
		}, {
			UID:      44,
			TestType: testParseBigFloat,
			Description: `
ENDEC.Parse32 is able to parse:
1. positive *bigFloat
2. ISO6093NR3 Format
`,
			Switches: map[string]bool{
				useZeroFloat64:      true,
				useISO6093NR3Format: true,
				convertToBigFloat:   true,
			},
		}, {
			UID:      45,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float64
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				usePositiveFloat64:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      46,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative float64
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				useNegativeFloat64:  true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      47,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero float64
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				useZeroFloat64:      true,
				usePositiveAccuracy: true,
			},
		}, {
			UID:      48,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive *big.Float
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				usePositiveFloat64:  true,
				usePositiveAccuracy: true,
				convertToBigFloat:   true,
			},
		}, {
			UID:      49,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. negative *big.Float
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				useNegativeFloat64:  true,
				usePositiveAccuracy: true,
				convertToBigFloat:   true,
			},
		}, {
			UID:      50,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. zero *big.Float
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:     true,
				useZeroFloat64:      true,
				usePositiveAccuracy: true,
				convertToBigFloat:   true,
			},
		}, {
			UID:      51,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32 with negative exponent
2. positive accuracy
3. to Normal format
`,
			Switches: map[string]bool{
				useNormalFormat:            true,
				useNegativeExponentFloat32: true,
				usePositiveAccuracy:        true,
			},
		}, {
			UID:      52,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32 with negative exponent
2. positive accuracy
3. to Scientific format
`,
			Switches: map[string]bool{
				useScientificFormat:        true,
				useNegativeExponentFloat32: true,
				usePositiveAccuracy:        true,
			},
		}, {
			UID:      53,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32 with negative exponent
2. positive accuracy
3. to ISO6093NR1 format
`,
			Switches: map[string]bool{
				useISO6093NR1Format:        true,
				useNegativeExponentFloat32: true,
				usePositiveAccuracy:        true,
			},
		}, {
			UID:      54,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32 with negative exponent
2. positive accuracy
3. to ISO6093NR2 format
`,
			Switches: map[string]bool{
				useISO6093NR2Format:        true,
				useNegativeExponentFloat32: true,
				usePositiveAccuracy:        true,
			},
		}, {
			UID:      55,
			TestType: testToString,
			Description: `
ENDEC.ToString is able to convert:
1. positive float32 with negative exponent
2. positive accuracy
3. to ISO6093NR3 format
`,
			Switches: map[string]bool{
				useISO6093NR3Format:        true,
				useNegativeExponentFloat32: true,
				usePositiveAccuracy:        true,
			},
		}, {
			UID:      56,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. minimum base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseMin:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      57,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. minimum base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseMin:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      58,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. minimum base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseMin:                 true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      59,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. odd base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseOdd:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      60,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. odd base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseOdd:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      61,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. odd base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseOdd:                 true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      62,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. even base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      63,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. even base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      64,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. even base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseEven:                true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      65,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. max base (36)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseMax:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      66,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. max base (36)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseMax:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      67,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. max base (36)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseMax:                 true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      68,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. bad base (min - 1)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseBadLower:     true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      69,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. bad base (min - 1)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseBadLower:     true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      70,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. bad base (min - 1)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseBadLower:            true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      71,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. bad base (max + 1)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseBadHigher:    true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      72,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. bad base (max + 1)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseBadHigher:    true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      73,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. bad base (max + 1)
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseBadHigher:           true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      74,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. none float64
2. min base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useFloatNone:        true,
				useBaseMin:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      75,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. base-8
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseEight:        true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      76,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. base-10
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseTen:          true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      77,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. even base
3. positive accuracy
4. normal normalization
5. zero precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionZero:    true,
			},
		}, {
			UID:      78,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. even base
3. positive accuracy
4. normal normalization
5. zero precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionZero:    true,
			},
		}, {
			UID:      79,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. even base
3. positive accuracy
4. normal normalization
5. zero precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseEven:                true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionZero:           true,
			},
		}, {
			UID:      80,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. even base
3. positive accuracy
4. normal normalization
5. big precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionBig:     true,
			},
		}, {
			UID:      81,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. even base
3. positive accuracy
4. normal normalization
5. big precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionBig:     true,
			},
		}, {
			UID:      82,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. even base
3. positive accuracy
4. normal normalization
5. big precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseEven:                true,
				usePositiveAccuracy:        true,
				useNormalizeNormal:         true,
				usePrecisionBig:            true,
			},
		}, {
			UID:      83,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. even base
3. positive accuracy
4. full round normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:    true,
				useBaseEven:           true,
				usePositiveAccuracy:   true,
				useNormalizeFullRound: true,
				usePrecisionNormal:    true,
			},
		}, {
			UID:      84,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. even base
3. positive accuracy
4. full round normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:    true,
				useBaseEven:           true,
				usePositiveAccuracy:   true,
				useNormalizeFullRound: true,
				usePrecisionNormal:    true,
			},
		}, {
			UID:      85,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. even base
3. positive accuracy
4. full round normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseEven:                true,
				usePositiveAccuracy:        true,
				useNormalizeFullRound:      true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      86,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64
2. even base
3. positive accuracy
4. none normalization
5. normal precision
`,
			Switches: map[string]bool{
				usePositiveFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNone:    true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      87,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. negative float64
2. even base
3. positive accuracy
4. none normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeFloat64:  true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNone:    true,
				usePrecisionNormal:  true,
			},
		}, {
			UID:      88,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. positive float64 with negative exponent
2. even base
3. positive accuracy
4. none normalization
5. normal precision
`,
			Switches: map[string]bool{
				useNegativeExponentFloat64: true,
				useBaseEven:                true,
				usePositiveAccuracy:        true,
				useNormalizeNone:           true,
				usePrecisionNormal:         true,
			},
		}, {
			UID:      89,
			TestType: testToBase,
			Description: `
ENDEC.ToBase is able to convert:
1. zero float64
2. even base
3. positive accuracy
4. normal normalization
5. normal precision
`,
			Switches: map[string]bool{
				useZeroFloat64:      true,
				useBaseEven:         true,
				usePositiveAccuracy: true,
				useNormalizeNormal:  true,
				usePrecisionNormal:  true,
			},
		},
	}
}
