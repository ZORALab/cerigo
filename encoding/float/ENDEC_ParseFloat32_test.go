package float

import (
	"testing"
)

func TestParse32(t *testing.T) {
	scenarios := testENDECScenarios()

	for i, s := range scenarios {
		if s.TestType != testParseFloat32 {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		e := New()
		f := s.prepareFloat32()

		// test
		e.ParseFloat32(f)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertENDECParse32(th, e, f)
		s.log(th, map[string]interface{}{
			"ENDEC   ": e,
			"float32 ": f,
		})
		th.Conclude()
	}
}
