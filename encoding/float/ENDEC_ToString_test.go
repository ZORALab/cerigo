package float

import (
	"testing"
)

func TestToString(t *testing.T) {
	scenarios := testENDECScenarios()

	for i, s := range scenarios {
		if s.TestType != testToString {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		e := New()
		result := s.prepareENDEC(e)
		accuracy := s.prepareAccuracy()
		format := s.prepareFormat()

		// test
		x := e.ToString(format, accuracy)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStrings("given", result, "got", x)
		s.log(th, map[string]interface{}{
			"ENDEC   ": e,
			"expect  ": result,
			"output  ": x,
			"format  ": format,
			"accuracy": accuracy,
		})
		th.Conclude()
	}
}
