package float

import (
	"testing"
)

func TestParse64(t *testing.T) {
	scenarios := testENDECScenarios()

	for i, s := range scenarios {
		if s.TestType != testParseFloat64 {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		e := New()
		f := s.prepareFloat64()

		// test
		e.ParseFloat64(f)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertENDECParse64(th, e, f)
		s.log(th, map[string]interface{}{
			"ENDEC   ": e,
			"float32 ": f,
		})
		th.Conclude()
	}
}
