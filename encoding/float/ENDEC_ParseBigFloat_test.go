package float

import (
	"testing"
)

func TestParseBigFloat(t *testing.T) {
	scenarios := testENDECScenarios()

	for i, s := range scenarios {
		if s.TestType != testParseBigFloat {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		e := New()
		f := s.prepareBigFloat()

		// test
		e.ParseBigFloat(f)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertENDECParseBigFloat(th, e, f)
		s.log(th, map[string]interface{}{
			"ENDEC     ": e,
			"big Float ": f,
		})
		th.Conclude()
	}
}
