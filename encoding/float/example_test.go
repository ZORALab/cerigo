package float

import (
	"fmt"
)

func Example() {
	f := float32(12.321231e-123)

	// 1. Create the endec using float.New() function.
	b := New()

	// 2. Parse your float data points.
	b.ParseFloat32(f)

	// 3. Present your float data in other format.
	s := b.ToString(Normal, 10)
	fmt.Println(s)

	// 4. Convert to other base numbers.
	t, err := b.ToBase(9, NormalizeNormal, 10, 5)
	if err != nil {
		// handle error
		return
	}

	fmt.Println(t)
}
