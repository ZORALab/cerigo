package float

import (
	"math/big"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

// test values
const (
	// base value
	baseEven = 16
	baseOdd  = 9

	// accuracies
	negativeAccuracy = int(-10)
	positiveAccuracy = int(5)

	// float32
	float32NegativeExponentValue   = float32(1.2e-10)
	float64NegativeExponentValue   = float64(1.23452352354e-256)
	float32NegativeValue           = -float32PositiveValue
	float64NegativeValue           = -float64PositiveValue
	float32NegativeISO6093NR2Value = -float32PositiveISO6093NR2Value
	float64NegativeISO6093NR2Value = -float64PositiveISO6093NR2Value
	float32PositiveValue           = float32(1.2e10)
	float64PositiveValue           = float64(1.23452352354e256)
	float32PositiveISO6093NR2Value = float32(123.4567)
	float64PositiveISO6093NR2Value = float64(123.4567)
	zeroFloat32Value               = float32(0)
	zeroFloat64Value               = float64(0)

	// float32 results
	float32NormalNegativeExponentResult    = "1.2e-10"
	float32NormalNegativeResult            = "-1.2e+10"
	float32NormalPositiveNegativeAccResult = "1.2e+10"
	float32NormalPositiveResult            = "1.2e+10"
	float32NormalPositiveZeroAccResult     = "1e+10"
	float32NormalZeroResult                = "0"

	float32ISO6093NR1NegativeExponentResult    = "0"
	float32ISO6093NR1NegativeResult            = "-12000000000"
	float32ISO6093NR1PositiveNegativeAccResult = "12000000000"
	float32ISO6093NR1PositiveResult            = "12000000000"
	float32ISO6093NR1PositiveZeroAccResult     = "12000000000"
	float32ISO6093NR1ZeroResult                = "0"

	float32ISO6093NR2NegativeExponentResult    = "0.00000"
	float32ISO6093NR2NegativeResult            = "-123.45670"
	float32ISO6093NR2PositiveNegativeAccResult = "123.45670318603516"
	float32ISO6093NR2PositiveResult            = "123.45670"
	float32ISO6093NR2PositiveZeroAccResult     = "123"
	float32ISO6093NR2ZeroResult                = "0.00000"

	float32ISO6093NR3NegativeExponentResult    = "1.20000e-10"
	float32ISO6093NR3NegativeResult            = "-1.20000e+10"
	float32ISO6093NR3PositiveNegativeAccResult = "1.2e+10"
	float32ISO6093NR3PositiveResult            = "1.20000e+10"
	float32ISO6093NR3ZeroResult                = "0.00000e+00"
	float32ISO6093NR3PositiveZeroAccResult     = "1e+10"

	float32ScientificNegativeExponentResult    = "1.20000*10^(-10)"
	float32ScientificNegativeResult            = "-1.20000*10^(+10)"
	float32ScientificPositiveNegativeAccResult = "1.2*10^(+10)"
	float32ScientificPositiveResult            = "1.20000*10^(+10)"
	float32ScientificZeroResult                = "0.00000*10^(+00)"
	float32ScientificPositiveZeroAccResult     = "1*10^(+10)"

	// float64 results
	float64NormalNegativeResult = "-1.2345e+256"
	float64NormalPositiveResult = "1.2345e+256"
	float64NormalZeroResult     = "0"

	float64MinBaseNormalResult                 = "1.001111*2^(+850)"
	float64MinBaseNormalNegativeResult         = "-1.001111*2^(+850)"
	float64MinBaseNormalNegativeExponentResult = "0.00111111" +
		"*2^(-9223372036854775808)"
	float64OddBaseNormalResult                 = "1.20886148*9^(+268)"
	float64OddBaseNormalNegativeResult         = "-1.20886148*9^(+268)"
	float64OddBaseNormalNegativeExponentResult = "1.088864071" +
		"*9^(-9223372036854775808)"
	float64EvenBaseNormalResult                 = "1.3c0980b242*16^(+213)"
	float64EvenBaseNormalNegativeResult         = "-1.3c0980b242*16^(+213)"
	float64EvenBaseNormalNegativeExponentResult = "1.f9a8cdea" +
		"*16^(-9223372036854775808)"
	float64MaxBaseNormalResult                 = "1.8fxrjlfskx*36^(+164)"
	float64MaxBaseNormalNegativeResult         = "-1.8fxrjlfskx*36^(+164)"
	float64MaxBaseNormalNegativeExponentResult = "4.fzrycxz9a" +
		"*36^(-9223372036854775808)"

	float64Base8NormalResult  = "1.1700460054*8^(+283)"
	float64Base10NormalResult = "1.23452*10^(+256)"

	float64ZeroPercisionNegativeExponentResult = "1.f9a8cdea033e8" +
		"*16^(-9223372036854775808)"
	float64ZeroPercisionNegativeResult = "-1.3c0980b242070c*16^(+213)"
	float64ZeroPercisionPositiveResult = "1.3c0980b242070c*16^(+213)"

	float64NegativePercisionNegativeExponentResult = "1.f9a8cdea033e8" +
		"*16^(-9223372036854775808)"
	float64NegativePercisionNegativeResult = "-1.3c0980b242070c*16^(+213)"
	float64NegativePercisionPositiveResult = "1.3c0980b242070c*16^(+213)"

	float64BigPercisionNegativeExponentResult = "1.f9a8cdea033e8" +
		"*16^(-9223372036854775808)"
	float64BigPercisionNegativeResult = "-1.3c0980b242070c*16^(+213)"
	float64BigPercisionPositiveResult = "1.3c0980b242070c*16^(+213)"

	float64MinBaseFullRoundResult = "13c0980b242.0" +
		"*16^(+203)"
	float64MinBaseFullRoundNegativeResult = "-13c0980b242.0" +
		"*16^(+203)"
	float64MinBaseFullRoundNegativeExponentResult = "1f9a8cdea0.0" +
		"*16^(-9223372036854775808)"

	float64MinBaseNoneResult                 = "1.3c0980b242*16^(+213)"
	float64MinBaseNoneNegativeResult         = "-1.3c0980b242*16^(+213)"
	float64MinBaseNoneNegativeExponentResult = "0.1f9a8cdea0" +
		"*16^(-9223372036854775808)"

	float64MinBaseNoneZeroResult = "0.0"

	// normalize
	normalizeBadValue = -125

	// precision
	precisionBigValue      = 100
	precisionNegativeValue = -10
	precisionNormalValue   = 10
	precisionZeroValue     = 0
)

// testTypes
const (
	testToBase        = "testToBase"
	testToString      = "testToString"
	testParseFloat32  = "testParseFloat32"
	testParseFloat64  = "testParseFloat64"
	testParseBigFloat = "testPraseBigFloat"
)

// switches
const (
	convertToBigFloat          = "convertToBigFloat"
	useBadNormalization        = "useBadNormalization"
	useBaseBadLower            = "useBaseBadLower"
	useBaseBadHigher           = "useBaseBadHigher"
	useBaseEight               = "useBaseEight"
	useBaseEven                = "useBaseEven"
	useBaseMax                 = "useBaseMax"
	useBaseMin                 = "useBaseMin"
	useBaseOdd                 = "useBaseOdd"
	useBaseTen                 = "useBaseTen"
	useFloatNone               = "useFloatNone"
	useISO6093NR1Format        = "useISO6093NR1Format"
	useISO6093NR2Format        = "useISO6093NR2Format"
	useISO6093NR3Format        = "useISO6093NR3Format"
	useNegativeAccuracy        = "useNegativeAccuracy"
	useNegativeExponentFloat32 = "useNegativeExponentFloat32"
	useNegativeExponentFloat64 = "useNegativeExponentFloat64"
	useNegativeFloat32         = "useNegativeFloat32"
	useNegativeFloat64         = "useNegativeFloat64"
	useNormalFormat            = "useNormalFormat"
	useNormalizeNormal         = "useNormalizeNormal"
	useNormalizeFullRound      = "useNormalizeFullRound"
	useNormalizeNone           = "useNormalizeNone"
	usePositiveAccuracy        = "usePositiveAccuracy"
	usePositiveFloat32         = "usePositiveFloat32"
	usePositiveFloat64         = "usePositiveFloat64"
	usePrecisionBig            = "usePrecisionBig"
	usePrecisionNormal         = "usePrecisionNormal"
	usePrecisionNegative       = "usePrecisionNegative"
	usePrecisionZero           = "usePrecisionZero"
	useScientificFormat        = "useScientificFormat"
	useZeroAccuracy            = "useZeroAccuracy"
	useZeroFloat32             = "useZeroFloat32"
	useZeroFloat64             = "useZeroFloat64"
)

type testENDECScenario thelper.Scenario

func (s *testENDECScenario) log(th *thelper.THelper,
	data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testENDECScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testENDECScenario) assertENDECParse32(th *thelper.THelper,
	e *ENDEC, f float32) {
	if s.Switches[usePositiveFloat32] ||
		s.Switches[useZeroFloat32] ||
		s.Switches[useNegativeFloat32] {
		th.ExpectSameFloat32("given", f, "got", e.f32)

		if e.fType != float32bit {
			th.Errorf("fType was not set to float32Bit")
		}
	}
}

func (s *testENDECScenario) assertENDECParse64(th *thelper.THelper,
	e *ENDEC, f float64) {
	if s.Switches[usePositiveFloat64] ||
		s.Switches[useZeroFloat64] ||
		s.Switches[useNegativeFloat64] {
		th.ExpectSameFloat64("given", f, "got", e.f64)

		if e.fType != float64bit {
			th.Errorf("fType was not set to float64Bit")
		}
	}
}

func (s *testENDECScenario) assertENDECParseBigFloat(th *thelper.THelper,
	e *ENDEC, f *big.Float) {
	if s.Switches[usePositiveFloat64] ||
		s.Switches[useZeroFloat64] ||
		s.Switches[useNegativeFloat64] {
		th.ExpectSameBigFloat("given", f, "got", e.fb)

		if e.fType != floatBig {
			th.Errorf("fType was not set to floatBigFloat")
		}
	}
}

func (s *testENDECScenario) prepareFloat32() (result float32) {
	switch {
	case s.Switches[usePositiveFloat32]:
		result = float32PositiveValue
		if s.Switches[useISO6093NR2Format] {
			result = float32PositiveISO6093NR2Value
		}
	case s.Switches[useZeroFloat32]:
		result = zeroFloat32Value
	case s.Switches[useNegativeFloat32]:
		result = float32NegativeValue
		if s.Switches[useISO6093NR2Format] {
			result = float32NegativeISO6093NR2Value
		}
	case s.Switches[useNegativeExponentFloat32]:
		result = float32NegativeExponentValue
	default:
		result = zeroFloat32Value
	}

	return result
}

func (s *testENDECScenario) prepareFloat64() (result float64) {
	switch {
	case s.Switches[usePositiveFloat64]:
		result = float64PositiveValue
		if s.Switches[useISO6093NR2Format] {
			result = float64PositiveISO6093NR2Value
		}
	case s.Switches[useZeroFloat64]:
		result = zeroFloat64Value
	case s.Switches[useNegativeFloat64]:
		result = float64NegativeValue
		if s.Switches[useISO6093NR2Format] {
			result = float64NegativeISO6093NR2Value
		}
	case s.Switches[useNegativeExponentFloat64]:
		result = float64NegativeExponentValue
	default:
		result = zeroFloat64Value
	}

	return result
}

func (s *testENDECScenario) prepareBigFloat() (result *big.Float) {
	if !s.Switches[convertToBigFloat] {
		return result
	}

	switch {
	case s.Switches[usePositiveFloat64] ||
		s.Switches[useZeroFloat64] ||
		s.Switches[useNegativeFloat64]:
		result = big.NewFloat(s.prepareFloat64())
	case s.Switches[usePositiveFloat32] ||
		s.Switches[useZeroFloat32] ||
		s.Switches[useNegativeFloat32] ||
		s.Switches[useNegativeExponentFloat32]:
		result = big.NewFloat(float64(s.prepareFloat32()))
	default:
		result = nil
	}

	return result
}

func (s *testENDECScenario) prepareENDEC(e *ENDEC) (result string) {
	switch {
	case s.Switches[convertToBigFloat]:
		e.fType = floatBig
		e.fb = s.prepareBigFloat()
	case s.Switches[usePositiveFloat32] ||
		s.Switches[useZeroFloat32] ||
		s.Switches[useNegativeFloat32] ||
		s.Switches[useNegativeExponentFloat32]:
		e.f32 = s.prepareFloat32()
		e.fType = float32bit
	case s.Switches[usePositiveFloat64] ||
		s.Switches[useZeroFloat64] ||
		s.Switches[useNegativeFloat64]:
		e.fType = float64bit
		e.f64 = s.prepareFloat64()
	}

	return s.prepareStringResult()
}

func (s *testENDECScenario) prepareAccuracy() (x int) {
	switch {
	case s.Switches[usePositiveAccuracy]:
		x = positiveAccuracy
	case s.Switches[useNegativeAccuracy]:
		x = negativeAccuracy
	default:
	}

	return x
}

func (s *testENDECScenario) prepareFormat() (x int) {
	switch {
	case s.Switches[useNormalFormat]:
		x = Normal
	case s.Switches[useScientificFormat]:
		x = Scientific
	case s.Switches[useISO6093NR1Format]:
		x = ISO6093NR1
	case s.Switches[useISO6093NR2Format]:
		x = ISO6093NR2
	case s.Switches[useISO6093NR3Format]:
		x = ISO6093NR3
	}

	return x
}

func (s *testENDECScenario) prepareBase() (x int) {
	switch {
	case s.Switches[useBaseBadLower]:
		x = minBase - 1
	case s.Switches[useBaseMin]:
		x = minBase
	case s.Switches[useBaseOdd]:
		x = baseOdd
	case s.Switches[useBaseEight]:
		x = base8
	case s.Switches[useBaseTen]:
		x = base10
	case s.Switches[useBaseEven]:
		x = baseEven
	case s.Switches[useBaseMax]:
		x = maxBase
	case s.Switches[useBaseBadHigher]:
		x = minBase - 1
	}

	return x
}

func (s *testENDECScenario) prepareNormalization() (n int) {
	switch {
	case s.Switches[useNormalizeNormal]:
		n = NormalizeNormal
	case s.Switches[useNormalizeFullRound]:
		n = NormalizeFullRound
	case s.Switches[useNormalizeNone]:
		n = NormalizeNone
	case s.Switches[useBadNormalization]:
		n = normalizeBadValue
	default:
		n = NormalizeNormal
	}

	return n
}

func (s *testENDECScenario) preparePrecision() (p int) {
	switch {
	case s.Switches[usePrecisionNormal]:
		p = precisionNormalValue
	case s.Switches[usePrecisionBig]:
		p = precisionBigValue
	case s.Switches[usePrecisionNegative]:
		p = precisionNegativeValue
	case s.Switches[usePrecisionZero]:
		p = precisionZeroValue
	default:
		p = precisionNormalValue
	}

	return p
}

//nolint:gocyclo,gocognit,wsl
func (s *testENDECScenario) prepareToBaseResult() (result string,
	resultErr bool) {
	if s.Switches[useFloatNone] {
		return "", true
	}

	switch {
	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionZero]:
		result = float64ZeroPercisionPositiveResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionZero]:
		result = float64ZeroPercisionNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionZero]:
		result = float64ZeroPercisionNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionNegative]:
		result = float64NegativePercisionPositiveResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionNegative]:
		result = float64NegativePercisionNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionNegative]:
		result = float64NegativePercisionNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionBig]:
		result = float64BigPercisionPositiveResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionBig]:
		result = float64BigPercisionNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal] &&
		s.Switches[usePrecisionBig]:
		result = float64BigPercisionNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseMin] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MinBaseNormalResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseMin] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MinBaseNormalNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseMin] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MinBaseNormalNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseOdd] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64OddBaseNormalResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseOdd] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64OddBaseNormalNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseOdd] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64OddBaseNormalNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64EvenBaseNormalResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64EvenBaseNormalNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64EvenBaseNormalNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseMax] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MaxBaseNormalResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseMax] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MaxBaseNormalNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseMax] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MaxBaseNormalNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseBadLower] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = ""
		resultErr = true

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseBadLower] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = ""
		resultErr = true

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseBadLower] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = ""
		resultErr = true

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseBadHigher] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = ""
		resultErr = true

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseBadHigher] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = ""
		resultErr = true

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseBadHigher] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = ""
		resultErr = true

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEight] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64Base8NormalResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseTen] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64Base10NormalResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeFullRound]:
		result = float64MinBaseFullRoundResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeFullRound]:
		result = float64MinBaseFullRoundNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeFullRound]:
		result = float64MinBaseFullRoundNegativeExponentResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNone]:
		result = float64MinBaseNoneResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNone]:
		result = float64MinBaseNoneNegativeResult

	case s.Switches[useNegativeExponentFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNone]:
		result = float64MinBaseNoneNegativeExponentResult

	case s.Switches[useZeroFloat64] &&
		s.Switches[useBaseEven] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalizeNormal]:
		result = float64MinBaseNoneZeroResult
	}

	return result, resultErr
}

//nolint:gocyclo,gocognit,wsl
func (s *testENDECScenario) prepareStringResult() string {
	switch {
	case s.Switches[usePositiveFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float32NormalPositiveResult

	case s.Switches[useZeroFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float32NormalZeroResult

	case s.Switches[useNegativeFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float32NormalNegativeResult

	case s.Switches[useNegativeExponentFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float32NormalNegativeExponentResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useZeroAccuracy] &&
		s.Switches[useNormalFormat]:
		return float32NormalPositiveZeroAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useNegativeAccuracy] &&
		s.Switches[useNormalFormat]:
		return float32NormalPositiveNegativeAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useScientificFormat]:
		return float32ScientificPositiveResult

	case s.Switches[useZeroFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useScientificFormat]:
		return float32ScientificZeroResult

	case s.Switches[useNegativeFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useScientificFormat]:
		return float32ScientificNegativeResult

	case s.Switches[useNegativeExponentFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useScientificFormat]:
		return float32ScientificNegativeExponentResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useZeroAccuracy] &&
		s.Switches[useScientificFormat]:
		return float32ScientificPositiveZeroAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useNegativeAccuracy] &&
		s.Switches[useScientificFormat]:
		return float32ScientificPositiveNegativeAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR1Format]:
		return float32ISO6093NR1PositiveResult

	case s.Switches[useZeroFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR1Format]:
		return float32ISO6093NR1ZeroResult

	case s.Switches[useNegativeFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR1Format]:
		return float32ISO6093NR1NegativeResult

	case s.Switches[useNegativeExponentFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR1Format]:
		return float32ISO6093NR1NegativeExponentResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useZeroAccuracy] &&
		s.Switches[useISO6093NR1Format]:
		return float32ISO6093NR1PositiveZeroAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useNegativeAccuracy] &&
		s.Switches[useISO6093NR1Format]:
		return float32ISO6093NR1PositiveNegativeAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR2Format]:
		return float32ISO6093NR2PositiveResult

	case s.Switches[useZeroFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR2Format]:
		return float32ISO6093NR2ZeroResult

	case s.Switches[useNegativeFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR2Format]:
		return float32ISO6093NR2NegativeResult

	case s.Switches[useNegativeExponentFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR2Format]:
		return float32ISO6093NR2NegativeExponentResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useZeroAccuracy] &&
		s.Switches[useISO6093NR2Format]:
		return float32ISO6093NR2PositiveZeroAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useNegativeAccuracy] &&
		s.Switches[useISO6093NR2Format]:
		return float32ISO6093NR2PositiveNegativeAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR3Format]:
		return float32ISO6093NR3PositiveResult

	case s.Switches[useZeroFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR3Format]:
		return float32ISO6093NR3ZeroResult

	case s.Switches[useNegativeFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR3Format]:
		return float32ISO6093NR3NegativeResult

	case s.Switches[useNegativeExponentFloat32] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useISO6093NR3Format]:
		return float32ISO6093NR3NegativeExponentResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useZeroAccuracy] &&
		s.Switches[useISO6093NR3Format]:
		return float32ISO6093NR3PositiveZeroAccResult

	case s.Switches[usePositiveFloat32] &&
		s.Switches[useNegativeAccuracy] &&
		s.Switches[useISO6093NR3Format]:
		return float32ISO6093NR3PositiveNegativeAccResult

	case s.Switches[usePositiveFloat64] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float64NormalPositiveResult

	case s.Switches[useNegativeFloat64] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float64NormalNegativeResult

	case s.Switches[useZeroFloat64] &&
		s.Switches[usePositiveAccuracy] &&
		s.Switches[useNormalFormat]:
		return float64NormalZeroResult

	default:
		return ""
	}
}
