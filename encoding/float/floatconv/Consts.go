package floatconv

// Normalization constants are enumerations ID for adjusting the output to the
// desired presentation.
const (
	// NormalizeNormal is to normalize toward single digit round number.
	//
	// Example:
	//   1. XXXX.YYYYeZZZ --> X.XXXYYYYe(ZZZ+3)
	NormalizeNormal = 0

	// NormalizeRound is to normalize toward full round numbers.
	//
	// Example:
	//   1. XXXX.YYYYeZZZ --> XXXXYYYY.0e(ZZZ-4)
	NormalizeRound = 1

	// NormalizeNone is to not apply any normalization.
	//
	// Example:
	//   1. XXXX.YYYYeZZZ --> XXXX.YYYYeZZZ
	NormalizeNone = 2
)
