package floatconv

// internal use values
const (
	bit64 = 64

	defaultPrecisionLimit = 100

	base2  = 2
	base8  = 8
	base10 = 10
	base16 = 16
	base36 = 36

	minBase = base2
	maxBase = base36

	convSeekSign         = 0
	convSeekRound        = 1
	convSeekFraction     = 2
	convSeekExponentSign = 3
	convSeekExponent     = 4
)
