package floatconv

import (
	"testing"
)

func TestParseISO6093(t *testing.T) {
	scenarios := testConverterScenarios()

	for i, s := range scenarios {
		if s.testType != testParseISO6093 {
			continue
		}

		// prepare
		c := &Converter{}

		s.register(t)

		// test
		str, expect, expectError := s.generateISO6093()
		err := c.ParseISO6093(str)

		// assert
		s.assertError(expectError, err)
		s.assertConverter(expect, c)
		s.conclude(i, map[string]interface{}{
			"error":        err,
			"sign":         c.sign,
			"usable":       c.usable,
			"exponentSign": c.exponentSign,
			"round":        c.round,
			"fraction":     c.fraction,
			"exponent":     c.exponent,
		})
	}
}
