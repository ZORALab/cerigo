package floatconv

import (
	"testing"
)

func TestParseText(t *testing.T) {
	txt := &Text{}
	txt.correction()
	txt.Base = "38"

	c := &Converter{}
	err := c.ParseText(txt)

	t.Logf("test invalid base values")

	if err == nil {
		t.Errorf("missing error for invalid base conversion")
	}
}
