package floatconv

import (
	"fmt"
	"testing"
)

// testType
const (
	testParseISO6093 = "testParseISO6093"
)

// switches
const (
	useNegativeSign   = "useNegativeSign"
	usePositiveSign   = "usePositiveSign"
	useNoExponentSign = "useNoExponentSign"

	useGoodRound = "useGoodRound"
	useBadRound  = "useBadRound"

	useGoodFraction = "useGoodFraction"
	useBadFraction  = "useBadFraction"

	useCapitalExponent = "useCapitalExponent"
	useNoExponent      = "useNoExponent"
	useBadExponent     = "useBadExponent"
	useExactExponent   = "useExactExponent"
	useLowerExponent   = "useLowerExponent"

	useNegativeExponentSign = "useNegativeExponentSign"
	useGoodExponent         = "useGoodExponent"
)

type testConverterScenario struct {
	uid         int
	testType    string
	description string
	switches    map[string]bool

	// internal use
	controller *testing.T
	logs       []string
	errors     []string
}

func (s *testConverterScenario) register(t *testing.T) {
	s.controller = t
	s.logs = []string{}
	s.errors = []string{}
}

func (s *testConverterScenario) logf(format string, args ...interface{}) {
	x := fmt.Sprintf(format, args...)
	s.logs = append(s.logs, x)
}

func (s *testConverterScenario) errorf(format string, args ...interface{}) {
	x := fmt.Sprintf(format, args...)
	s.errors = append(s.errors, x)
}

func (s *testConverterScenario) conclude(id int, data map[string]interface{}) {
	if s.controller == nil {
		return
	}

	// check uid
	if id+1 != s.uid {
		s.errorf("uid mistmatched: index=%v uid=%v", id+1, s.uid)
	}

	// process scenario data
	s.logf("\n")
	s.logf("CASE\n%v\n\n", s.uid)
	s.logf("TEST TYPE\n%v\n\n", s.testType)
	s.logf("DESCRIPTION%v\n\n", s.description)
	s.logf("SWITCHES\n")

	for k, v := range s.switches {
		s.logf("%-20v = %v\n", k, v)
	}

	s.logf("\n")

	s.logf("GOT\n")

	for k, v := range data {
		s.logf("%-20v = %v\n", k, v)
	}

	s.logf("\n")

	// print errors
	out := ""
	for _, x := range s.errors {
		out += x + "\n"
	}

	if out != "" {
		s.controller.Errorf("\nTESTFAIL\n%v\n", out)
	}

	// print logs
	out = ""
	for _, x := range s.logs {
		out += x
	}

	s.controller.Logf("\n%v\n", out)
}

func (s *testConverterScenario) assertError(expect bool, err error) {
	switch {
	case expect && err == nil:
		s.errorf("missing error: %v", err)
	case !expect && err != nil:
		s.errorf("bogus error: %v", err)
	}
}

func (s *testConverterScenario) generateISO6093() (str string,
	expect *Converter,
	expectError bool) {
	expect = &Converter{usable: true}

	str, expectError = s.genSign(str, expect, expectError)
	str, expectError = s.genRound(str, expect, expectError)
	str, expectError = s.genFraction(str, expect, expectError)
	str, expectError = s.genExponent(str, expectError)
	str, expectError = s.genExponentSign(str, expect, expectError)
	str, expectError = s.genExponentValue(str, expect, expectError)

	if expectError {
		expect.usable = false
	}

	return str, expect, expectError
}

func (s *testConverterScenario) genExponentValue(str string,
	c *Converter,
	err bool) (string, bool) {
	switch {
	case s.switches[useNoExponent]:
		return str, err
	case s.switches[useGoodExponent]:
		str += "12"
		c.exponent = 12
	case s.switches[useExactExponent]:
		str += "2"
	case s.switches[useLowerExponent]:
		str += "1"
	default:
		str += "1"
		c.exponent = 1
	}

	switch {
	case s.switches[useExactExponent]:
		c.exponent = 0
	case s.switches[useLowerExponent]:
		c.exponent = 0
	case s.switches[useNegativeExponentSign]:
		c.exponent = 10
	case s.switches[useBadExponent]:
		c.exponent = 0
	}

	return str, err
}

func (s *testConverterScenario) genExponentSign(str string,
	c *Converter,
	err bool) (string, bool) {
	switch {
	case s.switches[useNoExponent]:
	case s.switches[useNoExponentSign]:
	case s.switches[useExactExponent]:
		str += "-"
		c.exponentSign = false
	case s.switches[useLowerExponent]:
		str += "-"
		c.exponentSign = false
	case s.switches[useNegativeExponentSign]:
		str += "-"

		c.exponentSign = true
	default:
		str += "+"
	}

	return str, err
}

func (s *testConverterScenario) genExponent(str string,
	err bool) (string, bool) {
	switch {
	case s.switches[useNoExponent]:
	case s.switches[useCapitalExponent]:
		str += "E"
	case s.switches[useBadExponent]:
		str += "δ"
		err = true
	default:
		str += "e"
	}

	return str, err
}

func (s *testConverterScenario) genFraction(str string,
	c *Converter,
	err bool) (string, bool) {
	switch {
	case s.switches[useGoodFraction]:
		str += ".375"
		c.fraction = 0.375
	case s.switches[useBadFraction]:
		str += ".op"
		c.fraction = 0
		err = true
	default:
		str += ".0"
		c.fraction = 0
	}

	switch {
	case s.switches[useLowerExponent]:
		c.fraction = 0.2375
	case s.switches[useNegativeExponentSign]:
		c.fraction = 0.12375
	case s.switches[useBadRound]:
		c.fraction = 0
	case s.switches[useBadExponent]:
		c.fraction = 0
	}

	return str, err
}

func (s *testConverterScenario) genRound(str string,
	c *Converter,
	err bool) (string, bool) {
	switch {
	case s.switches[useGoodRound]:
		str += "12"
		c.round = 12
	case s.switches[useBadRound]:
		str += "op"
		err = true
		c.round = 0
	default:
		str += "0"
		c.round = 0
	}

	switch {
	case s.switches[useLowerExponent]:
		c.round = 1
	case s.switches[useNegativeExponentSign]:
		c.round = 0
	default:
	}

	return str, err
}

func (s *testConverterScenario) genSign(str string,
	c *Converter,
	err bool) (string, bool) {
	switch {
	case s.switches[useNegativeSign]:
		c.sign = true
		str = "-"
	case s.switches[usePositiveSign]:
		str = "+"
	default:
	}

	return str, err
}

func (s *testConverterScenario) assertConverter(expect, c *Converter) {
	switch {
	case expect != nil && c == nil:
		fallthrough
	case expect == nil && c != nil:
		s.errorf("unexpected nil: %v|%v", expect == nil, c == nil)
		return
	}

	if expect.usable != c.usable {
		s.errorf("incorrect usable: %v|%v", expect.usable, c.usable)
	}

	if expect.sign != c.sign {
		s.errorf("incorrect sign: %v|%v", expect.sign, c.sign)
	}

	if expect.exponentSign != c.exponentSign {
		s.errorf("incorrect exp sign: %v|%v",
			expect.exponentSign,
			c.exponentSign)
	}

	if expect.round != c.round {
		s.errorf("incorrect round: %v|%v",
			expect.round,
			c.round)
	}

	if expect.fraction != c.fraction {
		s.errorf("incorrect fraction: %v|%v",
			expect.fraction,
			c.fraction)
	}

	if expect.exponent != c.exponent {
		s.errorf("incorrect exponent: %v|%v",
			expect.exponent,
			c.exponent)
	}
}
