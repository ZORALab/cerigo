package floatconv

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

// Converter is the object structure meant for performing base conversion.
//
// It is safe to create using &struct{} mechanism.
type Converter struct {
	usable       bool
	sign         bool
	exponentSign bool
	round        uint64
	fraction     float64
	exponent     uint64
}

// ParseISO6093 is to accept ISO6093 compliant string format as input.
//
// If the given string is invalid, this function returns an error and the
// converter is not usable, waiting for the next parsing operation.
func (c *Converter) ParseISO6093(number string) error {
	t := &Text{Base: "10"}
	seek := convSeekSign

	if number == "0" {
		t.correction()
		t.Base = "10"

		goto parseTextObject
	}

	for _, char := range number {
		switch {
		case seek == convSeekSign:
			seek = convSeekRound

			if char == '-' {
				t.Sign = "-"
				continue
			} else if char == '+' {
				continue
			}

			fallthrough
		case seek == convSeekRound:
			if char == '.' {
				seek = convSeekFraction
				continue
			}

			t.Round += string(char)
		case seek == convSeekFraction:
			if char == 'e' || char == 'E' {
				t.Base = "10"
				seek = convSeekExponentSign

				continue
			}

			t.Fraction += string(char)
		case seek == convSeekExponentSign:
			t.ExponentSign = "+"
			seek = convSeekExponent

			if char == '-' {
				t.ExponentSign = "-"
				continue
			} else if char == '+' {
				continue
			}

			fallthrough
		case seek == convSeekExponent:
			t.Exponent += string(char)
		}
	}

parseTextObject:
	return c.ParseText(t)
}

// ParseText is to accept a pre-filled *Text data structure as input.
//
// If the given Text data structure is invalid, ParseText(...) will return an
// error and the converter is not usable, waiting for the next parsing
// operation.
func (c *Converter) ParseText(t *Text) (err error) {
	var lmr, δ uint64

	// reset converter status
	c.usable = false

	// apply text correction
	t.correction()

	// base number
	base := 0

	for i := minBase; i < maxBase; i++ {
		x := strconv.FormatInt(int64(i), maxBase)
		y := strconv.FormatInt(int64(i), base10)

		if t.Base == x || t.Base == y {
			base = i
			break
		}
	}

	if base == 0 {
		return fmt.Errorf(ErrorBaseOutOfRange)
	}

	// sign
	if t.Sign == "-" {
		c.sign = true
	}

	// exponent sign
	if t.ExponentSign == "-" {
		c.exponentSign = true
	}

	// exponent
	c.exponent, _ = strconv.ParseUint(t.Exponent, base10, bit64)

	// correct round number's roundness
	if t.ExponentSign == "-" && t.Round != "0" {
		lmr = uint64(len(t.Round))

		switch {
		case lmr < c.exponent: // XX.YYe-ZZ --> 0.XXYYe(-ZZ+2)
			c.exponent -= lmr
			t.Fraction = t.Round + t.Fraction
			t.Round = "0"
		case lmr == c.exponent: // XXX.YYe-3 --> 0.XXXYYe0
			t.Fraction = t.Round + t.Fraction
			c.exponent = 0
			c.exponentSign = false
			t.Round = "0"
		case lmr > c.exponent: // XXXXX.YYe-2 --> XXX.XXYYe0
			δ = lmr - c.exponent
			t.Fraction = t.Round[lmr-δ:] + t.Fraction
			t.Round = t.Round[0:δ]
			c.exponent = 0
			c.exponentSign = false
		}
	}

	c.round, err = strconv.ParseUint(t.Round, base, bit64)
	if err != nil {
		return err
	}

	f, err := strconv.ParseUint(t.Fraction, base, bit64)
	if err != nil {
		return err
	}

	fraction := strconv.FormatUint(f, base10)
	c.fraction, _ = strconv.ParseFloat("0."+fraction, bit64)

	c.usable = true

	return nil
}

func (c *Converter) ConvertBase(base int,
	normalization int,
	precisionLimit int) (t *Text, err error) {
	var δexponent int

	// check conversion is usable
	if !c.usable {
		return nil, fmt.Errorf(ErrorConvertBaseNotUsable)
	}

	// check base number is valid
	if base < 2 || base > 36 {
		return nil, fmt.Errorf(ErrorBaseOutOfRange)
	}

	t = &Text{}
	t.correction()

	// check inputs
	if c.round == 0 && c.fraction == 0 {
		return t, nil
	}

	// signs
	if c.sign {
		t.Sign = "-"
	}

	// mantissa
	mr := c.convRound(base)
	mf := c.convFraction(base, precisionLimit)

	switch normalization {
	case NormalizeNone:
		t.Round, t.Fraction, δexponent = c.normalizeNone(mr, mf)
	case NormalizeRound:
		t.Round, t.Fraction, δexponent = c.normalizeRound(mr, mf)
	case NormalizeNormal:
		fallthrough
	default:
		t.Round, t.Fraction, δexponent = c.normalizeNormal(mr, mf)
	}

	// exponent
	t.ExponentSign, t.Exponent = c.convExponent(base, δexponent)

	// base
	t.Base = strconv.FormatInt(int64(base), base10)

	// apply correction
	t.correction()

	return t, nil
}

func (c *Converter) convRound(base int) string {
	if c.round == 0 {
		return ""
	}

	return strconv.FormatUint(c.round, base)
}

func (c *Converter) convFraction(base int, precisionLimit int) string {
	if precisionLimit <= 0 {
		precisionLimit = defaultPrecisionLimit
	}

	if c.fraction == 0 {
		return ""
	}

	m := ""
	q, r, b := float64(0), c.fraction, float64(base)

	for {
		if r == 0 || len(m) >= precisionLimit {
			break
		}

		r *= b
		q = math.Floor(r)
		r -= q
		m += strconv.FormatUint(uint64(q), base)
	}

	return m
}

func (c *Converter) convExponent(base int,
	δexponent int) (sign string, value string) {
	var exponent, logConstant float64
	var raw int64

	// skip division if exponent is 0
	if c.exponent == 0 {
		goto adjust
	}

	// convert exponent to natural value
	if c.exponentSign {
		exponent = float64(-c.exponent)
	} else {
		exponent = float64(c.exponent)
	}

	// log constant lookup table for base conversion
	switch {
	case base == base2:
		logConstant = 3.321928094887362 //  log2(10)
	case base == base8:
		logConstant = 1.1073093649624541 // log2(10) / 3
	case base == base10:
		goto adjust
	case base == base16:
		logConstant = 0.8304820237218405 // log2(10) / 4
	default:
		logConstant = math.Log2(10) / math.Log2(float64(base))
	}

	exponent *= logConstant

adjust:
	exponent += float64(δexponent)

	raw = int64(math.Round(exponent))
	value = strconv.FormatInt(raw, 10)

	switch {
	case raw < 0:
		sign = "-"
		value = value[1:]
	default:
		sign = "+"
	}

	return sign, value
}

func (c *Converter) normalizeNormal(mr string,
	mf string) (round string, fraction string, δexponent int) {
	m := ""
	lmr := len(mr)
	lmf := len(mf)

	switch {
	case lmr == 1 && lmf == 0: // X.0
		m = mr
		m += ".0"
	case lmr != 0 && lmf == 0: // XXXX.0 --> X.XXX*B^N
		m = mr[0:1]
		m += "."
		m += mr[1:]
		δexponent = lmr - 1
	case lmr == 0 && lmf != 0: // 0.XXXX --> X.XXX*B^-N
		m = mf[0:1]
		m += "."
		m += mf[1:]
		δexponent = 1 - lmr
	default: // XXXX.XXXX --> X.XXXXXXX*B^N
		m = mr[0:1]
		m += "."
		m += mr[1:]
		m += mf
		δexponent = lmr - 1
	}

	mparts := strings.Split(m, ".")

	round, fraction = mparts[0], mparts[1]
	if fraction != "0" {
		fraction = strings.TrimRight(fraction, "0")
	}

	return round, fraction, δexponent
}

func (c *Converter) normalizeRound(mr string,
	mf string) (round string, fraction string, δexponent int) {
	round = mr
	lmf := len(mf)

	if lmf != 0 {
		round += mf
		δexponent -= lmf
	}

	round = strings.TrimLeft(round, "0")

	return round, "0", δexponent
}

func (c *Converter) normalizeNone(mr string,
	mf string) (round string, fraction string, δexponent int) {
	return mr, mf, 0
}
