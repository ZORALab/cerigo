package float

import (
	"fmt"
	"math/big"
	"strings"

	"gitlab.com/zoralab/cerigo/encoding/float/floatconv"
)

// ENDEC is the REAL number format encoder and decoder (ENDEC).
//
// It holds one float value at a time for format translation. To change into
// different float number, simply parse the float values again.
//
// This structure contains unexported configuration fields so use
// float.New() function to create one instead of the conventional &structure{}
// way.
type ENDEC struct {
	// original data
	fType uint
	f64   float64
	f32   float32
	fb    *big.Float
}

// New creates the float.ENDEC for encoding/decoding float data into other
// format.
func New() *ENDEC {
	return &ENDEC{
		fType: floatNone,
	}
}

func (e *ENDEC) reset() {
	e.fType = floatNone
	e.f64 = 0
	e.f32 = 0
	e.fb = nil
}

func (e *ENDEC) getFloat() *big.Float {
	var x *big.Float

	switch e.fType {
	case floatBig:
		x = e.fb
	case float32bit:
		x = big.NewFloat(float64(e.f32))
	case float64bit:
		x = big.NewFloat(e.f64)
	}

	return x
}

// ParseFloat64 accepts float64 object as the float data.
//
// The input is:
//   1. x       - float64 number
func (e *ENDEC) ParseFloat64(x float64) {
	e.reset()
	e.fType = float64bit
	e.f64 = x
}

// ParseFloat32 accepts float32 object as the float data.
//
// The input is:
//   1. x       - float32 number
func (e *ENDEC) ParseFloat32(x float32) {
	e.reset()
	e.fType = float32bit
	e.f32 = x
}

// ParseBigFloat accepts math/big/Float object pointer as the float data.
//
// The input is:
//   1. x       - math/big.Float object pointer (e.g. &var)
func (e *ENDEC) ParseBigFloat(x *big.Float) {
	e.reset()
	e.fType = floatBig
	e.fb = x
}

// ToBase changes the float REAL value into other base number system.
//
// It produces float.Text data structure output holding various segments of
// REAL number. Due to the support between base2 to base36 (2...z), the
// output is strictly limited to string data type and [Scientific] format. To
// view output as a whole, simply call the Text.String().
//
// The inputs are:
//   1. base            - base number system from 2 to 36.
//   2. normalization   - Normalization choice. The default is
//                        [NormalizeNormal].
//                        See "Normalization Enumerations" for more info.
//   3. accuracy        - the REAL value's accuracy before base change.
//                        The output preserves the REAL value as close as
//                        possible.
//   4. precision       - the fraction precision limit after conversion.
//                        The default is 100.
//
// The outputs are:
//   1. float.*Text     - the float.Text data structure pointer holding the
//                        string parts values.
//   2. error           - any error occurred during conversion. If error
//                        exists, the float.*Text output will be nil.
func (e *ENDEC) ToBase(base int,
	normalization int,
	accuracy int,
	precision int) (t *floatconv.Text, err error) {
	if base < minBase || base > maxBase {
		return nil, fmt.Errorf("base must be: 2 ≤ base ≤ 36")
	}

	x := e.getFloat()
	if x == nil {
		return nil, fmt.Errorf("missing float number")
	}

	c := &floatconv.Converter{}

	_ = c.ParseISO6093(x.Text('e', accuracy))

	return c.ConvertBase(base, normalization, precision)
}

// ToString changes the float REAL value into the selected format.
//
// If the format is unavailable or incompatible, the return value is empty
// ("").
//
// The input are:
//   1. format    - the format enumerated value. There is no default.
//                  See "Format Type Enumerations" for more info.
//   2. accuracy  - the precision degree for the output.
//
// The output are:
//   1. format    - the formatted presentation in string.
func (e *ENDEC) ToString(format int, accuracy int) string {
	x := e.getFloat()
	if x == nil {
		return ""
	}

	var t string

	switch format {
	case Normal:
		t = x.Text('g', accuracy)
	case Scientific:
		t = x.Text('e', accuracy)
		t = strings.Replace(t, "e", "*10^(", -1)
		t += ")"
	case ISO6093NR1:
		t = x.Text('f', 0)
	case ISO6093NR2:
		t = x.Text('f', accuracy)
	case ISO6093NR3:
		t = x.Text('e', accuracy)
	}

	return t
}
