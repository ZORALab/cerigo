package strhelper

import (
	"testing"
)

func TestToUnix(t *testing.T) {
	scenarios := testStrHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testToUNIX {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		styler := &Styler{}
		sample, expect := s.prepareString()

		// test
		out := styler.ToUnix(sample)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStrings("Subject", out, "Expectee", expect)
		s.log(th, map[string]interface{}{
			"sample": sample,
			"expect": expect,
			"got":    out,
		})
		th.Conclude()
	}
}
