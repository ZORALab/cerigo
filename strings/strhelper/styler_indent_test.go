package strhelper

import (
	"testing"
)

func TestIndent(t *testing.T) {
	scenarios := testStrHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testIndent {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		styler := &Styler{}
		sample, indent, size, expect := s.prepareIndentSample()

		// test
		out := styler.Indent(sample, indent, size)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStringSlices("subject", &out, "expect", &expect)
		s.log(th, map[string]interface{}{
			"input sample": sample,
			"expect":       expect,
			"input size":   size,
			"input indent": indent,
			"got":          out,
		})
		th.Conclude()
	}
}
