package strhelper

func testStrHelperScenarios() []testStrHelperScenario {
	return []testStrHelperScenario{
		{
			UID:      1,
			TestType: testToUNIX,
			Description: `
ToUNIX should work properly when given a proper string with CRLF new lines.
`,
			Switches: map[string]bool{
				useCRLF:  true,
				expectLF: true,
			},
		}, {
			UID:      2,
			TestType: testToUNIX,
			Description: `
ToUNIX should work properly when given a proper string with LF new lines.
`,
			Switches: map[string]bool{
				useLF:    true,
				expectLF: true,
			},
		}, {
			UID:      3,
			TestType: testToUNIX,
			Description: `
ToUNIX should work properly when given a proper string with CR new lines.
`,
			Switches: map[string]bool{
				useCR:    true,
				expectLF: true,
			},
		}, {
			UID:      4,
			TestType: testToWindows,
			Description: `
ToWindows should work properly when given a proper string with CRLF new lines.
`,
			Switches: map[string]bool{
				useCRLF:    true,
				expectCRLF: true,
			},
		}, {
			UID:      5,
			TestType: testToWindows,
			Description: `
ToWindows should work properly when given a proper string with LF new lines.
`,
			Switches: map[string]bool{
				useLF:      true,
				expectCRLF: true,
			},
		}, {
			UID:      6,
			TestType: testToWindows,
			Description: `
ToWindows should work properly when given a proper string with CR new lines.
`,
			Switches: map[string]bool{
				useCR:      true,
				expectCRLF: true,
			},
		}, {
			UID:      7,
			TestType: testWordWrap,
			Description: `
WordWrap should work properly when given a proper sample, and proper limit.
`,
			Switches: map[string]bool{},
		}, {
			UID:      8,
			TestType: testWordWrap,
			Description: `
WordWrap should work properly when given a bad sample, and proper limit.
`,
			Switches: map[string]bool{
				useBadSample: true,
			},
		}, {
			UID:      9,
			TestType: testWordWrap,
			Description: `
WordWrap should work properly when given a proper sample, and bad limit.
`,
			Switches: map[string]bool{
				useBadLimit: true,
			},
		}, {
			UID:      10,
			TestType: testContentWrap,
			Description: `
ContentWrap should work properly when given a proper sample, proper limit,
and LF eol
`,
			Switches: map[string]bool{
				useLF:    true,
				expectLF: true,
			},
		}, {
			UID:      11,
			TestType: testContentWrap,
			Description: `
ContentWrap should work properly when given a proper sample, proper limit,
and CRLF eol
`,
			Switches: map[string]bool{
				useCRLF:    true,
				expectCRLF: true,
			},
		}, {
			UID:      12,
			TestType: testContentWrap,
			Description: `
ContentWrap should work properly when given a proper sample, proper limit,
and CR eol
`,
			Switches: map[string]bool{
				useCR:    true,
				expectCR: true,
			},
		}, {
			UID:      13,
			TestType: testContentWrap,
			Description: `
ContentWrap should work properly when given a bad sample, proper limit,
and LF eol
`,
			Switches: map[string]bool{
				useBadSample: true,
				useLF:        true,
				expectLF:     true,
			},
		}, {
			UID:      14,
			TestType: testContentWrap,
			Description: `
ContentWrap should work properly when given a proper sample, bad limit,
and LF eol
`,
			Switches: map[string]bool{
				useBadLimit: true,
				useLF:       true,
				expectLF:    true,
			},
		}, {
			UID:      15,
			TestType: testContentWrap,
			Description: `
ContentWrap should work properly when given a proper sample, bad limit,
and weird EOL
`,
			Switches: map[string]bool{
				useWeirdEOL: true,
				useLF:       true,
				expectLF:    true,
			},
		}, {
			UID:      16,
			TestType: testIndent,
			Description: `
Indent should work properly when given a proper sample, proper space indent
character, and proper indent size
`,
			Switches: map[string]bool{},
		}, {
			UID:      17,
			TestType: testIndent,
			Description: `
Indent should work properly when given a proper sample, proper tab indent
character, and proper indent size
`,
			Switches: map[string]bool{
				useTabIndent: true,
			},
		}, {
			UID:      18,
			TestType: testIndent,
			Description: `
Indent should work properly when given a proper sample, bad indent
character, and proper indent size
`,
			Switches: map[string]bool{
				useBadIndent: true,
			},
		}, {
			UID:      19,
			TestType: testIndent,
			Description: `
Indent should work properly when given a proper sample, proper indent
character, and bad indent size
`,
			Switches: map[string]bool{
				useBadIndentSize: true,
			},
		}, {
			UID:      20,
			TestType: testIndent,
			Description: `
Indent should work properly when given a bad sample, proper indent
character, and proper indent size
`,
			Switches: map[string]bool{
				useBadParagraph: true,
			},
		},
	}
}
