package strhelper

import (
	"testing"
)

func TestContentWrap(t *testing.T) {
	scenarios := testStrHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testContentWrap {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		styler := &Styler{}
		sample, limit, eol, expect := s.prepareParagraphsSample()

		// test
		out := styler.ContentWrap(sample, limit, eol)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStringSlices("subject", &out, "expect", &expect)
		s.log(th, map[string]interface{}{
			"input sample": sample,
			"expect":       expect,
			"input limit":  limit,
			"input eol":    eol,
			"got":          out,
		})
		th.Conclude()
	}
}
