package cmdchain

import (
	"testing"
)

func TestSetData(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testSetData {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		data := s.prepareData(c)

		// test
		c.SetData(data)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertData(th, c, data)
		s.log(th, map[string]interface{}{
			"input data": data,
			"got":        c.data,
		})
		th.Conclude()
	}
}
