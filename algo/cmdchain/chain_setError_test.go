package cmdchain

import (
	"testing"
)

func TestSetError(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testSetError {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		expect := s.prepareErrors(c)

		// test
		c.SetError(expect)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertError(th, c.err, expect)
		s.log(th, map[string]interface{}{
			"input error": expect,
			"got":         c.err,
		})
		th.Conclude()
	}
}
