package cmdchain

import (
	"fmt"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

// data
const (
	chainedCMD      = "chained CMD"
	lv1InterfaceCMD = "lv1 Interface CMD"
	lv2InterfaceCMD = "lv2 Interface CMD"
	lv3InterfaceCMD = "lv3 Interface CMD"
	lv4InterfaceCMD = "lv4 Interface CMD"
	lv1NativeCMD    = "lv1 Native CMD"
	lv2NativeCMD    = "lv2 Native CMD"
	lv3NativeCMD    = "lv3 Native CMD"
	lv4NativeCMD    = "lv4 Native CMD"

	badInterceptLabel  = -2
	badTimeout         = 1 * NanoSecond
	goodInterceptLabel = 2
	goodTimeout        = 200 * MilliSecond
	initialTimeout     = 12345 * NanoSecond

	testErrorNotAssertable = "test data is not assertable"
)

// test type
const (
	testCMDExists       = "testCMDExists"
	testDelete          = "testDelete"
	testGetData         = "testGetData"
	testGetError        = "testGetError"
	testGetTimeout      = "testGetTimeout"
	testIntercept       = "testIntercept"
	testInterrupts      = "testInterrupts"
	testRegister        = "testRegister"
	testRun             = "testRun"
	testSetError        = "testSetError"
	testSetErrorHandler = "testSetErrorHandler"
	testSetData         = "testSetData"
	testSetTimeout      = "testSetTimeout"
)

// switches
const (
	missingError            = "missingError"
	missingErrorHandlingCMD = "missingErrorHandlingCMD"
	missingInterceptCMD     = "missingInterceptCMD"
	missingInterruptCMD     = "missingInterruptCMD"
	presetData              = "presetData"
	prepareError            = "prepareError"
	prepareIntercept        = "prepareIntercept"
	prepareInterrupt        = "prepareInterrupt"
	prepareNoData           = "prepareNoData"
	presetTimeout           = "presetTimeout"
	useBadData              = "useBadData"
	useBadInterceptLabel    = "useBadInterceptLabel"
	useBadTimeout           = "useBadTimeout"
	useInterfaceFunction    = "useInterfaceFunction"
	useZeroTimeout          = "useZeroTimeout"
)

type testChainScenario thelper.Scenario

func (s *testChainScenario) log(th *thelper.THelper,
	data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testChainScenario) prepareTestHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testChainScenario) assertChainRun(th *thelper.THelper,
	data *data,
	expect *data) {
	if expect.e != data.e {
		th.Errorf("bad error processing")
	}

	if expect.s != data.s {
		th.Errorf("bad data processing")
	}
}

//nolint:gocognit
func (s *testChainScenario) prepareChain(c *Chain) (cmd func(super *Chain),
	d *data,
	expect *data) {
	d = &data{}
	expect = &data{}

	errMessage := ""

	if s.Switches[prepareError] {
		if s.Switches[useInterfaceFunction] {
			c.errorHandler = d.lv1CMD
			errMessage = lv1InterfaceCMD
		} else {
			c.errorHandler = lv1CMD
			errMessage = lv1NativeCMD
		}
	}

	cmd = lv4CMD
	expect.s = lv4NativeCMD

	if s.Switches[useInterfaceFunction] {
		cmd = d.lv4CMD
		expect.s = lv4InterfaceCMD
	}

	c.data = d
	if s.Switches[useBadData] {
		c.data = *d
		expect.s = ""
		expect.e = testErrorNotAssertable
	}

	c.timeout = goodTimeout

	switch {
	case s.Switches[useBadTimeout]:
		c.timeout = badTimeout
		expect.e = ErrorTimeout
	case s.Switches[useZeroTimeout]:
		c.timeout = 0
	}

	if s.Switches[prepareIntercept] {
		if s.Switches[useInterfaceFunction] {
			c.list[goodInterceptLabel] = d.lv3CMD
			expect.s = lv3InterfaceCMD
		} else {
			c.list[goodInterceptLabel] = lv3CMD
			expect.s = lv3NativeCMD
		}

		c.next = goodInterceptLabel
	}

	if s.Switches[missingInterceptCMD] {
		delete(c.list, goodInterceptLabel)

		expect.e = ErrorMissingCommand
		expect.s = errMessage
	}

	if s.Switches[prepareInterrupt] {
		if s.Switches[useInterfaceFunction] {
			c.interrupts = append(c.interrupts, d.lv2CMD)
			expect.s = lv2InterfaceCMD
		} else {
			c.interrupts = append(c.interrupts, lv2CMD)
			expect.s = lv2NativeCMD
		}
	}

	return cmd, d, expect
}

func (s *testChainScenario) prepareCMDExists(c *Chain) (label int,
	expect bool) {
	label = goodInterceptLabel
	x := c.list[label]
	expect = (x != nil)

	if s.Switches[useBadInterceptLabel] {
		label = badInterceptLabel
		expect = false
	}

	return label, expect
}

func (s *testChainScenario) assertIntercept(th *thelper.THelper,
	c *Chain,
	expect bool) {
	if expect && c.next == 0 {
		th.Errorf("failed to intercept while expected")
	}

	if !expect && c.next != 0 {
		th.Errorf("unknown unexpected interception")

		if c.err == nil {
			th.Errorf("missing error for bad interception")
		}
	}
}

func (s *testChainScenario) prepareIntercept(c *Chain) (label int,
	expect bool) {
	label = goodInterceptLabel
	x := c.list[label]
	expect = (x != nil)
	c.next = 0

	if s.Switches[useBadInterceptLabel] {
		label = badInterceptLabel
		expect = false
	}

	return label, expect
}

func (s *testChainScenario) assertRegister(th *thelper.THelper,
	c *Chain,
	label int,
	err error,
	expect error) {
	if err == nil {
		if expect != nil {
			th.Errorf("missing expected error")
			return
		}

		x := c.list[label]

		if x == nil {
			th.Errorf("failed to register the command into it")
		}

		return
	}

	if expect == nil {
		th.Errorf("unexpected error object")
		return
	}

	if err.Error() != expect.Error() {
		th.Errorf("incorrect error message")
	}
}

func (s *testChainScenario) prepareRegister(c *Chain,
	inCMD func(super *Chain)) (label int,
	cmd func(super *Chain),
	expect error) {
	label = goodInterceptLabel
	cmd = inCMD
	x := c.list[label]

	if x != nil {
		expect = fmt.Errorf(ErrorExistingCommand)
	}

	if s.Switches[useBadInterceptLabel] {
		label = 0
		expect = fmt.Errorf(ErrorBadLabel)
	}

	if s.Switches[missingInterceptCMD] {
		cmd = nil
		expect = fmt.Errorf(ErrorMissingCommand)
	}

	return label, cmd, expect
}

func (s *testChainScenario) assertDelete(th *thelper.THelper,
	c *Chain,
	label int) {
	x := c.list[label]
	if x != nil {
		th.Errorf("failed to delete")
	}
}

func (s *testChainScenario) prepareDelete() (label int) {
	label = goodInterceptLabel
	if s.Switches[useBadInterceptLabel] {
		label = badInterceptLabel
	}

	return label
}

func (s *testChainScenario) assertInterrupts(th *thelper.THelper,
	c *Chain,
	cmd func(super *Chain)) {
	l := len(c.interrupts)

	if cmd == nil {
		if l == 1 && s.Switches[prepareInterrupt] {
			return
		}

		if l == 0 && !s.Switches[prepareInterrupt] {
			return
		}
	}

	if l == 0 && !s.Switches[prepareInterrupt] {
		th.Errorf("failed to register interrupts")
		return
	}

	if l == 1 && s.Switches[prepareInterrupt] {
		th.Errorf("failed to register interrupts")
	}
}

func (s *testChainScenario) prepareInterrupts(
	inCMD func(super *Chain)) (cmd func(super *Chain)) {
	cmd = inCMD
	if s.Switches[missingInterruptCMD] {
		cmd = nil
	}

	return cmd
}

func (s *testChainScenario) assertError(th *thelper.THelper,
	err error,
	expect error) {
	if err != expect {
		th.Errorf("unexpected error")
	}
}

func (s *testChainScenario) prepareErrors(c *Chain) (expect error) {
	expect = fmt.Errorf("just testing")

	if s.Switches[prepareError] {
		c.err = expect
		if s.TestType == testGetError && s.Switches[missingError] {
			c.err = nil
		}
	}

	if s.Switches[missingError] {
		expect = nil
	}

	return expect
}

func (s *testChainScenario) assertErrorHandler(th *thelper.THelper,
	c *Chain,
	cmd func(super *Chain)) {
	switch {
	case cmd == nil && c.errorHandler != nil:
		fallthrough
	case cmd != nil && c.errorHandler == nil:
		th.Errorf("failed to set handler")
	}
}

func (s *testChainScenario) prepareErrorHandler(
	inCMD func(super *Chain)) (cmd func(super *Chain)) {
	cmd = inCMD
	if s.Switches[missingErrorHandlingCMD] {
		cmd = nil
	}

	return cmd
}

func (s *testChainScenario) assertData(th *thelper.THelper,
	c *Chain,
	data interface{}) {
	if c.data != data {
		th.Errorf("incorrect data set")
	}
}

func (s *testChainScenario) prepareData(c *Chain) (data *int) {
	x := 123
	data = &x

	if s.Switches[presetData] {
		c.data = data
	}

	if s.Switches[prepareNoData] {
		data = nil
	}

	return data
}

func (s *testChainScenario) assertTimeout(th *thelper.THelper,
	c *Chain) {
	if s.Switches[presetTimeout] && c.timeout == initialTimeout {
		if s.TestType != testGetTimeout {
			th.Errorf("timeout is not set")
		}

		return
	}

	if s.Switches[useBadTimeout] {
		if c.timeout != badTimeout {
			th.Errorf("bad timeout is not set")
		}

		return
	}

	if c.timeout != goodTimeout {
		th.Errorf("timeout is not set correctly")
	}
}

func (s *testChainScenario) prepareTimeout(c *Chain) (timeout uint64) {
	timeout = goodTimeout

	if s.Switches[presetTimeout] {
		c.timeout = initialTimeout
	}

	if s.Switches[useBadTimeout] {
		timeout = badTimeout
	}

	return timeout
}
