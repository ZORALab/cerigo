package cmdchain

import (
	"fmt"
	"time"
)

// data oriented interface functions
type data struct {
	s string
	e string
}

func (d *data) lv1CMD(super *Chain) {
	processChain(super, lv1InterfaceCMD)
}

func (d *data) lv2CMD(super *Chain) {
	processChain(super, lv2InterfaceCMD)
}

func (d *data) lv3CMD(super *Chain) {
	processChain(super, lv3InterfaceCMD)
	super.Done()
}

func (d *data) lv4CMD(super *Chain) {
	processChain(super, lv4InterfaceCMD)
	super.Run(chainedLv4)
}

// common package functions
func lv1CMD(super *Chain) {
	processChain(super, lv1NativeCMD)
}

func lv2CMD(super *Chain) {
	processChain(super, lv2NativeCMD)
}

func lv3CMD(super *Chain) {
	processChain(super, lv3NativeCMD)
	super.Done()
}

func lv4CMD(super *Chain) {
	processChain(super, lv4NativeCMD)
	super.Run(chainedLv4)
}

// private test functions
func chainedLv4(super *Chain) {
	processChain(super, chainedCMD)
	super.Done()
}

func processChain(super *Chain, setter string) {
	time.Sleep(10 * time.Millisecond)

	d, ok := super.GetData().(*data)
	if !ok {
		super.SetError(fmt.Errorf(testErrorNotAssertable))
		return
	}

	if d.s == "" {
		d.s = setter
	}

	e := super.GetError()
	if e != nil {
		d.e = e.Error()
	}
}
