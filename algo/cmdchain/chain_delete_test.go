package cmdchain

import (
	"testing"
)

func TestDelete(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testDelete {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		label := s.prepareDelete()

		// test
		c.Delete(label)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertDelete(th, c, label)
		s.log(th, map[string]interface{}{
			"input label": label,
			"got":         c.list,
		})
		th.Conclude()
	}
}
