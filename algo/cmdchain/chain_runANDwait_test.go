package cmdchain

import (
	"testing"
)

func TestRunAndWait(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testRun {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		cmd, data, expect := s.prepareChain(c)

		// test
		c.Run(cmd)
		c.Wait()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertChainRun(th, data, expect)
		s.log(th, map[string]interface{}{
			"expect":   expect,
			"got data": data,
		})
		th.Conclude()
	}
}
