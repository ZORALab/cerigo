package cmdchain

import (
	"fmt"
)

const (
	cmdGetAlternateInput = 1
)

// 0. Map your command chains before starting to write one. In this example,
//    the calculator performs as follows:
//    (i)      GetInput
//    (ii)     Add
//    (iii)    Print
//    (iv)     PrepareNext
//    (v)      GetInput
//    (vi)     Minus
//    (vii)    Print
//    (viii)   PrepareNext
//
//    It timeout or error occurs:
//    (x-i)    HandleError

// 1. build your data type to use across the chain.
type calculator struct {
	a     int
	b     int
	total int
	ops   string
	count int
}

// 2. build your chain of commands' command functions. Make sure that each
//    functions has a link to one another.
func HandleError(c *Chain) {
	fmt.Printf("[ Sandbox ERROR ] %v\n", c.GetError())
}

func InterruptPrint(c *Chain) {
	fmt.Printf("[ INFO ] chain got interrupted. servicing...\n")
}

func Add(c *Chain) {
	cal := getCalculator(c)
	if cal == nil {
		return
	}

	cal.total = cal.a + cal.b
	cal.ops = "+"
	cal.count++

	c.Run(Print) // 2.1 - running the next command
}

func Minus(c *Chain) {
	cal := getCalculator(c)
	if cal == nil {
		return
	}

	cal.total = cal.a - cal.b
	cal.ops = "-"
	cal.count++

	c.Run(Print) // 2.1 - running the next command
}

func Print(c *Chain) {
	cal := getCalculator(c)
	if cal == nil {
		return
	}

	s := fmt.Sprintf("ops %v: %v %v %v = %v\n",
		cal.count,
		cal.a,
		cal.ops,
		cal.b,
		cal.total)
	fmt.Printf("%s", s)

	c.Interrupts(InterruptPrint) // 2.2 - setting an interruptive run
	c.Run(PrepareNext)           // 2.1 - running the next command
}

func PrepareNext(c *Chain) {
	cal := getCalculator(c)
	if cal == nil {
		return
	}

	cal.a = cal.total
	cal.b = 0
	cal.total = 0

	if cal.ops == "-" {
		c.Done() // 2.3 - notify Chain that the chain is done.
		return
	}

	c.Run(GetInput) // 2.1 - running the next command
}

func GetInput(c *Chain) {
	cal := getCalculator(c)
	if cal == nil {
		return
	}

	if cal.a == 0 {
		// 2.4 - intercept next process block with a registered command
		c.Intercept(cmdGetAlternateInput)
	}

	cal.b = 123

	if cal.ops == "+" {
		c.Run(Minus) // 2.1 - running the next command
	} else {
		c.Run(Add) // 2.1 - running the next command
	}
}

func SetInitialInput(c *Chain) {
	cal := getCalculator(c)
	if cal == nil {
		return
	}

	cal.a = 399

	c.Run(GetInput) // 2.1 - running the next command
}

func getCalculator(c *Chain) *calculator {
	cal, ok := c.GetData().(*calculator)
	if ok {
		return cal
	}

	// 2.2 - set error unto the Chain instead of running error handler
	//       yourself. The Chain will automatically runs the error
	//       handler as the last command.
	c.SetError(fmt.Errorf("missing calculator data"))

	return nil
}

// main function
func Example() {
	// 3. create a Chain
	c := New()

	// 4. set a timeout. The unit is nanosecond.
	c.SetTimeout(1 * 1000 * 1000 * 1000) // t * sec * mil * nano

	// 5. set your data types
	c.SetData(&calculator{
		a:     0,
		b:     0,
		total: 0,
	})

	// 6. set the error handler function
	c.SetErrorHandler(HandleError)

	// 7. register any higher level commands for inteception
	c.Register(cmdGetAlternateInput, SetInitialInput)

	// 8. Run the first command. Use Wait() on for the first command
	//    to wait for entire chain to complete.
	c.Run(GetInput).Wait()
}
