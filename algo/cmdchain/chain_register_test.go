package cmdchain

import (
	"testing"
)

func TestRegister(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testRegister {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		cmd, _, _ := s.prepareChain(c)
		label, cmd, expect := s.prepareRegister(c, cmd)

		// test
		err := testRunRegister(c, label, cmd)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertRegister(th, c, label, err, expect)
		s.log(th, map[string]interface{}{
			"input label":  label,
			"input cmd":    cmd,
			"expect panic": expect,
			"got panic":    err,
		})
		th.Conclude()
	}
}

func testRunRegister(c *Chain, label int, cmd func(super *Chain)) (err error) {
	defer func() {
		switch e := recover().(type) {
		case error:
			err = e
		default:
			err = nil
		}
	}()
	c.Register(label, cmd)

	return nil
}
