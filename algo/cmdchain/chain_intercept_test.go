package cmdchain

import (
	"testing"
)

func TestIntercept(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testIntercept {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		label, expect := s.prepareIntercept(c)

		// test
		c.Intercept(label)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertIntercept(th, c, expect)
		s.log(th, map[string]interface{}{
			"input label": label,
			"expect":      expect,
			"got":         c.next,
		})
		th.Conclude()
	}
}
