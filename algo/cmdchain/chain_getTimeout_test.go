package cmdchain

import (
	"testing"
)

func TestGetTimeout(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testGetTimeout {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		_ = s.prepareTimeout(c)

		// test
		timeout := c.GetTimeout()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertTimeout(th, c)
		s.log(th, map[string]interface{}{
			"input timeout": c.timeout,
			"got timeout":   timeout,
		})
		th.Conclude()
	}
}
