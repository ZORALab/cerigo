package cmdchain

import (
	"testing"
)

func TestSetErrorHandler(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testSetErrorHandler {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		cmd, _, _ := s.prepareChain(c)
		cmd = s.prepareErrorHandler(cmd)

		// test
		c.SetErrorHandler(cmd)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertErrorHandler(th, c, cmd)
		s.log(th, map[string]interface{}{
			"input": cmd,
			"got":   c.errorHandler,
		})
		th.Conclude()
	}
}
