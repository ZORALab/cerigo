package cmdchain

import (
	"fmt"
	"sync"
	"time"
)

const (
	// NanoSecond is the unit multipler for chain.Timeout
	NanoSecond = uint64(1)

	// MicroSecond is the unit mulipler for chain.Timeout
	MicroSecond = 1000 * NanoSecond

	// MilliSecond is the unit multipler for chain.Timeout
	MilliSecond = 1000 * MicroSecond

	// Second is the unit multiplier for chain.Timeout
	Second = 1000 * MilliSecond

	// Minute is the unit multiplier for chain.Timeout
	Minute = 60 * Second

	// Hour is the unit multiplier for chain.Timeout
	Hour = 60 * Minute

	// DefaultTimeout is the default chain.Timeout, which is 2 Minutes
	DefaultTimeout = 2 * Minute
)

const (
	notIntercepted = 0
)

// Chain is the structure that facilitates the chain of commands executions.
//
// Chain is using sync.mutexes to synchonize all its data variables for safe
// concurrent executions. As for concurrent delegation, Chain uses Goroutines
// where it best suited for.
//
// This function has private elements that require initialization. Hence, use
// New() function to create one instead of using the conventional structure{}
// method.
type Chain struct {
	data    interface{}
	timeout uint64

	errorHandler func(super *Chain)
	interrupts   []func(super *Chain)
	list         map[int]func(super *Chain)

	next int
	err  error
	stop bool
	done chan bool
	sync *sync.RWMutex
}

// New is to create a new Chain object with internal elements initialized.
func New() *Chain {
	c := &Chain{}
	c.Reset()

	return c
}

// Run is to execute a given function.
//
// If the Chain has an error value, this function will execute its available
// error handler and ends the chain.
//
// Run executes commands based on pre-defined priorities:
//   1. priority #1   - error handling and subsequently stop the chain
//   2. priority #2   - process any pending interrupts then resumes the run
//   3. priority #3   - process any pending pre-registered commands
//   4. priority #4   - process the given input command
//
// It returns:
//   1. itself      - for optional next functional call like Wait()
func (c *Chain) Run(x func(super *Chain)) *Chain {
	c.sync.Lock()
	defer c.sync.Unlock()

	switch {
	case c.stop:
	case c.err != nil:
		go c.handleError()
	case len(c.interrupts) != 0:
		go c.handleInterrupts(x)
	case c.next != notIntercepted:
		go c.handleNext(x)
	case x != nil:
		go x(c)
	}

	return c
}

func (c *Chain) handleError() {
	if c.errorHandler != nil {
		c.errorHandler(c)
	}

	c.Done()
}

func (c *Chain) handleInterrupts(x func(super *Chain)) {
	var cmd func(super *Chain)
	cmd, c.interrupts = c.interrupts[0], c.interrupts[1:]
	cmd(c)
	c.Run(x)
}

func (c *Chain) handleNext(x func(super *Chain)) {
	cmd := c.list[c.next]
	c.next = 0

	if cmd != nil {
		cmd(c)
		return
	}

	c.SetError(fmt.Errorf(ErrorMissingCommand))
	c.Run(x)
}

// Wait is to wait for the entire chain completion.
//
// This function relies on the given Timeout value to exit an indefinite chain
// execution. If the Timeout is 0, this function sets to DefaultTimeout. To
// wait indefinitely, set the timeout duration to unreasonable value such as 2
// hours.
//
// This function should only be called once right after the first command's
// Run(...). It is meant to instruct the "main" routine to hold and wait for
// all the chains to complete.
//
// Upon timeout, it instructs the chain to perform error handling with the
// error message ErrorTimeout as the last process.
func (c *Chain) Wait() {
	done := make(chan bool, 1)

	go func() {
		<-c.done
		done <- true
	}()

	timeout := c.GetTimeout()
	if timeout == 0 {
		timeout = DefaultTimeout
	}

	for {
		select {
		case <-done:
			return
		case <-time.After(time.Duration(timeout)):
			c.SetError(fmt.Errorf(ErrorTimeout))
		}
	}
}

// Done is to instruct the chain to complete and close the process chains.
func (c *Chain) Done() {
	c.sync.Lock()
	defer c.sync.Unlock()
	c.stop = true
	c.done <- true
}

// Reset is to set the chain back to its initial condition for the next run.
func (c *Chain) Reset() {
	c.err = nil
	c.done = make(chan bool)
	c.sync = &sync.RWMutex{}
	c.list = map[int]func(super *Chain){}
	c.interrupts = []func(super *Chain){}
	c.stop = false
}

// CMDExists is to check a registered label has a command
//
// It returns:
//   1. true  - the label has a registered command
//   2. false - the label is vacant
func (c *Chain) CMDExists(label int) bool {
	c.sync.Lock()
	defer c.sync.Unlock()
	x := c.list[label]

	return x != nil
}

// Intercept is to take over the next Run(..)  with a registered command
// safely using mutexes.
//
// It requires a registered command's label as the input.
//
// If the given label does not exist, Intercept routes the next Run(...) to
// error handling with ErrorMissingCommand error object.
func (c *Chain) Intercept(label int) {
	exist := c.CMDExists(label)
	c.sync.Lock()
	defer c.sync.Unlock()

	if exist {
		c.next = label
	} else {
		c.err = fmt.Errorf(ErrorMissingCommand)
	}
}

// Register is to save a command into the chain for process interception.
//
// If the command is already exist or the given label is 0, this function
// panics, prompting an immediate attention for fixing.
//
// The label can be any integer number (both negative or positive) except 0,
// which is a reserved label for "no interception".
func (c *Chain) Register(label int, cmd func(super *Chain)) {
	if label == 0 {
		panic(fmt.Errorf(ErrorBadLabel))
	}

	if cmd == nil {
		panic(fmt.Errorf(ErrorMissingCommand))
	}

	c.sync.Lock()
	defer c.sync.Unlock()
	x := c.list[label]

	if x != nil {
		panic(fmt.Errorf(ErrorExistingCommand))
	}

	c.list[label] = cmd
}

// Delete is to delete a registered command from the chain regardlessly and
// safely using mutexes.
//
// If the command does not exists, this function does nothing.
func (c *Chain) Delete(label int) {
	c.sync.Lock()
	defer c.sync.Unlock()
	delete(c.list, label)
}

// Interrupts sets an interrupted process block for an existing run safely
// using mutexes.
//
// Interrupts pauses the incoming Run(...) and execute the given interrupting
// cmd process based on First-In-First-Out (FIFO) policy. Once all interrupt
// commands are executed, it resumes the Run(...) back to the original process
// block.
//
// While being interruptive, the interruptive command's content must be
// independent and safe (e.g. thread-safe) at any given point of the chain
// processes. Otherwise, the chain can behaves unpredictably.
//
// If cmd is nil, this function does nothing and return early.
func (c *Chain) Interrupts(cmd func(super *Chain)) {
	if cmd == nil {
		return
	}

	c.sync.Lock()
	defer c.sync.Unlock()
	c.interrupts = append(c.interrupts, cmd)
}

// GetError is to obtain the error object from the chain safely using mutexes.
//
// It returns:
//   1. nil    - no error found
//   2. error  - error object.
func (c *Chain) GetError() error {
	c.sync.Lock()
	defer c.sync.Unlock()

	return c.err
}

// SetError is to set an error object into the chain safely using mutexes.
func (c *Chain) SetError(err error) {
	c.sync.Lock()
	defer c.sync.Unlock()
	c.err = err
}

// SetErrorHandler is to set a given process function to handle the error
// safely using mutexes.
func (c *Chain) SetErrorHandler(x func(super *Chain)) {
	c.sync.Lock()
	defer c.sync.Unlock()
	c.errorHandler = x
}

// SetData is to save a given data into the Chain data system safely using
// mutexes.
func (c *Chain) SetData(d interface{}) {
	c.sync.Lock()
	defer c.sync.Unlock()
	c.data = d
}

// Getdata is to obtain the saved data from the Chain data system safely using
// mutexes
func (c *Chain) GetData() interface{} {
	c.sync.Lock()
	defer c.sync.Unlock()

	return c.data
}

// SetTimeout is to save a given timeout duration into the Chain safely using
// mutexes.
//
// the timeout value is for the entire chain executions. The value can be
// multiplied for bigger values using the available unit constants like:
//
//                  chain.Timeout = 5 * cmdchain.Seconds
func (c *Chain) SetTimeout(duration uint64) {
	c.sync.Lock()
	defer c.sync.Unlock()
	c.timeout = duration
}

// GetTimeout is to get a given timeout duration from the Chain safely using
// mutexes.
func (c *Chain) GetTimeout() uint64 {
	c.sync.Lock()
	defer c.sync.Unlock()

	return c.timeout
}
